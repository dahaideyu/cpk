﻿using CPK.Data;
using CPK.Domain.Entity.SystemManage;
using CPK.Domain.IRepository.SystemManage;
using CPK.Repository.SystemManage;
using System.Collections.Generic;

namespace CPK.Repository.SystemManage
{
    public class RoleRepository : RepositoryBase<RoleEntity>, IRoleRepository
    {
        public void DeleteForm(string keyValue)
        {
            using (var db = new RepositoryBase().BeginTrans())
            {
                db.Delete<RoleEntity>(t => t.F_Id == keyValue);
                db.Delete<RoleAuthorizeEntity>(t => t.F_ObjectId == keyValue);
                db.Commit();
            }
        }
        public void SubmitForm(RoleEntity roleEntity, List<RoleAuthorizeEntity> roleAuthorizeEntitys, string keyValue)
        {
            using (var db = new RepositoryBase().BeginTrans())
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    db.Update(roleEntity);
                }
                else
                {
                    roleEntity.F_Category = 1;
                    db.Insert(roleEntity);
                }
                var dlist = db.FindEntity<RoleAuthorizeEntity>(t => t.F_ObjectId == roleEntity.F_Id);
                if (dlist != null)
                {
                    db.Delete<RoleAuthorizeEntity>(t => t.F_ObjectId == roleEntity.F_Id);

                }
                db.Insert(roleAuthorizeEntitys);
                db.Commit();
            }
        }
    }
}
