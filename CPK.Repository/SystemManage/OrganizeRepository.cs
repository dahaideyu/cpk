﻿using CPK.Data;
using CPK.Domain.Entity.SystemManage;
using CPK.Domain.IRepository.SystemManage;
using CPK.Repository.SystemManage;

namespace CPK.Repository.SystemManage
{
    public class OrganizeRepository : RepositoryBase<OrganizeEntity>, IOrganizeRepository
    {

    }
}
