using CPK.Data;
using CPK.Domain.Entity.ProductManage;
using CPK.Domain.IRepository.ProductManage;
using CPK.Repository.ProductManage;
using System.Collections.Generic;
using System.Text;

namespace CPK.Repository.ProductManage
{
    public class CPKResultRepository : RepositoryBase<CPKResultEntity>, ICPKResultRepository
    {
        public List<CPKResultEntity> GetNORepeatList()
        {
            StringBuilder strsql = new StringBuilder();
            strsql.AppendFormat("select ");
            strsql.AppendFormat("[F_Id]");
            strsql.AppendFormat(",[F_ChartId]");
            strsql.AppendFormat(",[F_CPK]");
            strsql.AppendFormat(",[F_LSL]");
            strsql.AppendFormat(",[F_USL]");
            strsql.AppendFormat(",[F_Sigma]");
            strsql.AppendFormat(",[F_DistributeStatistics]");
            strsql.AppendFormat(",[F_StartTime]");
            strsql.AppendFormat(",[F_FrequencyType]");
            strsql.AppendFormat(",[F_Frequency]");
            strsql.AppendFormat(",[F_LastLoadTime]");
            strsql.AppendFormat(",[F_SortCode]");
            strsql.AppendFormat(",[F_EnabledMark]");
            strsql.AppendFormat(",[F_LastLoadTime]");
            strsql.AppendFormat(",[F_CreatorTime]");
            strsql.AppendFormat(",[F_CreatorUserId]");
            strsql.AppendFormat(",[F_LastModifyTime]");
            strsql.AppendFormat(",[F_LastModifyUserId]");
            strsql.AppendFormat(",[F_DeleteMark]");
            strsql.AppendFormat(",[F_DeleteTime]");
            strsql.AppendFormat(",[F_DeleteUserId]");
            strsql.AppendFormat(",[F_CreatorUserName]");
            strsql.AppendFormat(",[F_DeleteUserName]");
            strsql.AppendFormat(",[F_LastModifyUserName]");
            strsql.AppendFormat(" from");
            strsql.AppendFormat(" (select   [dbo].[CPK_CPKResult].*, row_number() over ");
            strsql.AppendFormat(" (partition by F_ChartId order by F_CreatorTime desc)"); 
            strsql.AppendFormat(" rn from[dbo].[CPK_CPKResult]) a  ");
            strsql.AppendFormat(" where rn = 1"); 
            //return SqlQuery<CPKResultEntity>(strsql.ToString().Trim()).ToList();
            List<CPKResultEntity> list = new List<CPKResultEntity>();
            using (var db = new RepositoryBase().BeginTrans())
            {
             //   list= db.FindList<CPKResultEntity>(strsql.ToString());
                list = db.FindList<CPKResultEntity>("select * from [dbo].[CPK_CPKResult]");

            }
            return list;
        }
    }
}