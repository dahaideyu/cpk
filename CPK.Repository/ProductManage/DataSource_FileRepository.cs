using CPK.Data;
using CPK.Domain.Entity.ProductManage;
using CPK.Domain.IRepository.ProductManage;
using CPK.Repository.ProductManage;

namespace CPK.Repository.ProductManage
{
    public class DataSource_FileRepository : RepositoryBase<DataSource_FileEntity>, IDataSource_FileRepository
    {
        
    }
}