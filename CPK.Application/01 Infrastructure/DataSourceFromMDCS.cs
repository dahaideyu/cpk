using System;
using System.Collections.Generic;
using System.Data;
using CPK.Data.Extensions;
using CPK.Domain.ViewModel;

namespace CPK.Application.Infrastructure
{
    public class DataSourceFromMDCS
    {
        public string ConnString { get; private set; }
        public DataSourceFromMDCS(string sConnString)
        {
            ConnString =  sConnString;
        }

        public DataTable getLinesList(string sKeyWord)
        {
            List<DataSourceItemVM> result = new List<DataSourceItemVM>();
            DbHelper dbhelper=new DbHelper(ConnString);
            string sSQL=@"
        select * from  [Lines] l
        where  l.LineName like '%{0}%' 
        order by  l.LineName";
            DataSet ds = dbhelper.GetDataBySQL(string.Format(sSQL,sKeyWord));
            if(ds.Tables[0].Rows.Count>0)
            {
               return ds.Tables[0];
            }
            return null;
        }

        public DataTable getDevicesList(string sKeyWord,string sLineName)
        {
            List<DataSourceItemVM> result = new List<DataSourceItemVM>();
            DbHelper dbhelper=new DbHelper(ConnString);
            string sSQL=@"
        select * from  [Devices] d
        where  d.Name like '%{0}%' and (d.lineName = '{1}' or '{1}'='')
        order by  d.Name";
            DataSet ds = dbhelper.GetDataBySQL(string.Format(sSQL,sKeyWord,sLineName));
            if(ds.Tables[0].Rows.Count>0)
            {
               return ds.Tables[0];
            }
            return null;
        }

        public DataTable getVariablesList(string sKeyWord,string sDeviceId)
        {
            List<DataSourceItemVM> result = new List<DataSourceItemVM>();
            DbHelper dbhelper=new DbHelper(ConnString);
            string sSQL=@"
        select * from  [Variables] d
        where  d.Name like '%{0}%' and (d.DeviceID = '{1}' or '{1}'='')
        order by  d.Name";
            DataSet ds = dbhelper.GetDataBySQL(string.Format(sSQL,sKeyWord,sDeviceId));
            if(ds.Tables[0].Rows.Count>0)
            {
               return ds.Tables[0];
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sLine"></param>
        /// <param name="sDevice"></param>
        /// <param name="sVarriable"></param>
        /// <param name="dStartTime"></param>
        /// <param name="dEndTime"></param>
        /// <returns></returns>
        public List<DataSourceItemVM> getMDCSDataSource(string sLine,string sDevice,string sVarriable,DateTime dStartTime,DateTime dEndTime,string[] DirtyRead=null,string sMCF="",string sBoardType="")
        {
            List<DataSourceItemVM> result = new List<DataSourceItemVM>();
            DbHelper dbhelper=new DbHelper(ConnString);
            string smcfsql=@" left join [Data] mcf on a.TestID=mcf.TestID
            left join [Variables] mcfname on mcf.variable=mcfname.VarID";
            string sbtsql=@" left join [Data] bt on a.TestID=bt.TestID
            left join [Variables] btname on bt.variable=btname.VarID";
            string smcfwheresql=@" and lower(mcfname.Name)='mcf_num' 
                and mcf.sval='{5}' ";
            string sbtwheresql=@" and lower(btname.Name)='boardtype'
                and bt.sval='{6}' ";
            string sSQL=@"
        select a.testid,b.nval,a.TestTime,a.[key]  from [Test] a left join [Data] b on a.TestID=b.TestID
        left join [Variables] c on b.variable=c.VarID
        left join [Devices] d on a.DeviceID = d.DeviceID
        left join [Lines] l on l.LineName = d.LineName
        "+(string.IsNullOrWhiteSpace(sMCF)|| sMCF.ToUpper()=="NULL"? "":smcfsql)
        +(string.IsNullOrWhiteSpace(sBoardType) || sBoardType.ToUpper() == "NULL" ? "":sbtsql)
        + @" where  d.LineName='{0}' and a.DeviceID='{1}' and b.variable='{2}'
        and a.testtime is not null and a.testtime between '{3}' and '{4}'
        "+(string.IsNullOrWhiteSpace(sMCF) || sMCF.ToUpper() == "NULL" ? "":smcfwheresql)
        +(string.IsNullOrWhiteSpace(sBoardType) || sBoardType.ToUpper() == "NULL" ? "" :sbtwheresql)
        +@" order by  a.testid";
            DataSet ds = dbhelper.GetDataBySQL(
                string.Format(sSQL,sLine,sDevice,sVarriable,
                dStartTime.ToString("yyyy-MM-dd HH:mm:ss"),dEndTime.ToString("yyyy-MM-dd HH:mm:ss"),
                sMCF,sBoardType)
                );
            if(ds.Tables[0].Rows.Count>0)
            {
                foreach(DataRow dr in ds.Tables[0].Rows)
                {
                    //跳过藏数据
                    if(DirtyRead!=null)
                    {
                        bool isDirty=false;
                        foreach(string item in DirtyRead)
                        {
                            if(!string.IsNullOrWhiteSpace(item)&&item.ToUpper()!="NULL")
                            {
                                if(item.Contains(">"))
                                {
                                    if(item.Contains(">="))
                                    {
                                        if(Convert.ToDecimal(dr["nval"])>=Convert.ToDecimal(item.Replace(">=","")))
                                        {
                                            isDirty=true;
                                            break;
                                        }else
                                        {
                                            continue;
                                        }
                                    }
                                    else
                                    if(Convert.ToDecimal(dr["nval"])>Convert.ToDecimal(item.Replace(">","")))
                                    {
                                        isDirty=true;
                                        break;
                                    }else
                                    {
                                        continue;
                                    }
                                }
                                else
                                if(item.Contains("<"))
                                {
                                    if(item.Contains("<="))
                                    {
                                        if(Convert.ToDecimal(dr["nval"])<=Convert.ToDecimal(item.Replace("<=","")))
                                        {
                                            isDirty=true;
                                            break;
                                        }else
                                        {
                                            continue;
                                        }
                                    }
                                    else
                                    if(Convert.ToDecimal(dr["nval"])>Convert.ToDecimal(item.Replace("<","")))
                                    {
                                        isDirty=true;
                                        break;
                                    }else
                                    {
                                        continue;
                                    }
                                }
                                else
                                if(Convert.ToDecimal(dr["nval"])==Convert.ToDecimal(item))
                                {
                                    isDirty=true;
                                    break;
                                }else
                                {
                                    continue;
                                }
                            }
                        }
                        if(isDirty)
                            continue;
                    }
                    DataSourceItemVM dsItem=new DataSourceItemVM();
                    dsItem.SortValue=Convert.ToString(dr["testid"]);
                    dsItem.TimeValue=Convert.ToDateTime(dr["TestTime"]);
                    dsItem.KeyValue=Convert.ToString(dr["key"]);
                    dsItem.CalValue=Convert.ToDecimal(dr["nval"]);
                    dsItem.TagList.Add(new DataSourceItemTagVM("key",Convert.ToString(dr["key"])));
                    if(!string.IsNullOrWhiteSpace(sMCF))
                        dsItem.TagList.Add(new DataSourceItemTagVM("MCF_Num",Convert.ToString(sMCF)));
                    if(!string.IsNullOrWhiteSpace(sBoardType))
                        dsItem.TagList.Add(new DataSourceItemTagVM("BoardType",Convert.ToString(sBoardType)));
                    result.Add(dsItem);
                }
            }
            return result;
        }


    }
}