﻿using CPK.Domain.ViewModel;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CPK.Application
{
    public static class DataItemCalCom
    {
        public static decimal CalSigmax(List<DataSourceItemVM> dataSourceVMList)
        {
            var total = 0;
            var totalsum = 0m;
            foreach (var item in dataSourceVMList)
            {
                totalsum = totalsum + item.CalValue * item.CalValue;
                total++;
            }
            var sigmax = Math.Sqrt((double)totalsum) / total;
            return (decimal)sigmax;
        }
        public static decimal CalXSigmax(List<DataSourceCalRsultVM> dataSourceVMList,decimal avg)
        {
            double sigmax=0;
            var total = dataSourceVMList.Count;
            if(total>1)
            {
                var totalsum = 0m;
                foreach (var item in dataSourceVMList)
                {
                    totalsum +=  (item.X - avg)*(item.X - avg);
                }
                sigmax = Math.Sqrt( (double)totalsum/ (dataSourceVMList.Count-1));
            }
                return (decimal)sigmax;
            
            // foreach (var item in dataSourceVMList)
            // {
            //     totalsum = totalsum + item.X * item.X;
            //     total++;
            // }
            // var sigmax = Math.Sqrt((double)totalsum) / total;

        }
        public static decimal CalRSigmax(List<DataSourceCalRsultVM> dataSourceVMList,decimal avg)
        {
            double sigmax=0;
            var total = dataSourceVMList.Count;
            if(total>1)
            {
                var totalsum = 0m;
                foreach (var item in dataSourceVMList)
                {
                    totalsum +=  (item.R - avg)*(item.R - avg);
                }
                sigmax = Math.Sqrt( (double)totalsum/ (dataSourceVMList.Count-1));
            }
            // foreach (var item in dataSourceVMList)
            // {
            //     totalsum = totalsum + item.R * item.R;
            //     total++;
            // }
            // var sigmax = Math.Sqrt((double)totalsum) / total;
            return (decimal)sigmax;
        }
        /// <summary>
        /// rule 判定
        /// </summary>
        /// <param name="dataSourceCalVMList"></param>
        /// <param name="rule2"></param>
        /// <returns></returns>
        public static List<DataSourceCalRsultVM> CalRule(List<DataSourceCalRsultVM> dataSourceCalVMList,decimal lcl,decimal ucl, 
        decimal rlcl,decimal rucl,
        int rule1 = 1, int rule2 = 9, int rule3 = 6, int rule4 = 14, int rule51 = 3, 
        int rule52 = 2,int rule61=5,int rule62=4,int rule7=15,int rule8=8)
        {
            dataSourceCalVMList=CalRuleX(dataSourceCalVMList,lcl,ucl,rule1,rule2,rule3,rule4,rule51,rule52,rule61,rule62,rule7,rule8);
            dataSourceCalVMList=CalRuleR(dataSourceCalVMList,rlcl,rucl,rule1,rule2,rule3,rule4,rule51,rule52,rule61,rule62,rule7,rule8);
            return dataSourceCalVMList;
        }
        /// <summary>
        /// rule 判定
        /// </summary>
        /// <param name="dataSourceCalVMList"></param>
        /// <param name="rule2"></param>
        /// <returns></returns>
        public static List<DataSourceCalRsultVM> CalRuleX(List<DataSourceCalRsultVM> dataSourceCalVMList,decimal lcl,decimal ucl,
        int rule1 = 1, int rule2 = 9, int rule3 = 6, int rule4 = 14, int rule51 = 3, 
        int rule52 = 2,int rule61=5,int rule62=4,int rule7=15,int rule8=8)
        {
            // var total = 0;
            // var totalsum = 0m;
            // var totalavgsum=0m;
            // foreach (var item in dataSourceCalVMList)
            // {
            //     totalsum = totalsum + item.X * item.X;
            //     totalavgsum=totalavgsum+item.X;
            //     total++;
            // }
            // var sigmax = Math.Sqrt((double)totalsum) / total;
            //var sigmax = (decimal)sigmax;
            decimal sigmax =Math.Abs(ucl -lcl)/6;
            decimal cl= (ucl+lcl)/2;
            for (var i = 0; i < dataSourceCalVMList.Count; i++)
            {
                dataSourceCalVMList[i].Sigmax = (decimal)sigmax;
              //  var sigmax3 = (decimal)(3 * sigmax);
                //1个点落在A区以外
                if (rule1 != 0 && (dataSourceCalVMList[i].X > ucl||dataSourceCalVMList[i].X < lcl))
                {
                    dataSourceCalVMList[i].Rule1 = true;
                }
                //连续9个点落在中心线的同一侧
                if (i >= rule2 && rule2 != 0)
                {
                    var rule2LowFlag = 0;
                    var rule2UpFlag = 0;

                    for (var rule2index = 0; rule2index < rule2; rule2index++)
                    {
                        if (dataSourceCalVMList[i - rule2index].X > cl)
                        {
                            rule2UpFlag++;
                        }
                        if (dataSourceCalVMList[i - rule2index].X < cl)
                        {
                            rule2LowFlag++;
                        }
                    }
                    if (rule2LowFlag == rule2 || rule2UpFlag == rule2)
                    {
                        for (var rule2index = 0; rule2index < rule2; rule2index++)
                        {
                            dataSourceCalVMList[i - rule2index].Rule2 = true;

                        }
                    }

                }
                //连续6个点递增或递减
                if (i > rule3 && rule3 != 0)
                {
                    var rule3LowFlag = 0;
                    var rule3UpFlag = 0;

                    for (var rule3index = 0; rule3index < rule3; rule3index++)
                    {
                        if (dataSourceCalVMList[i - rule3index].X > dataSourceCalVMList[i - rule3index - 1].X)
                        {
                            rule3UpFlag++;
                        }
                        if (dataSourceCalVMList[i - rule3index].X < dataSourceCalVMList[i - rule3index - 1].X)
                        {
                            rule3LowFlag++;
                        }
                    }
                    if (rule3LowFlag == rule3 || rule3UpFlag == rule3)
                    {
                        for (var rule3index = 0; rule3index < rule3; rule3index++)
                        {
                            dataSourceCalVMList[i - rule3index].Rule3 = true;

                        }
                    }
                }
                //连续14个点中相邻点交替上下
                if (i > rule4 && rule4 != 0)
                {
                    var rule4Flag = 0;

                    for (var rule4index = 0; rule4index < rule4 - 2; rule4index++)
                    {
                        var cha1 = dataSourceCalVMList[i - rule4index].X - dataSourceCalVMList[i - rule4index - 1].X;
                        var cha2 = dataSourceCalVMList[i - rule4index - 1].X - dataSourceCalVMList[i - rule4index - 2].X;
                        if ((cha1 < 0 && cha2 > 0) || (cha1 > 0 && cha2 < 0))
                        {
                            rule4Flag++;
                        }

                    }
                    if (rule4Flag == rule4)
                    {
                        for (var rule4index = 0; rule4index < rule4; rule4index++)
                        {
                            dataSourceCalVMList[i - rule4index].Rule4 = true;

                        }
                    }
                }
                //连续3个点中有2个点落在中心线同一侧的B区以外
                if (i > rule51 && rule51 != 0)
                {
                    var rule5UPFlag = 0;
                    var rule5DOWNFlag = 0;
                    for (var rule5index = 0; rule5index < rule51 ; rule5index++)
                    {
                        if (dataSourceCalVMList[i - rule5index].X > cl + 2 * sigmax)
                        {
                            rule5UPFlag++;
                        }
                        if (dataSourceCalVMList[i - rule5index].X < cl - 2 * sigmax)
                        {
                            rule5DOWNFlag++;
                        }
                    }
                    if (rule5UPFlag == rule52||rule5DOWNFlag == rule52)
                    {
                        for (var rule5index = 0; rule5index < rule51; rule5index++)
                        {
                            dataSourceCalVMList[i - rule5index].Rule5 = true;

                        }
                    }
                }
                //连续5个点中有4个点落在中心线同一侧的C区以外
                 if (i > rule61 && rule61 != 0)
                {
                    var rule6UpFlag = 0;
                    var rule6DownFlag = 0;
                    for (var rule6index = 0; rule6index < rule61; rule6index++)
                    {
                        if (dataSourceCalVMList[i - rule6index].X > cl + sigmax)
                        {
                            rule6UpFlag++;
                        }
                        if (dataSourceCalVMList[i - rule6index].X < cl - sigmax)
                        {
                            rule6DownFlag++;
                        }
                    }
                    if (rule6UpFlag == rule62|| rule6DownFlag==rule62)
                    {
                        for (var rule6index = 0; rule6index < rule61; rule6index++)
                        {
                            dataSourceCalVMList[i - rule6index].Rule6 = true;

                        }
                    }
                }
                //连续15个点落在中心线两侧的C区以内
                if (i > rule7 && rule7 != 0)
                {
                    var rule7Flag = 0;

                    for (var rule7index = 0; rule7index < rule7; rule7index++)
                    {
                        if ((dataSourceCalVMList[i - rule7index].X <  (cl + sigmax))&&(dataSourceCalVMList[i - rule7index].X> (cl-sigmax)))
                        {
                            rule7Flag++;
                        }
                      
                    }
                    if (rule7Flag == rule7)
                    {
                        for (var rule7index = 0; rule7index < rule7; rule7index++)
                        {
                            dataSourceCalVMList[i - rule7index].Rule7 = true;

                        }
                    }
                }
                //连续8个点落在中心线两侧且无一在C区内
                if (i > rule8 && rule8 != 0)
                {
                    var rule8Flag = 0;

                    for (var rule8index = 0; rule8index < rule8; rule8index++)
                    {
                        if ((dataSourceCalVMList[i - rule8index].X > (cl + sigmax)) || (dataSourceCalVMList[i - rule8index].X < (cl + -sigmax)))
                        {
                            rule8Flag++;
                        }
                      
                    }
                    if (rule8Flag == rule8)
                    {
                        for (var rule8index = 0; rule8index < rule8; rule8index++)
                        {
                            dataSourceCalVMList[i - rule8index].Rule8 = true;

                        }
                    }
                }
            }
            return dataSourceCalVMList;
        }

        /// <summary>
        /// rule 判定
        /// </summary>
        /// <param name="dataSourceCalVMList"></param>
        /// <param name="rule2"></param>
        /// <returns></returns>
        public static List<DataSourceCalRsultVM> CalRuleR(List<DataSourceCalRsultVM> dataSourceCalVMList,decimal lcl,decimal ucl,
        int rule1 = 1, int rule2 = 9, int rule3 = 6, int rule4 = 14, int rule51 = 3, 
        int rule52 = 2,int rule61=5,int rule62=4,int rule7=15,int rule8=8)
        {
            // var total = 0;
            // var totalsum = 0m;
            // var totalavgsum=0m;
            // foreach (var item in dataSourceCalVMList)
            // {
            //     totalsum = totalsum + item.R * item.R;
            //     totalavgsum=totalavgsum+item.R;
            //     total++;
            // }
            // var sigmax = Math.Sqrt((double)totalsum) / total;
            //var sigmax = (decimal)sigmax;
            decimal sigmax =Math.Abs(ucl -lcl)/6;
            decimal cl= (ucl+lcl)/2;
            for (var i = 0; i < dataSourceCalVMList.Count; i++)
            {
                dataSourceCalVMList[i].Sigmax = (decimal)sigmax;
              //  var sigmax3 = (decimal)(3 * sigmax);
                //1个点落在A区以外
                if (rule1 != 0 && (dataSourceCalVMList[i].R > ucl||dataSourceCalVMList[i].R < lcl))
                {
                    dataSourceCalVMList[i].RuleR1 = true;
                }
                //连续9个点落在中心线的同一侧
                if (i >= rule2 && rule2 != 0)
                {
                    var rule2LowFlag = 0;
                    var rule2UpFlag = 0;

                    for (var rule2index = 0; rule2index < rule2; rule2index++)
                    {
                        if (dataSourceCalVMList[i - rule2index].R > cl)
                        {
                            rule2UpFlag++;
                        }
                        if (dataSourceCalVMList[i - rule2index].R < cl)
                        {
                            rule2LowFlag++;
                        }
                    }
                    if (rule2LowFlag == rule2 || rule2UpFlag == rule2)
                    {
                        for (var rule2index = 0; rule2index < rule2; rule2index++)
                        {
                            dataSourceCalVMList[i - rule2index].RuleR2 = true;

                        }
                    }

                }
                //连续6个点递增或递减
                if (i > rule3 && rule3 != 0)
                {
                    var rule3LowFlag = 0;
                    var rule3UpFlag = 0;

                    for (var rule3index = 0; rule3index < rule3; rule3index++)
                    {
                        if (dataSourceCalVMList[i - rule3index].R > dataSourceCalVMList[i - rule3index - 1].R)
                        {
                            rule3UpFlag++;
                        }
                        if (dataSourceCalVMList[i - rule3index].R < dataSourceCalVMList[i - rule3index - 1].R)
                        {
                            rule3LowFlag++;
                        }
                    }
                    if (rule3LowFlag == rule3 || rule3UpFlag == rule3)
                    {
                        for (var rule3index = 0; rule3index < rule3; rule3index++)
                        {
                            dataSourceCalVMList[i - rule3index].RuleR3 = true;

                        }
                    }
                }
                //连续14个点中相邻点交替上下
                if (i > rule4 && rule4 != 0)
                {
                    var rule4Flag = 0;

                    for (var rule4index = 0; rule4index < rule4 - 2; rule4index++)
                    {
                        var cha1 = dataSourceCalVMList[i - rule4index].R - dataSourceCalVMList[i - rule4index - 1].R;
                        var cha2 = dataSourceCalVMList[i - rule4index - 1].R - dataSourceCalVMList[i - rule4index - 2].R;
                        if ((cha1 < 0 && cha2 > 0) || (cha1 > 0 && cha2 < 0))
                        {
                            rule4Flag++;
                        }

                    }
                    if (rule4Flag == rule4)
                    {
                        for (var rule4index = 0; rule4index < rule4; rule4index++)
                        {
                            dataSourceCalVMList[i - rule4index].RuleR4 = true;

                        }
                    }
                }
                //连续3个点中有2个点落在中心线同一侧的B区以外
                if (i > rule51 && rule51 != 0)
                {
                    var rule5UPFlag = 0;
                    var rule5DOWNFlag = 0;
                    for (var rule5index = 0; rule5index < rule51 ; rule5index++)
                    {
                        if (dataSourceCalVMList[i - rule5index].R > cl + 2 * sigmax)
                        {
                            rule5UPFlag++;
                        }
                        if (dataSourceCalVMList[i - rule5index].R < cl - 2 * sigmax)
                        {
                            rule5DOWNFlag++;
                        }
                    }
                    if (rule5UPFlag == rule52||rule5DOWNFlag == rule52)
                    {
                        for (var rule5index = 0; rule5index < rule51; rule5index++)
                        {
                            dataSourceCalVMList[i - rule5index].RuleR5 = true;

                        }
                    }
                }
                //连续5个点中有4个点落在中心线同一侧的C区以外
                 if (i > rule61 && rule61 != 0)
                {
                    var rule6UpFlag = 0;
                    var rule6DownFlag = 0;

                    for (var rule6index = 0; rule6index < rule61; rule6index++)
                    {
                        if (dataSourceCalVMList[i - rule6index].R > cl + sigmax)
                        {
                            rule6UpFlag++;
                        }
                        if (dataSourceCalVMList[i - rule6index].R < cl - sigmax)
                        {
                            rule6DownFlag++;
                        }
                    }
                    if (rule6UpFlag == rule62|| rule6DownFlag==rule62)
                    {
                        for (var rule6index = 0; rule6index < rule61; rule6index++)
                        {
                            dataSourceCalVMList[i - rule6index].RuleR6 = true;

                        }
                    }
                }
                //连续15个点落在中心线两侧的C区以内
                if (i > rule7 && rule7 != 0)
                {
                    var rule7Flag = 0;

                    for (var rule7index = 0; rule7index < rule7; rule7index++)
                    {
                        if ((dataSourceCalVMList[i - rule7index].R <  (cl + sigmax))&&(dataSourceCalVMList[i - rule7index].R> (cl-sigmax)))
                        {
                            rule7Flag++;
                        }
                      
                    }
                    if (rule7Flag == rule7)
                    {
                        for (var rule7index = 0; rule7index < rule7; rule7index++)
                        {
                            dataSourceCalVMList[i - rule7index].RuleR7 = true;

                        }
                    }
                }
                //连续8个点落在中心线两侧且无一在C区内
                if (i > rule8 && rule8 != 0)
                {
                    var rule8Flag = 0;

                    for (var rule8index = 0; rule8index < rule8; rule8index++)
                    {
                        if ((dataSourceCalVMList[i - rule8index].R > (cl + sigmax)) || (dataSourceCalVMList[i - rule8index].R < (cl + -sigmax)))
                        {
                            rule8Flag++;
                        }
                      
                    }
                    if (rule8Flag == rule8)
                    {
                        for (var rule8index = 0; rule8index < rule8; rule8index++)
                        {
                            dataSourceCalVMList[i - rule8index].RuleR8 = true;

                        }
                    }
                }
            }
            // for (var i = 0; i < dataSourceCalVMList.Count; i++)
            // {
            //     dataSourceCalVMList[i].Sigmax =  (decimal)sigmax;
            //   //  var sigmax3 = (decimal)(3 * sigmax);
            //     if (rule1 != 0 && (dataSourceCalVMList[i].R > ucl||dataSourceCalVMList[i].R < lcl))
            //     {
            //         dataSourceCalVMList[i].RuleR1 = true;
            //     }
            //     if (i >= rule2 && rule2 != 0)
            //     {
            //         var rule2LowFlag = 0;
            //         var rule2UpFlag = 0;

            //         for (var rule2index = 0; rule2index < rule2; rule2index++)
            //         {
            //             if (dataSourceCalVMList[i - rule2index].R > sigmax)
            //             {
            //                 rule2UpFlag++;
            //             }
            //             if (dataSourceCalVMList[i - rule2index].R < sigmax)
            //             {
            //                 rule2LowFlag++;
            //             }
            //         }
            //         if (rule2LowFlag == rule2 || rule2UpFlag == rule2)
            //         {
            //             for (var rule2index = 0; rule2index < rule2; rule2index++)
            //             {
            //                 dataSourceCalVMList[i - rule2index].RuleR2 = true;

            //             }
            //         }

            //     }
            //     if (i > rule3 && rule3 != 0)
            //     {
            //         var rule3LowFlag = 0;
            //         var rule3UpFlag = 0;

            //         for (var rule3index = 0; rule3index < rule3; rule3index++)
            //         {
            //             if (dataSourceCalVMList[i - rule3index].R > dataSourceCalVMList[i - rule3index - 1].R)
            //             {
            //                 rule3UpFlag++;
            //             }
            //             if (dataSourceCalVMList[i - rule3index].R < dataSourceCalVMList[i - rule3index - 1].R)
            //             {
            //                 rule3LowFlag++;
            //             }
            //         }
            //         if (rule3LowFlag == rule3 || rule3UpFlag == rule3)
            //         {
            //             for (var rule3index = 0; rule3index < rule3; rule3index++)
            //             {
            //                 dataSourceCalVMList[i - rule3index].RuleR3 = true;

            //             }
            //         }
            //     }

            //     if (i > rule4 && rule4 != 0)
            //     {
            //         var rule4Flag = 0;

            //         for (var rule4index = 0; rule4index < rule4 - 2; rule4index++)
            //         {
            //             var cha1 = dataSourceCalVMList[i - rule4index].R - dataSourceCalVMList[i - rule4index - 1].R;
            //             var cha2 = dataSourceCalVMList[i - rule4index - 1].R - dataSourceCalVMList[i - rule4index - 2].R;
            //             if ((cha1 < 0 && cha2 > 0) || (cha1 > 0 && cha2 < 0))
            //             {
            //                 rule4Flag++;
            //             }

            //         }
            //         if (rule4Flag == rule4)
            //         {
            //             for (var rule4index = 0; rule4index < rule4; rule4index++)
            //             {
            //                 dataSourceCalVMList[i - rule4index].RuleR4 = true;

            //             }
            //         }
            //     }
            //     if (i > rule51 && rule51 != 0)
            //     {
            //         var rule5Flag = 0;

            //         for (var rule5index = 0; rule5index < rule51 ; rule5index++)
            //         {
            //             if (dataSourceCalVMList[i - rule5index].R > 2 * sigmax)
            //             {
            //                 rule5Flag++;
            //             }
            //             if (dataSourceCalVMList[i - rule5index].R < 2 * sigmax)
            //             {
            //                 rule5Flag++;
            //             }
            //         }
            //         if (rule5Flag == rule52)
            //         {
            //             for (var rule5index = 0; rule5index < rule51; rule5index++)
            //             {
            //                 dataSourceCalVMList[i - rule5index].RuleR5 = true;

            //             }
            //         }
            //     }
            //      if (i > rule61 && rule61 != 0)
            //     {
            //         var rule6Flag = 0;

            //         for (var rule6index = 0; rule6index < rule61; rule6index++)
            //         {
            //             if (dataSourceCalVMList[i - rule6index].R > 2 * sigmax)
            //             {
            //                 rule6Flag++;
            //             }
            //             if (dataSourceCalVMList[i - rule6index].R < 2 * sigmax)
            //             {
            //                 rule6Flag++;
            //             }
            //         }
            //         if (rule6Flag == rule62)
            //         {
            //             for (var rule6index = 0; rule6index < rule61; rule6index++)
            //             {
            //                 dataSourceCalVMList[i - rule6index].RuleR6 = true;

            //             }
            //         }
            //     }
            //     if (i > rule7 && rule7 != 0)
            //     {
            //         var rule7Flag = 0;

            //         for (var rule7index = 0; rule7index < rule7; rule7index++)
            //         {
            //             if (dataSourceCalVMList[i - rule7index].R <  sigmax||dataSourceCalVMList[i - rule7index].R>-sigmax)
            //             {
            //                 rule7Flag++;
            //             }
                      
            //         }
            //         if (rule7Flag == rule7)
            //         {
            //             for (var rule7index = 0; rule7index < rule7; rule7index++)
            //             {
            //                 dataSourceCalVMList[i - rule7index].RuleR7 = true;

            //             }
            //         }
            //     }
            //     if (i > rule8 && rule8 != 0)
            //     {
            //         var rule8Flag = 0;

            //         for (var rule8index = 0; rule8index < rule8; rule8index++)
            //         {
            //             if (dataSourceCalVMList[i - rule8index].R <  sigmax||dataSourceCalVMList[i - rule8index].R>-sigmax)
            //             {
            //                 rule8Flag++;
            //             }
                      
            //         }
            //         if (rule8Flag == rule8)
            //         {
            //             for (var rule8index = 0; rule8index < rule8; rule8index++)
            //             {
            //                 dataSourceCalVMList[i - rule8index].RuleR8 = true;

            //             }
            //         }
            //     }
            // }
            return dataSourceCalVMList;
        }
        // public static List<DataSourceCalRsultVM> CalRule3(List<DataSourceCalRsultVM> dataSourceCalVMList, int rule3=6)
        //  {
        //      List<DataSourceCalRsultVM> dataSourceCalVMList=new List<DataSourceCalRsultVM>
        //      int count=0;
        //      int shiftflag=0;
        //      int breakpoint=0;
        //      int sample6=6;
        //      int temp=0;
        //      DataSourceCalRsultVM temp=new DataSourceCalRsultVM();
        //      while(breakpoint+sample6<dataSourceCalVMList.count())
        //      {
        //         if(shiftflag=0)
        //         {
        //             for(breakpoint<breakpoint+sample6;breakpoint++)
        //             {
        //                 temp=breakpoint;
        //                 if (dataSourceCalVMList[breakpoint]>dataSourceCalVMList[temp])
        //                 {  count++; }
        //                 else
        //                 {
        //                     breakpoint=count;
        //                     count=0;
        //                     shiftflag=1;
        //                     break;
        //                 }
        //                 if(count==rule3)
        //                 {
        //                     for(int index=breakpoint;index<breakpoint+samle6;index++)
        //                     {dataSourceCalVMList[index].rule3=true;}
        //                 }
                            
        //             }
        //         }
        //         if(shiftflag=1)
        //         {
        //              for(breakpoint<breakpoint+sample6;breakpoint++)
        //             {
        //                 {
        //                     temp=breakpoint;
        //                     if (dataSourceCalVMList[begin]<dataSourceCalVMList[temp])
        //                     {  count++; }
        //                     else
        //                     {
        //                         breakpoint=count;
        //                         count=0;
        //                         shiftflag=0;
        //                         break;
        //                     }
        //                     if(count==rule3)
        //                     {
        //                         for(int index=breakpoint;index<breakpoint+samle6;index++)
        //                         {dataSourceCalVMList[index].rule3=true;}
        //                     }
        //                 }
        //             }
        //         }
        //      }

        //  }
    }
}
