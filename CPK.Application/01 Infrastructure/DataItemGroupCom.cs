using System;
using System.Collections.Generic;
using CPK.Domain.ViewModel;
using System.Linq;
using System.Diagnostics;

namespace CPK.Application
{
    public static class DataItemGroupCom
    {
        public static GroupCalSPCResultVM CalBaseValueNoGroup(List<DataSourceItemVM> dataitemList)
        {
            GroupCalSPCResultVM result = new GroupCalSPCResultVM();
            if (dataitemList == null || dataitemList.Count < 1)
            {
                return result;
            }
            result.Sigma = DataItemCalCom.CalSigmax(dataitemList);
            var avg = dataitemList.Average(a => a.CalValue);

            result.UCL = avg + 3 * (result.Sigma / (decimal)Math.Sqrt(dataitemList.Count));
            result.LCL = avg - 3 * (result.Sigma / (decimal)Math.Sqrt(dataitemList.Count));

            return result;
        }

        public static GroupCalSPCResultVM CalBaseValueGroupX(GroupCalSPCResultVM result)
        {
            List<DataSourceCalRsultVM> dataitemList = result.DataSourceGroupList;
            if (dataitemList == null || dataitemList.Count < 1)
            {
                return result;
            }
            var avg = dataitemList.Average(a => a.X);
            result.Sigma = DataItemCalCom.CalXSigmax(dataitemList,avg);
            // result.UCL = avg + 3 * (result.Sigma / (decimal)Math.Sqrt(dataitemList.Count));
            // result.LCL = avg - 3 * (result.Sigma / (decimal)Math.Sqrt(dataitemList.Count));
            result.UCL = avg + 3 * result.Sigma;
            result.LCL = avg - 3 * result.Sigma;
            result.CL = avg;


            return result;
        }

        public static GroupCalSPCResultVM CalBaseValueGroupR(GroupCalSPCResultVM result)
        {
            List<DataSourceCalRsultVM> dataitemList = result.DataSourceGroupList;
            if (dataitemList == null || dataitemList.Count < 1)
            {
                return result;
            }
            var avg = dataitemList.Average(a => a.R);
            result.Sigma = DataItemCalCom.CalRSigmax(dataitemList,avg);
            // result.RUCL = avg + 3 * (result.Sigma / (decimal)Math.Sqrt(dataitemList.Count));
            // result.RLCL = avg - 3 * (result.Sigma / (decimal)Math.Sqrt(dataitemList.Count));
            result.RUCL = avg + 3 * result.Sigma;
            result.RLCL = avg - 3 * result.Sigma;
            result.RCL = avg;

            return result;
        }

        /// <summary>
        /// 按顺序获取样本
        /// </summary>
        /// <param name="dataitemList">原始样本</param>
        /// <param name="groupTotal">单个样本组数量</param>
        /// <returns></returns>
        public static GroupCalSPCResultVM SequenceSortGroupCL(List<DataSourceItemVM> dataitemList, int groupTotal, bool CalXCL = false, bool CalRCL = false)
        {

            if (dataitemList == null || dataitemList.Count < 1)
            {
                return null;
            }
            GroupCalSPCResultVM result = new GroupCalSPCResultVM();

            dataitemList = DataItemSort(dataitemList);


            if (groupTotal <= 1)
            {
                DataSourceCalRsultVM itemresult = new DataSourceCalRsultVM();
                itemresult.DataSourceItemList = dataitemList;
                itemresult.X = dataitemList.Select(s => s.CalValue).Average();
                result.DataSourceGroupList.Add(itemresult);
                return result;
            }
            var skipsum = 0;
            //dataitemList.split
            for (int i = 0; i < dataitemList.Count; i += groupTotal)
            {
                DataSourceCalRsultVM itemresult = new DataSourceCalRsultVM();
                itemresult.DataSourceItemList = dataitemList.Skip(skipsum).Take(groupTotal).ToList();
                if (itemresult.DataSourceItemList != null && itemresult.DataSourceItemList.Count == groupTotal)
                {
                    var templateValueList = itemresult.DataSourceItemList.Select(s => s.CalValue);
                    itemresult.X = templateValueList.Average();
                    itemresult.R = templateValueList.Max() - templateValueList.Min();
                    result.DataSourceGroupList.Add(itemresult);
                }
                skipsum += groupTotal;
            }
            if (CalXCL)
            {
                result = CalBaseValueGroupX(result);
            }
            if (CalRCL)
            {
                result = CalBaseValueGroupR(result);
            }
            return result;
        }
        /// <summary>
        /// 取样方法
        /// </summary>
        /// <param name="dataitemList"></param>
        /// <param name="picktotal">要取多少样本</param>
        /// <param name="groupTotal">样本组</param>
        /// <returns></returns>
        public static GroupCalSPCResultVM PickSortGroupCL(List<DataSourceItemVM> dataitemList, int picktotal, int groupTotal, bool CalXCL = false, bool CalRCL = false)
        {
            if (dataitemList == null || dataitemList.Count < 1)
            {
                return null;
            }
            GroupCalSPCResultVM result = new GroupCalSPCResultVM();
            //GetRandomIntList
            var skipsum = 0;
            if (picktotal <= 0 || groupTotal <= 1)
            {
                DataSourceCalRsultVM itemresult = new DataSourceCalRsultVM();
                itemresult.DataSourceItemList = dataitemList;
                itemresult.X = dataitemList.Select(s => s.CalValue).Average();
                result.DataSourceGroupList.Add(itemresult);
                return result;
            }


            for (int i = 0; i < dataitemList.Count; i += groupTotal)
            {
                DataSourceCalRsultVM itemresult = new DataSourceCalRsultVM();
                var templateList = dataitemList.Skip(skipsum).Take(groupTotal).ToList();
                if (templateList.Count >= groupTotal)
                {
                    var randIntList = GetRandomIntList(groupTotal, picktotal);
                    foreach (var item in randIntList)
                    {
                        itemresult.DataSourceItemList.Add(templateList[item]);
                    }

                }
                else
                {
                    itemresult.DataSourceItemList = templateList;
                }

                if (itemresult.DataSourceItemList != null && itemresult.DataSourceItemList.Count == groupTotal)
                {
                    var templateValueList = itemresult.DataSourceItemList.Select(s => s.CalValue);
                    itemresult.X = templateValueList.Average();
                    itemresult.R = templateValueList.Max() - templateValueList.Min();
                    result.DataSourceGroupList.Add(itemresult);
                }
                skipsum += groupTotal;
            }
            if (CalXCL)
            {
                result = CalBaseValueGroupX(result);
            }
            if (CalRCL)
            {
                result = CalBaseValueGroupR(result);
            }
            return result;
        }
        /// <summary>
        /// 按固定时间获取样本
        /// </summary>
        /// <param name="dataitemList">样本组</param>
        /// <param name="groupTotal">获取样本数量</param>
        /// <param name="minutes">固定时间（分钟）</param>
        /// <returns></returns>
        public static GroupCalSPCResultVM PickSortGroupByTimeCL(List<DataSourceItemVM> dataitemList, int groupTotal, int minutes, bool CalXCL = false, bool CalRCL = false)
        {
            if (dataitemList == null || dataitemList.Count < 1)
            {
                return null;
            }
            if (dataitemList.Where(w => w.TimeValue == null).ToList().Count() > 0)
            {
                return null;
            }
            GroupCalSPCResultVM result = new GroupCalSPCResultVM();

            if (minutes <= 0 || groupTotal <= 1)
            {
                DataSourceCalRsultVM itemresult = new DataSourceCalRsultVM();
                itemresult.DataSourceItemList = dataitemList;
                itemresult.X = dataitemList.Select(s => s.CalValue).Average();
                result.DataSourceGroupList.Add(itemresult);
                return result;
            }
            dataitemList = dataitemList.OrderBy(o => o.TimeValue).ToList();
            var startTime = dataitemList.Select(s => s.TimeValue).Min();
            var endTime = dataitemList.Select(s => s.TimeValue).Max();
            while (startTime < endTime)
            {
                DataSourceCalRsultVM itemresult = new DataSourceCalRsultVM();
                var templateList = dataitemList.Where(w => w.TimeValue > startTime && w.TimeValue <= startTime.Value.AddMinutes(minutes)).ToList();
                if (templateList.Count() <= groupTotal)
                {
                    itemresult.DataSourceItemList = templateList;
                }
                else
                {
                    var randIntList = GetRandomIntList(templateList.Count(), groupTotal);
                    foreach (var item in randIntList)
                    {
                        itemresult.DataSourceItemList.Add(templateList[item]);
                    }

                }

                if (itemresult.DataSourceItemList != null && itemresult.DataSourceItemList.Count == groupTotal)
                {
                    var templateValueList = itemresult.DataSourceItemList.Select(s => s.CalValue);
                    itemresult.X = templateValueList.Average();
                    itemresult.R = templateValueList.Max() - templateValueList.Min();
                    result.DataSourceGroupList.Add(itemresult);
                }
                startTime = startTime.Value.AddMinutes(minutes);
            }
            if (CalXCL)
            {
                result = CalBaseValueGroupX(result);
            }
            if (CalRCL)
            {
                result = CalBaseValueGroupR(result);
            }
            return result;
        }

        /// <summary>
        /// 按顺序获取样本
        /// </summary>
        /// <param name="dataitemList">原始样本</param>
        /// <param name="groupTotal">单个样本组数量</param>
        /// <returns></returns>
        public static List<DataSourceCalRsultVM> SequenceSortGroup(List<DataSourceItemVM> dataitemList, int groupTotal)
        {
            if (dataitemList == null || dataitemList.Count < 1)
            {
                return null;
            }

            dataitemList = DataItemSort(dataitemList);

            List<DataSourceCalRsultVM> result = new List<DataSourceCalRsultVM>();
            if (groupTotal <= 1)
            {
                DataSourceCalRsultVM itemresult = new DataSourceCalRsultVM();
                itemresult.DataSourceItemList = dataitemList;
                itemresult.X = dataitemList.Select(s => s.CalValue).Average();
                result.Add(itemresult);
                return result;
            }
            var skipsum = 0;
            //dataitemList.split
            for (int i = 0; i < dataitemList.Count; i += groupTotal)
            {
                DataSourceCalRsultVM itemresult = new DataSourceCalRsultVM();
                itemresult.DataSourceItemList = dataitemList.Skip(skipsum).Take(groupTotal).ToList();
                if (itemresult.DataSourceItemList != null && itemresult.DataSourceItemList.Count == groupTotal)
                {
                    var templateValueList = itemresult.DataSourceItemList.Select(s => s.CalValue);
                    itemresult.X = templateValueList.Average();
                    itemresult.R = templateValueList.Max() - templateValueList.Min();
                    result.Add(itemresult);
                }
                skipsum += groupTotal;
            }
            return result;
        }
        /// <summary>
        /// 取样方法
        /// </summary>
        /// <param name="dataitemList"></param>
        /// <param name="picktotal">要取多少样本</param>
        /// <param name="groupTotal">样本组</param>
        /// <returns></returns>
        public static List<DataSourceCalRsultVM> PickSortGroup(List<DataSourceItemVM> dataitemList, int picktotal, int groupTotal)
        {
            if (dataitemList == null || dataitemList.Count < 1)
            {
                return null;
            }
            //GetRandomIntList
            var skipsum = 0;
            List<DataSourceCalRsultVM> result = new List<DataSourceCalRsultVM>();
            if (picktotal <= 0 || groupTotal <= 1)
            {
                DataSourceCalRsultVM itemresult = new DataSourceCalRsultVM();
                itemresult.DataSourceItemList = dataitemList;
                itemresult.X = dataitemList.Select(s => s.CalValue).Average();
                result.Add(itemresult);
                return result;
            }


            for (int i = 0; i < dataitemList.Count; i += groupTotal)
            {
                DataSourceCalRsultVM itemresult = new DataSourceCalRsultVM();
                var templateList = dataitemList.Skip(skipsum).Take(groupTotal).ToList();
                if (templateList.Count >= groupTotal && templateList.Count > picktotal)
                {
                    var randIntList = GetRandomIntList(groupTotal, picktotal);
                    foreach (var item in randIntList)
                    {
                        itemresult.DataSourceItemList.Add(templateList[item]);
                    }
                }
                else
                {
                    itemresult.DataSourceItemList = templateList;
                }

                if (itemresult.DataSourceItemList != null && itemresult.DataSourceItemList.Count == groupTotal)
                {
                    var templateValueList = itemresult.DataSourceItemList.Select(s => s.CalValue);
                    itemresult.X = templateValueList.Average();
                    itemresult.R = templateValueList.Max() - templateValueList.Min();
                    result.Add(itemresult);
                }
                skipsum += groupTotal;
            }
            return result;
        }
        /// <summary>
        /// 按固定时间获取样本
        /// </summary>
        /// <param name="dataitemList">样本组</param>
        /// <param name="groupTotal">获取样本数量</param>
        /// <param name="minutes">固定时间（分钟）</param>
        /// <returns></returns>
        public static List<DataSourceCalRsultVM> PickSortGroupByTime(List<DataSourceItemVM> dataitemList, int groupTotal, int minutes)
        {
            if (dataitemList == null || dataitemList.Count < 1)
            {
                return null;
            }
            if (dataitemList.Where(w => w.TimeValue == null).ToList().Count() > 0)
            {
                return null;
            }
            List<DataSourceCalRsultVM> result = new List<DataSourceCalRsultVM>();

            if (minutes <= 0 || groupTotal <= 1)
            {
                DataSourceCalRsultVM itemresult = new DataSourceCalRsultVM();
                itemresult.DataSourceItemList = dataitemList;
                itemresult.X = dataitemList.Select(s => s.CalValue).Average();
                result.Add(itemresult);
                return result;
            }
            dataitemList = dataitemList.OrderBy(o => o.TimeValue).ToList();
            var startTime = dataitemList.Select(s => s.TimeValue).Min();
            var endTime = dataitemList.Select(s => s.TimeValue).Max();
            while (startTime < endTime)
            {
                DataSourceCalRsultVM itemresult = new DataSourceCalRsultVM();
                var templateList = dataitemList.Where(w => w.TimeValue > startTime && w.TimeValue <= startTime.Value.AddMinutes(minutes)).ToList();
                if (templateList.Count() < groupTotal)
                {
                    itemresult.DataSourceItemList = templateList;
                }
                else
                {

                    var randIntList = GetRandomIntList(templateList.Count(), groupTotal);
                    foreach (var item in randIntList)
                    {
                        itemresult.DataSourceItemList.Add(templateList[item]);
                    }
                }

                if (itemresult.DataSourceItemList != null && itemresult.DataSourceItemList.Count == groupTotal)
                {
                    var templateValueList = itemresult.DataSourceItemList.Select(s => s.CalValue);
                    itemresult.X = templateValueList.Average();
                    itemresult.R = templateValueList.Max() - templateValueList.Min();
                    result.Add(itemresult);
                }
                startTime = startTime.Value.AddMinutes(minutes);
            }

            return result;
        }
        public static CPKResultVM CaculateCPK(List<DataSourceItemVM> dataitemList, decimal? lSL = null, decimal? uSL = null)
        {
            if (dataitemList == null || dataitemList.Count <= 1)
            {
                return null;
            }
            var avg = dataitemList.Select(s => s.CalValue).Average();
            double totalavg = 0;
            int count = 0;
            CPKResultVM result = new CPKResultVM();
            result.LSL = Convert.ToDecimal(lSL);
            result.USL = Convert.ToDecimal(uSL);
            var datasortlist = dataitemList.OrderBy(t => t.CalValue);
            foreach (var item in datasortlist)
            {
                totalavg = totalavg + Math.Pow((double)(item.CalValue - avg), 2.0);
                if (result.DistributeStatistics.ContainsKey(item.CalValue))
                {
                    result.DistributeStatistics[item.CalValue] = result.DistributeStatistics[item.CalValue] + 1;
                }
                else
                {
                    result.DistributeStatistics.Add(item.CalValue, 1);
                }
                count++;
            }
            result.Sigma = (decimal)Math.Sqrt(totalavg / count);
            if(result.Sigma==0)
            {
                return result;
            }else
            {
                if (uSL == null && lSL == null)
                {
                    result.CPK = 0;
                }
                if (uSL == null && lSL != null)
                {
                    result.CPK = Math.Abs((avg - Math.Abs(lSL.Value)) / (3 * result.Sigma));
                }
                if (uSL != null && lSL == null)
                {
                    result.CPK = Math.Abs((Math.Abs(uSL.Value) - avg) / (3 * result.Sigma));
                }
                if (uSL != null && lSL != null)
                {
                    List<decimal> cpkValuelist = new List<decimal>();
                    cpkValuelist.Add(Math.Abs((Math.Abs(uSL.Value) - avg) / (3 * result.Sigma)));
                    cpkValuelist.Add(Math.Abs((avg - Math.Abs(lSL.Value)) / (3 * result.Sigma)));

                    result.CPK = cpkValuelist.Min();
                }
                return result;
            }
        }
        public static List<DataSourceCalRsultVM> PickSortGroupByTimeSlot(List<DataSourceItemVM> dataitemList, int groupTotal, DateTime startTime, DateTime endTime)
        {
            if (dataitemList == null || dataitemList.Count < 1)
            {
                return null;
            }

            List<DataSourceCalRsultVM> result = new List<DataSourceCalRsultVM>();
            return result;
        }
        /// <summary>
        /// 排序
        /// </summary>
        /// <param name="acentList"></param>
        /// <returns></returns>
        public static List<DataSourceItemVM> DataItemSort(List<DataSourceItemVM> originalList)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            originalList.OrderBy(o => o.SortValue).ThenByDescending(t => t.TimeValue).ToList();
            sw.Stop();
            var totalTime = sw.ElapsedMilliseconds;
            return originalList.OrderBy(o => o.SortValue).ThenByDescending(t => t.TimeValue).ToList();
        }
        /// <summary>
        /// 在total里随机获取pick个数
        /// </summary>
        /// <param name="total"></param>
        /// <param name="pick"></param>
        /// <returns></returns>
        public static List<int> GetRandomIntList(int total, int pick)
        {
            List<int> result = new List<int>();//取出来的数就放到了这个集合里了
            Random rd = new Random();

            var scope = pick;
            if (total < pick)
            {
                scope = total;
            }
            for (int i = 0; i < pick;)
            {
                int a = rd.Next(0, total);
                if (result.Contains(a))
                    continue;
                result.Add(a);
                i++;
            }
            return result;



        }
    }
}