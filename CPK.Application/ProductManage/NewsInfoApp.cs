using CPK.Domain.Entity.ProductManage;
using CPK.Domain.Entity.SystemManage;
using CPK.Domain.IRepository.ProductManage;
using CPK.Domain.IRepository.SystemManage;
using CPK.Repository.SystemManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CPK.Application.ProductManage
{
    public class NewsInfoApp
    {
        private INewsInfoRepository _service = new NewsInfoRepository();

        public List<NewsInfoEntity> GetList()
        {
            return _service.IQueryable().OrderBy(t => t.F_CreatorTime).ToList();
        }
        public List<NewsInfoEntity> GetList(Expression<Func<NewsInfoEntity, bool>> expression)
        {
            return _service.IQueryable(expression).OrderByDescending(t => t.F_SortCode).ToList();
        }
        public NewsInfoEntity GetForm(string keyValue)
        {
            return _service.FindEntity(keyValue);
        }
        public void RemoveForm(NewsInfoEntity entity)
        {
            entity.Remove();
            _service.Update(entity);
        }
        public void DeleteForm(string keyValue)
        {
          _service.Delete(t => t.F_Id == keyValue);
        }
        public void SubmitForm(NewsInfoEntity entity, string keyValue)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                _service.Update(entity);
            }
            else
            {
                entity.Create();
                _service.Insert(entity);
            }
        }
    }
}
