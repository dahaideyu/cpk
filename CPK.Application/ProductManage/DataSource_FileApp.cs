using CPK.Domain.Entity.ProductManage;
using CPK.Domain.IRepository.ProductManage;
using CPK.Repository.ProductManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CPK.Application.ProductManage
{
    public class DataSource_FileApp
    {
        private IDataSource_FileRepository _service = new DataSource_FileRepository();

        public List<DataSource_FileEntity> GetList()
        {
            return _service.IQueryable().OrderBy(t => t.F_CreatorTime).ToList();
        }
        public List<DataSource_FileEntity> GetList(Expression<Func<DataSource_FileEntity, bool>> expression)
        {
            return _service.IQueryable(expression).OrderByDescending(t => t.F_CreatorTime).ToList();
        }
        public DataSource_FileEntity GetForm(string keyValue)
        {
            return _service.FindEntity(keyValue);
        }
        public void RemoveForm(DataSource_FileEntity entity)
        {
            entity.Remove();
            _service.Update(entity);
        }
        public void DeleteForm(string keyValue)
        {
          _service.Delete(t => t.F_Id == keyValue);
        }
        public void SubmitForm(DataSource_FileEntity entity, string keyValue)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                _service.Update(entity);
            }
            else
            {
                entity.Create();
                _service.Insert(entity);
            }
        }
    }
}
