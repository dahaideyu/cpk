using CPK.Domain.Entity.ProductManage;
using CPK.Domain.IRepository.ProductManage;
using CPK.Repository.ProductManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CPK.Application.ProductManage
{
    public class DataSource_DataBaseApp
    {
        private IDataSource_DataBaseRepository _service = new DataSource_DataBaseRepository();

        public List<DataSource_DataBaseEntity> GetList()
        {
            return _service.IQueryable().OrderBy(t => t.F_CreatorTime).ToList();
        }
        public List<DataSource_DataBaseEntity> GetList(Expression<Func<DataSource_DataBaseEntity, bool>> expression)
        {
            return _service.IQueryable(expression).OrderByDescending(t => t.F_CreatorTime).ToList();
        }
        public DataSource_DataBaseEntity GetForm(string keyValue)
        {
            return _service.FindEntity(keyValue);
        }
        public void RemoveForm(DataSource_DataBaseEntity entity)
        {
            entity.Remove();
            _service.Update(entity);
        }
        public void DeleteForm(string keyValue)
        {
          _service.Delete(t => t.F_Id == keyValue);
        }
        public void SubmitForm(DataSource_DataBaseEntity entity, string keyValue)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                _service.Update(entity);
            }
            else
            {
                entity.Create();
                _service.Insert(entity);
            }
        }
    }
}
