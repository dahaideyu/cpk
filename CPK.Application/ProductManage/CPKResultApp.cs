using CPK.Domain.Entity.ProductManage;
using CPK.Domain.IRepository.ProductManage;
using CPK.Repository.ProductManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace CPK.Application.ProductManage
{
    public class CPKResultApp
    {
        private ICPKResultRepository _service = new CPKResultRepository();
        public List<CPKResultEntity> GetNrepeatList()
        {
            return _service.GetNORepeatList();
        }
        public List<CPKResultEntity> GetList()
        {
            return _service.IQueryable().OrderBy(t => t.F_CreatorTime).ToList();
        }
        public List<CPKResultEntity> GetList(Expression<Func<CPKResultEntity, bool>> expression)
        {
            return _service.IQueryable(expression).OrderByDescending(t => t.F_CreatorTime).ToList();
        }
        public CPKResultEntity GetForm(string keyValue)
        {
            return _service.FindEntity(keyValue);
        }
        public void RemoveForm(CPKResultEntity entity)
        {
            entity.Remove();
            _service.Update(entity);
        }
        public void DeleteForm(string keyValue)
        {
          _service.Delete(t => t.F_Id == keyValue);
        }
        public void SubmitForm(CPKResultEntity entity, string keyValue)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                _service.Update(entity);
            }
            else
            {
                entity.Create();
                _service.Insert(entity);
            }
        }

    }
}
