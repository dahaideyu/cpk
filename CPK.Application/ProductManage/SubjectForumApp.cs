using CPK.Domain.Entity.ProductManage;
using CPK.Domain.IRepository.ProductManage;
using CPK.Repository.ProductManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CPK.Application.ProductManage
{
    public class SubjectForumApp
    {
        private ISubjectForumRepository service = new SubjectForumRepository();

        public List<SubjectForumEntity> GetList()
        {
            return service.IQueryable().OrderBy(t => t.F_CreatorTime).ToList();
        }
        public List<SubjectForumEntity> GetList(Expression<Func<SubjectForumEntity, bool>> expression)
        {
            return service.IQueryable(expression).OrderByDescending(t => t.F_CreatorTime).ToList();
        }
        public SubjectForumEntity GetForm(string keyValue)
        {
            return service.FindEntity(keyValue);
        }
        public void DeleteForm(string keyValue)
        {
            service.Delete(t => t.F_Id == keyValue);
        }
        public void SubmitForm(SubjectForumEntity SubjectForumEntity, string keyValue)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                SubjectForumEntity.Modify(keyValue);
                service.Update(SubjectForumEntity);
            }
            else
            {
                SubjectForumEntity.Create();
                service.Insert(SubjectForumEntity);
            }
        }
    }
}
