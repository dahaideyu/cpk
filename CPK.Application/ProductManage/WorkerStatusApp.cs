using CPK.Domain.Entity.ProductManage;
using CPK.Domain.IRepository.ProductManage;
using CPK.Repository.ProductManage;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CPK.Application.ProductManage
{
    public class WorkerStatusApp
    {
        private IWorkerStatusRepository service = new WorkerStatusRepository();

        public List<WorkerStatusEntity> GetList()
        {
            return service.IQueryable().OrderBy(t => t.F_CreatorTime).ToList();
        }
        public WorkerStatusEntity GetForm(string keyValue)
        {
            return service.FindEntity(keyValue);
        }
        public void DeleteForm(string keyValue)
        {
           service.Delete(t => t.F_Id == keyValue);
        }
        public void SubmitForm(WorkerStatusEntity WorkerStatusEntity, string keyValue)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                WorkerStatusEntity.Modify(keyValue);
                service.Update(WorkerStatusEntity);
            }
            else
            {
                WorkerStatusEntity.Create();
                service.Insert(WorkerStatusEntity);
            }
        }
    }
}
