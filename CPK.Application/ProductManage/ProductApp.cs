using CPK.Domain.Entity.ProductManage;
using CPK.Domain.IRepository.ProductManage;
using CPK.Repository.ProductManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CPK.Application.ProductManage
{
    public class ProductApp
    {
        private IProductRepository service = new ProductRepository();

        public List<ProductEntity> GetList()
        {
            return service.IQueryable().OrderBy(t => t.F_CreatorTime).ToList();
        }
        public List<ProductEntity> GetList(Expression<Func<ProductEntity, bool>> expression)
        {
            return service.IQueryable(expression).OrderByDescending(t => t.F_SortCode).ToList();
        }
        public ProductEntity GetForm(string keyValue)
        {
            return service.FindEntity(keyValue);
        }
        public void DeleteForm(string keyValue)
        {
            service.Delete(t => t.F_Id == keyValue);
        }
        public void SubmitForm(ProductEntity ProductEntity, string keyValue)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                ProductEntity.Modify(keyValue);
                service.Update(ProductEntity);
            }
            else
            {
                ProductEntity.Create();
                service.Insert(ProductEntity);
            }
        }
    }
}
