using CPK.Application.Infrastructure;
using CPK.Domain.Entity.ProductManage;
using CPK.Domain.IRepository.ProductManage;
using CPK.Domain.ViewModel;
using CPK.Repository.ProductManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CPK.Application.ProductManage
{
    public class ChartApp
    {
        private IChartRepository _service = new ChartRepository();
        private Chart_Parameters_SPCApp _SPC =new Chart_Parameters_SPCApp();
        private Chart_Parameters_CPKApp _CPK =new Chart_Parameters_CPKApp();
        private DataSource_MDCSApp _DataSource_MDCSApp = new DataSource_MDCSApp();

        public List<ChartEntity> GetList()
        {
            return _service.IQueryable().OrderBy(t => t.F_CreatorTime).ToList();
        }
        public List<ChartEntity> GetList(Expression<Func<ChartEntity, bool>> expression)
        {
            return _service.IQueryable(expression).OrderByDescending(t => t.F_CreatorTime).ToList();
        }
        public ChartEntity GetForm(string keyValue)
        {
            return _service.FindEntity(keyValue);
        }
        public void RemoveForm(ChartEntity entity)
        {
            entity.Remove();
            _service.Update(entity);
        }
        public void DeleteForm(string keyValue)
        {
          _service.Delete(t => t.F_Id == keyValue);
        }
        public void SubmitForm(ChartEntity entity, string keyValue)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                _service.Update(entity);
            }
            else
            {
                entity.Create();
                _service.Insert(entity);
            }
        }

         public void Update(List<ChartEntity> entities)
        {
            foreach(var item in entities)
            {
                item.Modify(item.F_Id);
            }
            _service.Update(entities);
            
        }

        public DateTime GetFreqDate(ChartEntity chartitem)
        {
            DateTime dtLast= Convert.ToDateTime(chartitem.F_LastLoadTime);
               DateTime dtFreq=dtLast;
               int freq=Convert.ToInt32(chartitem.F_Frequency);
               switch(chartitem.F_FrequencyType)
               {
                    case "S":
                    dtFreq= dtLast.AddSeconds(freq);
                    break;
                    case "MI":
                    dtFreq= dtLast.AddMinutes(freq);
                    break;
                    case "H":
                    dtFreq= dtLast.AddHours(freq);
                    break;
                    case "D":
                    dtFreq= dtLast.AddDays(freq);
                    break;
                    case "W":
                    dtFreq= dtLast.AddDays(freq*7);
                    break;
                    case "M":
                    dtFreq= dtLast.AddMonths(freq);
                    break;
                    case "Y":
                    dtFreq= dtLast.AddYears(freq);
                    break;
                    default:
                    dtFreq= dtLast.AddMinutes(freq);
                    break;                
               }
               return dtFreq;
        }
        
        public DateTime GetMonitorStartDate(ChartEntity chartitem)
        {
                DateTime dtNow=DateTime.Now;
                DateTime dtStart;
               int freq=Convert.ToInt32(chartitem.F_Frequency);
               switch(chartitem.F_FrequencyType)
               {
                    case "S":
                    dtStart= dtNow.AddSeconds(-freq);
                    break;
                    case "MI":
                    dtStart= dtNow.AddMinutes(-freq);
                    break;
                    case "H":
                    dtStart= dtNow.AddHours(-freq);
                    break;
                    case "D":
                    dtStart= dtNow.AddDays(-freq);
                    break;
                    case "W":
                    dtStart= dtNow.AddDays(-freq*7);
                    break;
                    case "M":
                    dtStart= dtNow.AddMonths(-freq);
                    break;
                    case "Y":
                    dtStart= dtNow.AddYears(-freq);
                    break;
                    default:
                    dtStart= dtNow.AddMinutes(-freq);
                    break;                
               }
               return dtStart;
        }

        public void GetChartData(string keyValue,string startTime,string endTime,out GroupCalSPCResultVM GroupResult,out CPKResultVM cpkresult)
        {
            GroupResult=new GroupCalSPCResultVM();
            cpkresult=new CPKResultVM();
            var chartparam = GetForm(keyValue);
            var spcparam = _SPC.GetForm(keyValue);
            var cpkparam = _CPK.GetForm(keyValue);
            List<DataSourceItemVM> result=new List<DataSourceItemVM>();
            DataSource_MDCSEntity dbsource=_DataSource_MDCSApp.GetForm(chartparam.F_DataSourceId);
            string ConnString=dbsource.F_ConnectString;
            string lineName=dbsource.F_Line;
            string deviceID=dbsource.F_Device;
            string varID=dbsource.F_Variable;
            string MCF= dbsource.F_MCF;
            string BoardType = dbsource.F_BoardType;
            DataSourceFromMDCS MDCSDB=new DataSourceFromMDCS(ConnString);
            DateTime dStartTime = Convert.ToDateTime(startTime);
            DateTime dEndTime = Convert.ToDateTime(endTime);
            string[] DirtyRead=null;
            if(!string.IsNullOrWhiteSpace(chartparam.F_DirtyRead))
            {
                DirtyRead=chartparam.F_DirtyRead.Trim().TrimEnd(',').Split(',');
            }
            result = MDCSDB.getMDCSDataSource(lineName,deviceID,varID,dStartTime,dEndTime,DirtyRead,MCF,BoardType);
            if(result.Count>0)
            {
                bool XautoCL=Convert.ToBoolean(spcparam.F_XAutoCL);
                bool RautoCL=Convert.ToBoolean(spcparam.F_RAutoCL);
                switch(spcparam.F_GroupType)
                {
                    case 0:
                        GroupResult = DataItemGroupCom.SequenceSortGroupCL(result,Convert.ToInt32(spcparam.F_GroupNum),XautoCL,RautoCL);
                        break;
                    case 1:
                        GroupResult = DataItemGroupCom.PickSortGroupCL(result,Convert.ToInt32(spcparam.F_GroupNum),Convert.ToInt32(spcparam.F_GroupSplit),XautoCL,RautoCL);
                        break;
                    case 2:
                        GroupResult = DataItemGroupCom.PickSortGroupByTimeCL(result,Convert.ToInt32(spcparam.F_GroupNum),Convert.ToInt32(spcparam.F_GroupSplit),XautoCL,RautoCL);
                        break;
                }
                if(!XautoCL)
                {
                    GroupResult.LCL=Convert.ToDecimal(spcparam.F_XLCL);
                    GroupResult.CL=Convert.ToDecimal(spcparam.F_XCL);
                    GroupResult.UCL=Convert.ToDecimal(spcparam.F_XUCL);
                }
                if(!RautoCL)
                {
                    GroupResult.RLCL=Convert.ToDecimal(spcparam.F_RLCL);
                    GroupResult.RCL=Convert.ToDecimal(spcparam.F_RCL);
                    GroupResult.RUCL=Convert.ToDecimal(spcparam.F_RUCL);
                }
                int rule1 = 1; 
                int rule2 = 9; 
                int rule3 = 6; 
                int rule4 = 14; 
                int rule51 = 3; 
                int rule52 = 2;
                int rule61=5;
                int rule62=4;
                int rule7=15;
                int rule8=8;
                rule1=Convert.ToBoolean(spcparam.F_Rule1)?rule1:0;
                rule2=Convert.ToBoolean(spcparam.F_Rule2)?rule2:0;
                rule3=Convert.ToBoolean(spcparam.F_Rule3)?rule3:0;
                rule4=Convert.ToBoolean(spcparam.F_Rule4)?rule4:0;
                rule51=Convert.ToBoolean(spcparam.F_Rule5)?rule51:0;
                rule52=Convert.ToBoolean(spcparam.F_Rule5)?rule52:0;
                rule61=Convert.ToBoolean(spcparam.F_Rule6)?rule61:0;
                rule62=Convert.ToBoolean(spcparam.F_Rule6)?rule62:0;
                rule7=Convert.ToBoolean(spcparam.F_Rule7)?rule7:0;
                rule8=Convert.ToBoolean(spcparam.F_Rule7)?rule8:0;
                GroupResult.DataSourceGroupList=DataItemCalCom.CalRule(GroupResult.DataSourceGroupList
                ,GroupResult.LCL,GroupResult.UCL,GroupResult.RLCL,GroupResult.RUCL
                ,rule1,rule2,rule3,rule4,rule51,rule52,rule61,rule62,rule7,rule8);
                cpkresult=DataItemGroupCom.CaculateCPK(result,Convert.ToDecimal(cpkparam.F_LSL),Convert.ToDecimal(cpkparam.F_USL));
            }
            else
            {
                GroupResult=null;
                cpkresult=null;
            }
        }
    }
}
