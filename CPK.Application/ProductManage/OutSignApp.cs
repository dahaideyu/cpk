using CPK.Domain.Entity.ProductManage;
using CPK.Domain.IRepository.ProductManage;
using CPK.Repository.ProductManage;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CPK.Application.ProductManage
{
    public class OutSignApp
    {
        private IOutSignRepository service = new OutSignRepository();

        public List<OutSignEntity> GetList()
        {
            return service.IQueryable().OrderBy(t => t.F_CreatorTime).ToList();
        }
        public OutSignEntity GetForm(string keyValue)
        {
            return service.FindEntity(keyValue);
        }
        public void DeleteForm(string keyValue)
        {
            service.Delete(t => t.F_Id == keyValue);
        }
        public void SubmitForm(OutSignEntity OutSignEntity, string keyValue)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                OutSignEntity.Modify(keyValue);
                service.Update(OutSignEntity);
            }
            else
            {
                OutSignEntity.Create();
                service.Insert(OutSignEntity);
            }
        }
    }
}
