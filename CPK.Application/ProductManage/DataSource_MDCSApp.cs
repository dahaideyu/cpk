using CPK.Domain.Entity.ProductManage;
using CPK.Domain.IRepository.ProductManage;
using CPK.Repository.ProductManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using CPK.Application.Infrastructure;

namespace CPK.Application.ProductManage
{
    public class DataSource_MDCSApp
    {
        private IDataSource_MDCSRepository _service = new DataSource_MDCSRepository();

        public List<DataSource_MDCSEntity> GetList()
        {
            return _service.IQueryable().OrderBy(t => t.F_CreatorTime).ToList();
        }
        public List<DataSource_MDCSEntity> GetList(Expression<Func<DataSource_MDCSEntity, bool>> expression)
        {
            return _service.IQueryable(expression).OrderByDescending(t => t.F_CreatorTime).ToList();
        }
        public DataSource_MDCSEntity GetForm(string keyValue)
        {
            return _service.FindEntity(keyValue);
        }
        public void RemoveForm(DataSource_MDCSEntity entity)
        {
            entity.Remove();
            _service.Update(entity);
        }
        public void DeleteForm(string keyValue)
        {
          _service.Delete(t => t.F_Id == keyValue);
        }
        public void SubmitForm(DataSource_MDCSEntity entity, string keyValue)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                _service.Update(entity);
            }
            else
            {
                entity.Create();
                _service.Insert(entity);
            }
        }

        public string GetDeviceName(string sConn,string sLine,string sDevice)
        {
            var list = new DataSourceFromMDCS(sConn).getDevicesList("",sLine).Select("DeviceID="+sDevice);
            if(list.Length>0)
                return Convert.ToString(list[0]["Name"]);
            else
                return "MDCS数据异动"+System.Environment.NewLine+"DeviceID:"+sDevice;
        }   

        public string GetVarName(string sConn,string sDevice,string sVarID)
        {
            var list = new DataSourceFromMDCS(sConn).getVariablesList("",sDevice).Select("VarID="+sVarID);
            if(list.Length>0)
                return Convert.ToString(list[0]["Name"]);
            else
                return "MDCS数据异动"+System.Environment.NewLine+"sVarID:"+sVarID;
        }  
    }
}
