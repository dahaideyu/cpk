using CPK.Domain.Entity.ProductManage;
using CPK.Domain.IRepository.ProductManage;
using CPK.Repository.ProductManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CPK.Application.ProductManage
{
    public class Chart_Parameters_SPCApp
    {
        private IChart_Parameters_SPCRepository _service = new Chart_Parameters_SPCRepository();

        public List<Chart_Parameters_SPCEntity> GetList()
        {
            return _service.IQueryable().OrderBy(t => t.F_CreatorTime).ToList();
        }
        public List<Chart_Parameters_SPCEntity> GetList(Expression<Func<Chart_Parameters_SPCEntity, bool>> expression)
        {
            return _service.IQueryable(expression).OrderByDescending(t => t.F_CreatorTime).ToList();
        }
        public Chart_Parameters_SPCEntity GetForm(string keyValue)
        {
            return _service.FindEntity(keyValue);
        }
        public void RemoveForm(Chart_Parameters_SPCEntity entity)
        {
            entity.Remove();
            _service.Update(entity);
        }
        public void DeleteForm(string keyValue)
        {
          _service.Delete(t => t.F_Id == keyValue);
        }
        public void SubmitForm(Chart_Parameters_SPCEntity entity, string keyValue)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                _service.Update(entity);
            }
            else
            {
                entity.Create();
                entity.F_Id = entity.F_ChartId;
                _service.Insert(entity);
            }
        }
    }
}
