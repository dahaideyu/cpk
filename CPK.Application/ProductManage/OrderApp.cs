using CPK.Domain.Entity.ProductManage;
using CPK.Domain.IRepository.ProductManage;
using CPK.Repository.ProductManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CPK.Application.ProductManage
{
    public class OrderApp
    {
        private IOrderRepository service = new OrderRepository();

        public List<OrderEntity> GetList()
        {
            return service.IQueryable().OrderBy(t => t.F_CreatorTime).ToList();
        }
        public List<OrderEntity> GetList(Expression<Func<OrderEntity, bool>> expression)
        {
            return service.IQueryable(expression).OrderByDescending(t => t.F_SortCode).ToList();
        }
        public OrderEntity GetForm(string keyValue)
        {
            return service.FindEntity(keyValue);
        }
        public void RemoveForm(OrderEntity entity)
        {
            entity.Remove();
            service.Update(entity);
        }
        public void DeleteForm(string keyValue)
        {
          service.Delete(t => t.F_Id == keyValue);
        }
        public void SubmitForm(OrderEntity entity, string keyValue)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                service.Update(entity);
            }
            else
            {
                entity.Create();
                service.Insert(entity);
            }
        }
    }
}
