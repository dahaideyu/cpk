using CPK.Domain.Entity.ProductManage;
using CPK.Domain.IRepository.ProductManage;
using CPK.Repository.ProductManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CPK.Application.ProductManage
{
    public class SubjectComplaintsApp
    {
        private ISubjectComplaintsRepository _service = new SubjectComplaintsRepository();

        public List<SubjectComplaintsEntity> GetList()
        {
            return _service.IQueryable().OrderBy(t => t.F_CreatorTime).ToList();
        }
        public List<SubjectComplaintsEntity> GetList(Expression<Func<SubjectComplaintsEntity, bool>> expression)
        {
            return _service.IQueryable(expression).OrderByDescending(t => t.F_SortCode).ToList();
        }
        public SubjectComplaintsEntity GetForm(string keyValue)
        {
            return _service.FindEntity(keyValue);
        }
        public void RemoveForm(SubjectComplaintsEntity entity)
        {
            entity.Remove();
            _service.Update(entity);
        }
        public void DeleteForm(string keyValue)
        {
          _service.Delete(t => t.F_Id == keyValue);
        }
        public void SubmitForm(SubjectComplaintsEntity entity, string keyValue)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                _service.Update(entity);
            }
            else
            {
                entity.Create();
                _service.Insert(entity);
            }
        }
    }
}
