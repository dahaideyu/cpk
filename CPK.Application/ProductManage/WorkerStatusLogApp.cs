using CPK.Domain.Entity.ProductManage;
using CPK.Domain.IRepository.ProductManage;
using CPK.Repository.ProductManage;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CPK.Application.ProductManage
{
    public class WorkerStatusLogApp
    {
        private IWorkerStatusLogRepository service = new WorkerStatusLogRepository();

        public List<WorkerStatusLogEntity> GetList()
        {
            return service.IQueryable().OrderBy(t => t.F_CreatorTime).ToList();
        }
        public WorkerStatusLogEntity GetForm(string keyValue)
        {
            return service.FindEntity(keyValue);
        }
        public void DeleteForm(string keyValue)
        {
            service.Delete(t => t.F_Id == keyValue);
        }
        public void SubmitForm(WorkerStatusLogEntity WorkerStatusLogEntity, string keyValue)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                WorkerStatusLogEntity.Modify(keyValue);
                service.Update(WorkerStatusLogEntity);
            }
            else
            {
                WorkerStatusLogEntity.Create();
                service.Insert(WorkerStatusLogEntity);
            }
        }
    }
}
