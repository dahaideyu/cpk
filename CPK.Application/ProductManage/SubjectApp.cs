using CPK.Domain.Entity.ProductManage;
using CPK.Domain.IRepository.ProductManage;
using CPK.Repository.ProductManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CPK.Application.ProductManage
{
    public class SubjectApp
    {
        private ISubjectRepository service = new SubjectRepository();

        public List<SubjectEntity> GetList()
        {
            return service.IQueryable().OrderBy(t => t.F_CreatorTime).ToList();
        }
        public List<SubjectEntity> GetList(Expression<Func<SubjectEntity, bool>> expression)
        {
            return service.IQueryable(expression).OrderByDescending(t => t.F_SortCode).ToList();
        }
        public SubjectEntity GetForm(string keyValue)
        {
            return service.FindEntity(keyValue);
        }
        public void DeleteForm(string keyValue)
        {
            service.Delete(t => t.F_Id == keyValue);
        }
        public void SubmitForm(SubjectEntity SubjectEntity, string keyValue)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                SubjectEntity.Modify(keyValue);
                service.Update(SubjectEntity);
            }
            else
            {
                SubjectEntity.Create();
                service.Insert(SubjectEntity);
            }
        }
    }
}
