﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CPK.Domain.ViewModel
{
    public class APIUserVM
    {
        /// <summary>
        /// 用户主键
        /// <summary>
        public string F_Id { get; set; }
        /// <summary>
        /// 账户
        /// <summary>
        public string F_Account { get; set; }
        /// <summary>
        /// 姓名
        /// <summary>
        public string F_RealName { get; set; }
        /// <summary>
        /// 呢称
        /// <summary>
        public string F_NickName { get; set; }
        /// <summary>
        /// 头像
        /// <summary>
        public string F_HeadIcon { get; set; }
        /// <summary>
        /// 性别
        /// <summary>
        public bool? F_Gender { get; set; }
        /// <summary>
        /// 生日
        /// <summary>
        public DateTime? F_Birthday { get; set; }
        /// <summary>
        /// 手机
        /// <summary>
        public string F_MobilePhone { get; set; }
        /// <summary>
        /// 邮箱
        /// <summary>
        public string F_Email { get; set; }
        /// <summary>
        /// 微信
        /// <summary>
        public string F_WeChat { get; set; }
        /// <summary>
        /// 主管主键
        /// <summary>
        public string F_ManagerId { get; set; }
        /// <summary>
        /// 安全级别
        /// <summary>
        public int? F_SecurityLevel { get; set; }
        /// <summary>
        /// 个性签名
        /// <summary>
        public string F_Signature { get; set; }
        /// <summary>
        /// 组织主键
        /// <summary>
        public string F_OrganizeId { get; set; }
        /// <summary>
        /// 组织名称
        /// <summary>
        public string F_OrganizeName { get; set; }
        /// <summary>
        /// 部门主键
        /// <summary>
        public string F_DepartmentId { get; set; }
        /// <summary>
        /// 部门名称
        /// <summary>
        public string F_DepartmentName { get; set; }
        /// <summary>
        /// 角色主键
        /// <summary>
        public string F_RoleId { get; set; }
        /// <summary>
        /// 角色名称
        /// <summary>
        public string F_RoleName { get; set; }
        /// <summary>
        /// 0是客户1材料商2销售3设计师4施工人员5监理6管理者7财务8项目经理
        /// <summary>
        public string F_UserType { get; set; }

        /// <summary>
        /// 岗位主键
        /// <summary>
        public string F_DutyId { get; set; }

        /// <summary>
        /// 岗位名称
        /// <summary>
        public string F_DutyName { get; set; }
        /// <summary>
        /// 是否管理员
        /// <summary>
        public bool? F_IsAdministrator { get; set; }
        /// <summary>
        /// 排序码
        /// <summary>
        public int? F_SortCode { get; set; }
        /// <summary>
        /// 删除标志
        /// <summary>
        public bool? F_DeleteMark { get; set; }
        /// <summary>
        /// 有效标志
        /// <summary>
        public bool? F_EnabledMark { get; set; }
        /// <summary>
        /// 描述
        /// <summary>
        public string F_Description { get; set; }
    }
}
