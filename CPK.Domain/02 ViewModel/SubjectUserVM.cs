﻿using CPK.Domain.Entity.SystemManage;
using System;
using System.Collections.Generic;
using System.Text;

namespace CPK.Domain.ViewModel
{
    public class SubjectUserVM
    {
        public List<ItemsDetailEntity> ItemDetailList { get; set; } = new List<ItemsDetailEntity>();
        public List<UserEntity> ClientList { get; set; } = new List<UserEntity>();
        public List<UserEntity> DesignList { get; set; } = new List<UserEntity>();
        public List<UserEntity> ImplementList { get; set; } = new List<UserEntity>();
        public List<UserEntity> ManagerList { get; set; } = new List<UserEntity>();
        public List<UserEntity> IPQCList { get; set; } = new List<UserEntity>();



    }
}
