using System;
using System.Collections.Generic;
using System.Text;

namespace CPK.Domain.ViewModel
{
    public class DataSourceItemVM
    {
        /// <summary>
        /// 关键字段
        /// </summary>
        public DataSourceItemVM()
        {
            TagList=new List<DataSourceItemTagVM>();
        }
        public string KeyValue { get; set; }
        /// <summary>
        /// 计算字段
        /// </summary>
        public decimal CalValue { get; set; }
        /// <summary>
        /// 时间字段
        /// </summary>
        public DateTime? TimeValue { get; set; }
        /// <summary>
        /// 排序字段
        /// </summary>
        public string SortValue { get; set; }
        /// <summary>
        /// 标注列表
        /// </summary>
        public List<DataSourceItemTagVM> TagList { get; set; }
    }

    public class DataSourceItemTagVM
    {
        /// <summary>
        /// 标注名称
        /// </summary>
        public DataSourceItemTagVM(string name, string value)
        {
            this.Name = name;
            this.Value = value;

        }
        public string Name { get; set; }
        /// <summary>
        /// 标注值
        /// </summary>
        public string Value { get; set; }
    }
}
