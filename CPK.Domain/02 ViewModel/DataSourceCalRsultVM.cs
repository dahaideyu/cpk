using System.Collections.Generic;

namespace CPK.Domain.ViewModel
{
    public class DataSourceCalRsultVM
    {
        public DataSourceCalRsultVM()
        {
            DataSourceItemList = new List<DataSourceItemVM>();
        }
        public List<DataSourceItemVM> DataSourceItemList { get; set; }
        /// <summary>
        /// 均值
        /// </summary>
        public decimal X { get; set; }
        public bool Rule1 { get; set; } = false;
        public bool Rule2 { get; set; } = false;
        public bool Rule3 { get; set; } = false;
        public bool Rule4 { get; set; } = false;
        public bool Rule5 { get; set; } = false;
        public bool Rule6 { get; set; } = false;
        public bool Rule7 { get; set; } = false;
        public bool Rule8 { get; set; } = false;
        public bool RuleR1 { get; set; } = false;
        public bool RuleR2 { get; set; } = false;
        public bool RuleR3 { get; set; } = false;
        public bool RuleR4 { get; set; } = false;
        public bool RuleR5 { get; set; } = false;
        public bool RuleR6 { get; set; } = false;
        public bool RuleR7 { get; set; } = false;
        public bool RuleR8 { get; set; } = false;
        public decimal UCL { get; set; }
        public decimal LCL { get; set; }
        public decimal USL { get; set; }
        public decimal LSL { get; set; }
        public decimal CPK { get; set; }
        /// <summary>
        /// sigmax
        /// </summary>
        public decimal Sigmax { get; set; }
        /// <summary>
        ///极差 
        /// </summary>
        public decimal R { get; set; }

    }
    public class GroupCalSPCResultVM
    {
        public List<DataSourceCalRsultVM> DataSourceGroupList { get; set; } = new List<DataSourceCalRsultVM>();
        public decimal Sigma { get; set; }
        public decimal UCL { get; set; } = 0;
        public decimal CL { get; set; } = 0;
        public decimal LCL { get; set; } = 0;
        public decimal RUCL { get; set; } = 0;
        public decimal RCL { get; set; } = 0;
        public decimal RLCL { get; set; } = 0;
    }
}