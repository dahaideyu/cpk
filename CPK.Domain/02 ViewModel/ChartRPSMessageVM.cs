using System;
using System.Collections.Generic;
using System.Text;

namespace CPK.Domain.ViewModel
{
    public class ChartRPSMessageVM
    {
        /// <summary>
        /// 图表Id
        /// </summary>
        public string ChartId { get; set; }
        /// <summary>
        /// 图表名称
        /// </summary>
        public string ChartName { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime? StartTime { get; set; }
         /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime? EndTime { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
    }
}
