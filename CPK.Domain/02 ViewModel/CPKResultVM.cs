﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CPK.Domain.ViewModel
{
    public class CPKResultVM
    {     
        public decimal CPK { get; set; } = 0;
        public decimal LSL { get; set; } = 0;
        public decimal USL { get; set; } = 0;
        public decimal Sigma { get; set; } = 0;
        public Dictionary<decimal, int> DistributeStatistics { get; set; } = new Dictionary<decimal, int>();
    }
}
