using System;

namespace CPK.Domain.Entity.ProductManage
{
    public class Chart_Parameters_SPCEntity : IEntity<Chart_Parameters_SPCEntity>, ICreationAudited, IDeleteAudited, IModificationAudited
    {
             /// <summary>
        /// 主键
        /// <summary>
        public string F_Id { get; set; }
        /// <summary>
        /// 编码
        /// <summary>
        public string F_ChartId { get; set; }
        /// <summary>
        /// SPC 分组方式
        /// <summary>
        public int? F_GroupType { get; set; }
        /// <summary>
        /// SPC 分组大小
        /// <summary>
        public int? F_GroupNum { get; set; }
        /// <summary>
        /// SPC 取样大小
        /// <summary>
        public int? F_GroupSplit { get; set; }
        /// <summary>
        /// X bar chart auto caculate UCL CL LCL
        /// <summary>
        public bool? F_XAutoCL { get; set; }
        /// <summary>
        /// X bar chart LCL
        /// <summary>
        public double? F_XLCL { get; set; }
        /// <summary>
        /// X bar chart CL
        /// <summary>
        public double? F_XCL { get; set; }
        /// <summary>
        /// X bar chart UCL
        /// <summary>
        public double? F_XUCL { get; set; }
        /// <summary>
        /// R chart auto caculate UCL CL LCL
        /// <summary>
        public bool? F_RAutoCL { get; set; }
        /// <summary>
        /// R chart LCL
        /// <summary>
        public double? F_RLCL { get; set; }
        /// <summary>
        /// R chart CL
        /// <summary>
        public double? F_RCL { get; set; }
        /// <summary>
        /// R chart UCL
        /// <summary>
        public double? F_RUCL { get; set; }
        /// <summary>
        /// 1个点落在A区以外
        /// <summary>
        public bool? F_Rule1 { get; set; }
        /// <summary>
        /// 连续9个点落在中心线的同一侧
        /// <summary>
        public bool? F_Rule2 { get; set; }
        /// <summary>
        /// 连续6个点递增或递减
        /// <summary>
        public bool? F_Rule3 { get; set; }
        /// <summary>
        /// 连续14个点中相邻点交替上下
        /// <summary>
        public bool? F_Rule4 { get; set; }
        /// <summary>
        /// 连续3个点中有2个点落在中心线同一侧的B区以外
        /// <summary>
        public bool? F_Rule5 { get; set; }
        /// <summary>
        /// 连续5个点中有4个点落在中心线同一侧的C区以外
        /// <summary>
        public bool? F_Rule6 { get; set; }
        /// <summary>
        /// 连续15个点落在中心线两侧的C区以内
        /// <summary>
        public bool? F_Rule7 { get; set; }
        /// <summary>
        /// 连续8个点落在中心线两侧且无一在C区内
        /// <summary>
        public bool? F_Rule8 { get; set; }
        /// <summary>
        /// 豁免周期(天)
        /// <summary>
        public int? F_ExemptionDay { get; set; }
        /// <summary>
        /// 排序码
        /// <summary>
        public int? F_SortCode { get; set; }
        /// <summary>
        /// 有效标志
        /// <summary>
        public bool? F_EnabledMark { get; set; }
        /// <summary>
        /// 创建日期
        /// <summary>
        public DateTime? F_CreatorTime { get; set; }
        /// <summary>
        /// 创建用户主键
        /// <summary>
        public string F_CreatorUserId { get; set; }
        /// <summary>
        /// 最后修改时间
        /// <summary>
        public DateTime? F_LastModifyTime { get; set; }
        /// <summary>
        /// 最后修改用户
        /// <summary>
        public string F_LastModifyUserId { get; set; }
        /// <summary>
        /// 删除标志
        /// <summary>
        public bool? F_DeleteMark { get; set; }
        /// <summary>
        /// 删除时间
        /// <summary>
        public DateTime? F_DeleteTime { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_DeleteUserId { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_CreatorUserName { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_DeleteUserName { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_LastModifyUserName { get; set; }

    }
}
