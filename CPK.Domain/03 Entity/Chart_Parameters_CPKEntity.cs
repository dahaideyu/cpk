using System;

namespace CPK.Domain.Entity.ProductManage
{
    public class Chart_Parameters_CPKEntity : IEntity<Chart_Parameters_CPKEntity>, ICreationAudited, IDeleteAudited, IModificationAudited
    {
             /// <summary>
        /// 主键
        /// <summary>
        public string F_Id { get; set; }
        /// <summary>
        /// 编码
        /// <summary>
        public string F_ChartId { get; set; }
        /// <summary>
        /// LSL
        /// <summary>
        public double? F_LSL { get; set; }
        /// <summary>
        /// USL
        /// <summary>
        public double? F_USL { get; set; }
        /// <summary>
        /// 警告范围最小值
        /// <summary>
        public double? F_WarnMin { get; set; }
        /// <summary>
        /// 警告范围最大值
        /// <summary>
        public double? F_WarnMax { get; set; }
        /// <summary>
        /// 报警范围最小值
        /// <summary>
        public double? F_FailMin { get; set; }
        /// <summary>
        /// 报警范围最大值
        /// <summary>
        public double? F_FailMax { get; set; }
        /// <summary>
        /// 排序码
        /// <summary>
        public int? F_SortCode { get; set; }
        /// <summary>
        /// 豁免周期(天)
        /// <summary>
        public int? F_ExemptionDay { get; set; }
        /// <summary>
        /// CPK line  取样频率
        /// </summary>
        public int? F_Frequency { get; set; }
        /// <summary>
        /// 有效标志
        /// <summary>
        public bool? F_EnabledMark { get; set; }
        /// <summary>
        /// 创建日期
        /// <summary>
        public DateTime? F_CreatorTime { get; set; }
        /// <summary>
        /// 创建用户主键
        /// <summary>
        public string F_CreatorUserId { get; set; }
        /// <summary>
        /// 最后修改时间
        /// <summary>
        public DateTime? F_LastModifyTime { get; set; }
        /// <summary>
        /// 最后修改用户
        /// <summary>
        public string F_LastModifyUserId { get; set; }
        /// <summary>
        /// 删除标志
        /// <summary>
        public bool? F_DeleteMark { get; set; }
        /// <summary>
        /// 删除时间
        /// <summary>
        public DateTime? F_DeleteTime { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_DeleteUserId { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_CreatorUserName { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_DeleteUserName { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_LastModifyUserName { get; set; }

    }
}
