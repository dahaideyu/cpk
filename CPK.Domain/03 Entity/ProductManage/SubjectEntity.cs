using System;

namespace CPK.Domain.Entity.ProductManage
{
    public class SubjectEntity : IEntity<SubjectEntity>, ICreationAudited, IDeleteAudited, IModificationAudited
    {
        /// <summary>
        /// 主键
        /// <summary>
        public string F_Id { get; set; }
        /// <summary>
        /// 编码
        /// <summary>
        public string F_EnCode { get; set; }
        /// <summary>
        /// 名称
        /// <summary>
        public string F_FullName { get; set; }
        /// <summary>
        /// 区域
        /// <summary>
        public string F_WorkAreaId { get; set; }
        /// <summary>
        /// 类型
        /// <summary>
        public string F_Type { get; set; }
        /// <summary>
        /// 地址
        /// <summary>
        public string F_Address { get; set; }
        /// <summary>
        /// 描述
        /// <summary>
        public string F_Description { get; set; }
        /// <summary>
        /// 小图
        /// <summary>
        public string F_MinImage { get; set; }
        /// <summary>
        /// 小图
        /// <summary>
        public string F_Image01 { get; set; }
        /// <summary>
        /// 图2
        /// <summary>
        public string F_Image02 { get; set; }
        /// <summary>
        /// 图2
        /// <summary>
        public string F_Image03 { get; set; }
        /// <summary>
        /// 图2
        /// <summary>
        public string F_Image04 { get; set; }
        /// <summary>
        /// 客户
        /// <summary>
        public string F_ClientId { get; set; }
        /// <summary>
        /// 施工人员
        /// <summary>
        public string F_ImplementId { get; set; }
        /// <summary>
        /// 设计者
        /// <summary>
        public string F_DesignId { get; set; }
        /// <summary>
        /// 项目管理者
        /// <summary>
        public string F_ManageId { get; set; }
        /// <summary>
        /// 质量监督员
        /// <summary>
        public string F_IPQC { get; set; }
        /// <summary>
        /// 状态
        /// <summary>
        public string F_Status { get; set; }
        /// <summary>
        /// 所属人
        /// <summary>
        public string F_OwnerId { get; set; }
        /// <summary>
        /// 排序码
        /// <summary>
        public int? F_SortCode { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public bool? F_DeleteMark { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public int? F_EnabledMark { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public DateTime? F_CreatorTime { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public string F_CreatorUserId { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public DateTime? F_LastModifyTime { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public string F_LastModifyUserId { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public DateTime? F_DeleteTime { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public string F_DeleteUserId { get; set; }
        /// <summary>
        /// 创建操作用户名
        /// <summary>
        public string F_CreatorUserName { get; set; }
        /// <summary>
        /// 删除操作用户
        /// <summary>
        public string F_DeleteUserName { get; set; }
        /// <summary>
        /// 最后修改操作用户
        /// <summary>
        public string F_LastModifyUserName { get; set; }
    }
}
