using System;

namespace CPK.Domain.Entity.ProductManage
{
    public class CPKCaseEntity : IEntity<CPKCaseEntity>, ICreationAudited, IDeleteAudited, IModificationAudited
    {
             /// <summary>
        /// 主键
        /// <summary>
        public string F_Id { get; set; }
        /// <summary>
        /// 编码
        /// <summary>
        public string F_EnCode { get; set; }
        /// <summary>
        /// 名称
        /// <summary>
        public string F_FullName { get; set; }
        /// <summary>
        /// 名称
        /// <summary>
        public string F_Type { get; set; }
        /// <summary>
        /// 显示列表图
        /// <summary>
        public string F_Image { get; set; }
        /// <summary>
        /// 小图
        /// <summary>
        public string F_MInImage { get; set; }
        /// <summary>
        /// 详细图1
        /// <summary>
        public string F_MaxImage01 { get; set; }
        /// <summary>
        /// 详细图2
        /// <summary>
        public string F_MaxImage02 { get; set; }
        /// <summary>
        /// 详细图3
        /// <summary>
        public string F_MaxImage03 { get; set; }
        /// <summary>
        /// 详细图4
        /// <summary>
        public string F_MaxImage04 { get; set; }
        /// <summary>
        /// 排序码
        /// <summary>
        public int? F_SortCode { get; set; }
        /// <summary>
        /// 删除标志
        /// <summary>
        public bool? F_DeleteMark { get; set; }
        /// <summary>
        /// 有效标志
        /// <summary>
        public bool? F_EnabledMark { get; set; }
        /// <summary>
        /// 描述
        /// <summary>
        public string F_Description { get; set; }
        /// <summary>
        /// 创建日期
        /// <summary>
        public DateTime? F_CreatorTime { get; set; }
        /// <summary>
        /// 创建用户主键
        /// <summary>
        public string F_CreatorUserId { get; set; }
        /// <summary>
        /// 最后修改时间
        /// <summary>
        public DateTime? F_LastModifyTime { get; set; }
        /// <summary>
        /// 最后修改用户
        /// <summary>
        public string F_LastModifyUserId { get; set; }
        /// <summary>
        /// 删除时间
        /// <summary>
        public DateTime? F_DeleteTime { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_DeleteUserId { get; set; }
        /// <summary>
        /// 创建操作用户名
        /// <summary>
        public string F_CreatorUserName { get; set; }
        /// <summary>
        /// 删除操作用户
        /// <summary>
        public string F_DeleteUserName { get; set; }
        /// <summary>
        /// 最后修改操作用户
        /// <summary>
        public string F_LastModifyUserName { get; set; }

    }
}
