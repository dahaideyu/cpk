using System;

namespace CPK.Domain.Entity.ProductManage
{
    public class WorkerStatusEntity : IEntity<WorkerStatusEntity>, ICreationAudited, IDeleteAudited, IModificationAudited
    {
             /// <summary>
        /// 
        /// <summary>
        public string F_Id { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public string F_UserId { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public string F_UserStatus { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public string F_WorkAreaId { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public bool? F_DeleteMark { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public int F_EnabledMark { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public DateTime? F_CreatorTime { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public string F_CreatorUserId { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public DateTime? F_LastModifyTime { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public string F_LastModifyUserId { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public DateTime? F_DeleteTime { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public string F_DeleteUserId { get; set; }
        public string F_CreatorUserName { get ; set  ; }
        public string F_DeleteUserName { get ; set ; }
        public string F_LastModifyUserName { get ; set  ; }
    }
}
