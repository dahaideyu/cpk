using System;

namespace CPK.Domain.Entity.ProductManage
{
    public class CPKResultEntity : IEntity<CPKResultEntity>, ICreationAudited, IDeleteAudited, IModificationAudited
    {
             /// <summary>
        /// 主键
        /// <summary>
        public string F_Id { get; set; }
        /// <summary>
        /// 编码
        /// <summary>
        public string F_ChartId { get; set; }
        /// <summary>
        /// 编码
        /// <summary>
        public decimal? F_CPK { get; set; }
        /// <summary>
        /// 名称
        /// <summary>
        public decimal? F_LSL { get; set; }
        /// <summary>
        /// 描述
        /// <summary>
        public decimal? F_USL { get; set; }
        /// <summary>
        /// 这个表的CPK结果最新记录描述
        /// <summary>
        public decimal? F_Sigma { get; set; }
        /// <summary>
        /// 最新发送RPS成功时间
        /// <summary>
        public string F_DistributeStatistics { get; set; }
        /// <summary>
        /// 数据源Id
        /// <summary>
        public DateTime? F_StartTime { get; set; }
        /// <summary>
        /// 监控周期类型(年、月、周、日、时、分、秒)
        /// <summary>
        public string F_FrequencyType { get; set; }
        /// <summary>
        /// 监控周期
        /// <summary>
        public int? F_Frequency { get; set; }
        /// <summary>
        /// 上次监控时间
        /// <summary>
        public DateTime? F_LastLoadTime { get; set; }
        /// <summary>
        /// 排序码
        /// <summary>
        public int? F_SortCode { get; set; }
        /// <summary>
        /// 有效标志
        /// <summary>
        public bool? F_EnabledMark { get; set; }
        /// <summary>
        /// 创建日期
        /// <summary>
        public DateTime? F_CreatorTime { get; set; }
        /// <summary>
        /// 创建用户主键
        /// <summary>
        public string F_CreatorUserId { get; set; }
        /// <summary>
        /// 最后修改时间
        /// <summary>
        public DateTime? F_LastModifyTime { get; set; }
        /// <summary>
        /// 最后修改用户
        /// <summary>
        public string F_LastModifyUserId { get; set; }
        /// <summary>
        /// 删除标志
        /// <summary>
        public bool? F_DeleteMark { get; set; }
        /// <summary>
        /// 删除时间
        /// <summary>
        public DateTime? F_DeleteTime { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_DeleteUserId { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_CreatorUserName { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_DeleteUserName { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_LastModifyUserName { get; set; }

    }
}
