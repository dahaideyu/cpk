using System;

namespace CPK.Domain.Entity.ProductManage
{
    public class OutSignEntity : IEntity<OutSignEntity>, ICreationAudited, IDeleteAudited, IModificationAudited
    {
             /// <summary>
        /// 用户主键
        /// <summary>
        public string F_Id { get; set; }
        /// <summary>
        /// 主管主键
        /// <summary>
        public string F_UserId { get; set; }
        /// <summary>
        /// 个性签名
        /// <summary>
        public string F_OutPlace { get; set; }
        /// <summary>
        /// 组织主键
        /// <summary>
        public DateTime F_StartTime { get; set; }
        /// <summary>
        /// 组织主键
        /// <summary>
        public DateTime F_EndTime { get; set; }
        /// <summary>
        /// 排序码
        /// <summary>
        public int F_SortCode { get; set; }
        /// <summary>
        /// 删除标志
        /// <summary>
        public bool? F_DeleteMark { get; set; }
        /// <summary>
        /// 有效标志
        /// <summary>
        public bool? F_EnabledMark { get; set; }
        /// <summary>
        /// 描述
        /// <summary>
        public string F_Description { get; set; }
        /// <summary>
        /// 创建时间
        /// <summary>
        public DateTime? F_CreatorTime { get; set; }
        /// <summary>
        /// 创建用户
        /// <summary>
        public string F_CreatorUserId { get; set; }
        /// <summary>
        /// 最后修改时间
        /// <summary>
        public DateTime? F_LastModifyTime { get; set; }
        /// <summary>
        /// 最后修改用户
        /// <summary>
        public string F_LastModifyUserId { get; set; }
        /// <summary>
        /// 删除时间
        /// <summary>
        public DateTime? F_DeleteTime { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_DeleteUserId { get; set; }
        /// <summary>
        /// 创建操作用户
        /// <summary>
        public string F_CreatorUserName { get; set; }
        /// <summary>
        /// 删除操作用户
        /// <summary>
        public string F_DeleteUserName { get; set; }
        /// <summary>
        /// 修改操作用户
        /// <summary>
        public string F_LastModifyUserName { get; set; }
    }
}
