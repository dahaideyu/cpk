using System;

namespace CPK.Domain.Entity.ProductManage
{
    public class OrderEntity : IEntity<OrderEntity>, ICreationAudited, IDeleteAudited, IModificationAudited
    {
             /// <summary>
        /// 
        /// <summary>
        public string F_Id { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public string F_OrderNo { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public string F_OrderType { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public string F_WorkAreaId { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public string F_ProductId { get; set; }
        /// <summary>
        /// 排序码
        /// <summary>
        public int F_SortCode { get; set; }
        /// <summary>
        /// 删除标志
        /// <summary>
        public bool? F_DeleteMark { get; set; }
        /// <summary>
        /// 是否可用
        /// <summary>
        public bool? F_EnabledMark { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public DateTime? F_CreatorTime { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public string F_CreatorUserId { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public DateTime? F_LastModifyTime { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public string F_LastModifyUserId { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public DateTime? F_DeleteTime { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public string F_DeleteUserId { get; set; }
        /// <summary>
        /// 创建操作用户
        /// <summary>
        public string F_CreatorUserName { get; set; }
        /// <summary>
        /// 删除操作用户
        /// <summary>
        public string F_DeleteUserName { get; set; }
        /// <summary>
        /// 修改操作用户
        /// <summary>
        public string F_LastModifyUserName { get; set; }
    }
}
