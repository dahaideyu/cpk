using System;

namespace CPK.Domain.Entity.ProductManage
{
    public class ProductEntity : IEntity<ProductEntity>, ICreationAudited, IDeleteAudited, IModificationAudited
    {
             /// <summary>
        /// 主键
        /// <summary>
        public string F_Id { get; set; }
        /// <summary>
        /// 编码
        /// <summary>
        public string F_EnCode { get; set; }
        /// <summary>
        /// 名称
        /// <summary>
        public string F_FullName { get; set; }
        /// <summary>
        /// 小图
        /// </summary>
        public string F_MinImage { get; set; }
        /// <summary>
        /// 大图
        /// </summary>
        public string F_MaxImage { get; set; }
        /// <summary>
        /// 区域
        /// <summary>
        public string F_WorkAreaId { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public string F_MainType { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public string F_SecondType { get; set; }
        /// <summary>
        /// 类型补充说明
        /// <summary>
        public string F_TypeMark { get; set; }
        /// <summary>
        /// 高
        /// <summary>
        public decimal F_Height { get; set; }
        /// <summary>
        /// 宽
        /// <summary>
        public decimal F_Width { get; set; }
        /// <summary>
        /// 长
        /// <summary>
        public decimal F_Length { get; set; }
        /// <summary>
        /// 数量
        /// <summary>
        public decimal F_Qty { get; set; }
        /// <summary>
        /// 单位
        /// <summary>
        public string F_Unit { get; set; }
        /// <summary>
        /// 辅助单位
        /// <summary>
        public string F_AssistUnit { get; set; }
        /// <summary>
        /// 所属人
        /// <summary>
        public string F_OwnerId { get; set; }
        /// <summary>
        /// 排序码
        /// <summary>
        public int F_SortCode { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public bool? F_DeleteMark { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public bool? F_EnabledMark { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public DateTime? F_CreatorTime { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public string F_CreatorUserId { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public DateTime? F_LastModifyTime { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public string F_LastModifyUserId { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public DateTime? F_DeleteTime { get; set; }
        /// <summary>
        /// 
        /// <summary>
        public string F_DeleteUserId { get; set; }
        public string F_CreatorUserName { get ; set ; }
        public string F_DeleteUserName { get  ; set  ; }
        public string F_LastModifyUserName { get  ; set ; }
    }
}
