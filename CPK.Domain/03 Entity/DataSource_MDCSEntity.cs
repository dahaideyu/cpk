using System;

namespace CPK.Domain.Entity.ProductManage
{
    public class DataSource_MDCSEntity : IEntity<DataSource_MDCSEntity>, ICreationAudited, IDeleteAudited, IModificationAudited
    {
             /// <summary>
        /// 主键
        /// <summary>
        public string F_Id { get; set; }
        /// <summary>
        /// 连接字符串
        /// <summary>
        public string F_ConnectString { get; set; }
        /// <summary>
        /// MDCS线别
        /// <summary>
        public string F_Line { get; set; }
        /// <summary>
        /// MDCS设备
        /// <summary>
        public string F_Device { get; set; }
        /// <summary>
        /// MDCS变量
        /// <summary>
        public string F_Variable { get; set; }
        /// <summary>
        /// 有效标志
        /// <summary>
        public bool? F_EnabledMark { get; set; }
        /// <summary>
        /// 创建日期
        /// <summary>
        public DateTime? F_CreatorTime { get; set; }
        /// <summary>
        /// 创建用户主键
        /// <summary>
        public string F_CreatorUserId { get; set; }
        /// <summary>
        /// 最后修改时间
        /// <summary>
        public DateTime? F_LastModifyTime { get; set; }
        /// <summary>
        /// 最后修改用户
        /// <summary>
        public string F_LastModifyUserId { get; set; }
        /// <summary>
        /// 删除标志
        /// <summary>
        public bool? F_DeleteMark { get; set; }
        /// <summary>
        /// 删除时间
        /// <summary>
        public DateTime? F_DeleteTime { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_DeleteUserId { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_CreatorUserName { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_DeleteUserName { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_LastModifyUserName { get; set; }
        /// <summary>
        /// MCF
        /// <summary>
        public string F_MCF { get; set; }
        /// <summary>
        /// BoardType
        /// <summary>
        public string F_BoardType { get; set; }

    }
}
