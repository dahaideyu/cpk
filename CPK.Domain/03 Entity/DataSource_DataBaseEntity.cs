using System;

namespace CPK.Domain.Entity.ProductManage
{
    public class DataSource_DataBaseEntity : IEntity<DataSource_DataBaseEntity>, ICreationAudited, IDeleteAudited, IModificationAudited
    {
             /// <summary>
        /// 主键
        /// <summary>
        public string F_Id { get; set; }
        /// <summary>
        /// 连接字符串
        /// <summary>
        public string F_ConnectString { get; set; }
        /// <summary>
        /// 表名
        /// <summary>
        public string F_TableName { get; set; }
        /// <summary>
        /// 关键字段
        /// <summary>
        public string F_KeyField { get; set; }
        /// <summary>
        /// 排序字段
        /// <summary>
        public string F_SortField { get; set; }
        /// <summary>
        /// 样本值字段
        /// <summary>
        public string F_ValueField { get; set; }
        /// <summary>
        /// 时间字段
        /// <summary>
        public string F_TimeField { get; set; }
        /// <summary>
        /// 标签字段，多个用逗号隔开
        /// <summary>
        public string F_TagFields { get; set; }
        /// <summary>
        /// 创建日期
        /// <summary>
        public DateTime? F_CreatorTime { get; set; }
        /// <summary>
        /// 创建用户主键
        /// <summary>
        public string F_CreatorUserId { get; set; }
        /// <summary>
        /// 最后修改时间
        /// <summary>
        public DateTime? F_LastModifyTime { get; set; }
        /// <summary>
        /// 最后修改用户
        /// <summary>
        public string F_LastModifyUserId { get; set; }
        /// <summary>
        /// 删除标志
        /// <summary>
        public bool? F_DeleteMark { get; set; }
        /// <summary>
        /// 删除时间
        /// <summary>
        public DateTime? F_DeleteTime { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_DeleteUserId { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_CreatorUserName { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_DeleteUserName { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_LastModifyUserName { get; set; }

    }
}
