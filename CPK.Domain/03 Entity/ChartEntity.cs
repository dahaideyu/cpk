using System;

namespace CPK.Domain.Entity.ProductManage
{
    public class ChartEntity : IEntity<ChartEntity>, ICreationAudited, IDeleteAudited, IModificationAudited
    {
             /// <summary>
        /// 主键
        /// <summary>
        public string F_Id { get; set; }
        /// <summary>
        /// 编码
        /// <summary>
        public string F_EnCode { get; set; }
        /// <summary>
        /// 名称
        /// <summary>
        public string F_FullName { get; set; }
        /// <summary>
        /// 描述
        /// <summary>
        public string F_Description { get; set; }
        /// <summary>
        /// 最后CPK结果结合
        /// </summary>
        public string F_LastMessage { get; set; }
        /// <summary>
        /// 最后的RPS时间
        /// </summary>

        public DateTime? F_LastRPSTime { get; set; }
        /// <summary>
        /// 数据源Id
        /// <summary>
        public string F_DataSourceId { get; set; }
        /// <summary>
        /// 监控周期类型(年、月、周、日、时、分、秒)
        /// <summary>
        public string F_FrequencyType { get; set; }
        /// <summary>
        /// 监控周期
        /// <summary>
        public int? F_Frequency { get; set; }
        /// <summary>
        /// 上次监控时间
        /// <summary>
        public DateTime? F_LastLoadTime { get; set; }
        /// <summary>
        /// 排序码
        /// <summary>
        public int? F_SortCode { get; set; }
        /// <summary>
        /// 有效标志
        /// <summary>
        public bool? F_EnabledMark { get; set; }
        /// <summary>
        /// 创建日期
        /// <summary>
        public DateTime? F_CreatorTime { get; set; }
        /// <summary>
        /// 创建用户主键
        /// <summary>
        public string F_CreatorUserId { get; set; }
        /// <summary>
        /// 最后修改时间
        /// <summary>
        public DateTime? F_LastModifyTime { get; set; }
        /// <summary>
        /// 最后修改用户
        /// <summary>
        public string F_LastModifyUserId { get; set; }
        /// <summary>
        /// 删除标志
        /// <summary>
        public bool? F_DeleteMark { get; set; }
        /// <summary>
        /// 删除时间
        /// <summary>
        public DateTime? F_DeleteTime { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_DeleteUserId { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_CreatorUserName { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_DeleteUserName { get; set; }
        /// <summary>
        /// 删除用户
        /// <summary>
        public string F_LastModifyUserName { get; set; }
        /// <summary>
        /// 脏数据
        /// <summary>
        public string F_DirtyRead { get; set; }
        /// <summary>
        /// 对应RPS线别
        /// </summary>
        public string F_RPSLineId { get; set; }
    }
}
