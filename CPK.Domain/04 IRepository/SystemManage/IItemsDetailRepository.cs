﻿using CPK.Data;
using CPK.Domain.Entity.SystemManage;
using System.Collections.Generic;

namespace CPK.Domain.IRepository.SystemManage
{
    public interface IItemsDetailRepository : IRepositoryBase<ItemsDetailEntity>
    {
        List<ItemsDetailEntity> GetItemList(string enCode);
    }
}
