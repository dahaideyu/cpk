﻿using System;
using System.Collections.Generic;
using System.Text;
using CPK.Data;
using CPK.Domain.Entity.SystemManage;

namespace CPK.Domain.IRepository.SystemManage
{
   public interface INewsInfoRepository:IRepositoryBase<NewsInfoEntity>
    {
    }
}
