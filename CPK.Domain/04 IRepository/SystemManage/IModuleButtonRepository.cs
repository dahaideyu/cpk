﻿using CPK.Data;
using CPK.Domain.Entity.SystemManage;
using System.Collections.Generic;

namespace CPK.Domain.IRepository.SystemManage
{
    public interface IModuleButtonRepository : IRepositoryBase<ModuleButtonEntity>
    {
        void SubmitCloneButton(List<ModuleButtonEntity> entitys);
    }
}
