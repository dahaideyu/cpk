﻿using CPK.Data;
using CPK.Domain.Entity.SystemManage;

namespace CPK.Domain.IRepository.SystemManage
{
    public interface IModuleRepository : IRepositoryBase<ModuleEntity>
    {
    }
}
