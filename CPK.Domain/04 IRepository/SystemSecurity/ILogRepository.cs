﻿using CPK.Data;
using CPK.Domain.Entity.SystemSecurity;

namespace CPK.Domain.IRepository.SystemSecurity
{
    public interface ILogRepository : IRepositoryBase<LogEntity>
    {
        
    }
}
