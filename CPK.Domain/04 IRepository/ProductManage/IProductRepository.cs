using CPK.Data;
using CPK.Domain.Entity.ProductManage;

namespace CPK.Domain.IRepository.ProductManage
{
    public interface IProductRepository : IRepositoryBase<ProductEntity>
    {
    }
}
