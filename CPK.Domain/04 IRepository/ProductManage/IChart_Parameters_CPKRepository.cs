using CPK.Data;
using CPK.Domain.Entity.ProductManage;

namespace CPK.Domain.IRepository.ProductManage
{
    public interface IChart_Parameters_CPKRepository : IRepositoryBase<Chart_Parameters_CPKEntity>
    {
    }
}
