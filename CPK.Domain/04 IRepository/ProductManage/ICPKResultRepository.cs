using CPK.Data;
using CPK.Domain.Entity.ProductManage;
using System.Collections.Generic;

namespace CPK.Domain.IRepository.ProductManage
{
    public interface ICPKResultRepository : IRepositoryBase<CPKResultEntity>
    {
          List<CPKResultEntity> GetNORepeatList();
    }
}
