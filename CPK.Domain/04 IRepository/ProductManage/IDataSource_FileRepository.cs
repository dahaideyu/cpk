using CPK.Data;
using CPK.Domain.Entity.ProductManage;

namespace CPK.Domain.IRepository.ProductManage
{
    public interface IDataSource_FileRepository : IRepositoryBase<DataSource_FileEntity>
    {
    }
}
