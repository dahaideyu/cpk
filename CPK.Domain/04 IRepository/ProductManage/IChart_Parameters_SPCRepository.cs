using CPK.Data;
using CPK.Domain.Entity.ProductManage;

namespace CPK.Domain.IRepository.ProductManage
{
    public interface IChart_Parameters_SPCRepository : IRepositoryBase<Chart_Parameters_SPCEntity>
    {
    }
}
