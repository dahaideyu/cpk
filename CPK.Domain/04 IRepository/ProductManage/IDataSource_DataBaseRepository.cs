using CPK.Data;
using CPK.Domain.Entity.ProductManage;

namespace CPK.Domain.IRepository.ProductManage
{
    public interface IDataSource_DataBaseRepository : IRepositoryBase<DataSource_DataBaseEntity>
    {
    }
}
