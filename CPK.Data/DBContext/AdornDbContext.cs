﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Extensions;
using System;
using System.Linq;
using System.Reflection;
using System.Data.Common;
using CPK.Code;

namespace CPK.Data
{
    public partial class CPKDbContext : DbContext
    {
        public CPKDbContext()
            : base()
        {
            
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["CPKDbContext"],options=>options.UseRowNumberForPaging());
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            string currentAssembleFileName = Assembly.GetExecutingAssembly().CodeBase.ToString();
            string assembleFileName = currentAssembleFileName.Replace(".Data.", ".Mapping.").Replace("file:///","");


            if (assembleFileName.IndexOf(":") == -1)
                assembleFileName = @"/" + assembleFileName;
            Assembly asm = Assembly.LoadFile(assembleFileName);
            var typesToRegister = asm.GetTypes()
            .Where(type => !String.IsNullOrEmpty(type.Namespace))
            .Where(type => type.BaseType != null && type.BaseType.IsGenericType && type.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>));
            foreach (var type in typesToRegister)
            {
                object configurationInstance = Activator.CreateInstance(type);
               
                modelBuilder.AddConfiguration(type, configurationInstance);
            }
            //modelBuilder.AddConfiguration(new UserMap());
            base.OnModelCreating(modelBuilder);
        }

    }
}
