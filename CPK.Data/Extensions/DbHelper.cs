﻿
using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using CPK.Code;

namespace CPK.Data.Extensions
{
    public class DbHelper
    {
        private string _connstring;

        public string connstring { get => _connstring; set => _connstring = value; }
        public DbHelper(string sConnString)
        {
            connstring = sConnString;
        }
        public int ExecuteSqlCommand(string cmdText)
        {
            using (DbConnection conn = new SqlConnection(_connstring))
            {
                DbCommand cmd = new SqlCommand();
                PrepareCommand(cmd, conn, null, CommandType.Text, cmdText, null);
                return cmd.ExecuteNonQuery();
            }
        }
        private void PrepareCommand(DbCommand cmd, DbConnection conn, DbTransaction isOpenTrans, CommandType cmdType, string cmdText, DbParameter[] cmdParms)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            cmd.Connection = conn;
            cmd.CommandText = cmdText;
            if (isOpenTrans != null)
                cmd.Transaction = isOpenTrans;
            cmd.CommandType = cmdType;
            if (cmdParms != null)
            {
                cmd.Parameters.AddRange(cmdParms);
            }
        }

        public DataSet GetDataBySQL(string sSQL)
        {
            try
            {
                DataSet result = new DataSet();
                using (SqlDataAdapter da = new SqlDataAdapter(sSQL, _connstring))
                {
                    da.Fill(result);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
