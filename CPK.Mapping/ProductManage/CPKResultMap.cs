using CPK.Domain.Entity.ProductManage;
using Microsoft.EntityFrameworkCore;

namespace CPK.Mapping.ProductManage
{
    public class CPKResultMap : EntityTypeConfiguration<CPKResultEntity>
    {
        public override void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CPKResultEntity>().ToTable("CPK_CPKResult").HasKey(_ => _.F_Id);
        }
    }
}