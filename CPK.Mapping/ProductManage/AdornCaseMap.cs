using CPK.Domain.Entity.ProductManage;
using Microsoft.EntityFrameworkCore;

namespace CPK.Mapping.ProductManage
{
    public class CPKCaseMap : EntityTypeConfiguration<CPKCaseEntity>
    {
        public override void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CPKCaseEntity>().ToTable("Sys_CPKCase").HasKey(_ => _.F_Id);
        }
    }
}