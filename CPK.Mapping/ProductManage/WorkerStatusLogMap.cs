using CPK.Domain.Entity.ProductManage;
using Microsoft.EntityFrameworkCore;

namespace CPK.Mapping.ProductManage
{
    public class WorkerStatusLogMap : EntityTypeConfiguration<WorkerStatusLogEntity>
    {
        public override void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<WorkerStatusLogEntity>().ToTable("Sys_WorkerStatusLog").HasKey(_ => _.F_Id);
        }
    }
}