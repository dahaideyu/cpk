using CPK.Domain.Entity.ProductManage;
using Microsoft.EntityFrameworkCore;

namespace CPK.Mapping.ProductManage
{
    public class SubjectForumMap : EntityTypeConfiguration<SubjectForumEntity>
    {
        public override void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SubjectForumEntity>().ToTable("Sys_SubjectForum").HasKey(_ => _.F_Id);
        }
    }
}