using CPK.Domain.Entity.ProductManage;
using Microsoft.EntityFrameworkCore;

namespace CPK.Mapping.ProductManage
{
    public class SubjectComplaintsMap : EntityTypeConfiguration<SubjectComplaintsEntity>
    {
        public override void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SubjectComplaintsEntity>().ToTable("Sys_SubjectComplaints").HasKey(_ => _.F_Id);
        }
    }
}