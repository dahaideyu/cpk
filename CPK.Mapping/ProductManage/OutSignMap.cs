using CPK.Domain.Entity.ProductManage;
using Microsoft.EntityFrameworkCore;

namespace CPK.Mapping.ProductManage
{
    public class OutSignMap : EntityTypeConfiguration<OutSignEntity>
    {
        public override void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OutSignEntity>().ToTable("Sys_OutSign").HasKey(_ => _.F_Id);
        }
    }
}