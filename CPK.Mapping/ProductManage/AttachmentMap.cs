using CPK.Domain.Entity.ProductManage;
using Microsoft.EntityFrameworkCore;

namespace CPK.Mapping.ProductManage
{
    public class AttachmentMap : EntityTypeConfiguration<AttachmentEntity>
    {
        public override void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AttachmentEntity>().ToTable("Sys_Attachment").HasKey(_ => _.F_Id);
        }
    }
}