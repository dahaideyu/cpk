using CPK.Domain.Entity.ProductManage;
using Microsoft.EntityFrameworkCore;

namespace CPK.Mapping.ProductManage
{
    public class DataSource_FileMap : EntityTypeConfiguration<DataSource_FileEntity>
    {
        public override void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DataSource_FileEntity>().ToTable("SPC_DataSource_File").HasKey(_ => _.F_Id);
        }
    }
}