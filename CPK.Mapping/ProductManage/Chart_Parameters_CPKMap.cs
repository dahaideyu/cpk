using CPK.Domain.Entity.ProductManage;
using Microsoft.EntityFrameworkCore;

namespace CPK.Mapping.ProductManage
{
    public class Chart_Parameters_CPKMap : EntityTypeConfiguration<Chart_Parameters_CPKEntity>
    {
        public override void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Chart_Parameters_CPKEntity>().ToTable("SPC_Chart_Parameters_CPK").HasKey(_ => _.F_Id);
        }
    }
}