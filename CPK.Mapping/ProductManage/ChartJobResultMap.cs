using CPK.Domain.Entity.ProductManage;
using Microsoft.EntityFrameworkCore;

namespace CPK.Mapping.ProductManage
{
    public class ChartJobResultMap : EntityTypeConfiguration<ChartJobResultEntity>
    {
        public override void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ChartJobResultEntity>().ToTable("SPC_ChartJobResult").HasKey(_ => _.F_Id);
        }
    }
}