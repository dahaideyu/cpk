using CPK.Domain.Entity.ProductManage;
using Microsoft.EntityFrameworkCore;

namespace CPK.Mapping.ProductManage
{
    public class DataSourceMap : EntityTypeConfiguration<DataSourceEntity>
    {
        public override void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DataSourceEntity>().ToTable("SPC_DataSource").HasKey(_ => _.F_Id);
        }
    }
}