using CPK.Domain.Entity.ProductManage;
using Microsoft.EntityFrameworkCore;

namespace CPK.Mapping.ProductManage
{
    public class DataSource_DataBaseMap : EntityTypeConfiguration<DataSource_DataBaseEntity>
    {
        public override void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DataSource_DataBaseEntity>().ToTable("SPC_DataSource_DataBase").HasKey(_ => _.F_Id);
        }
    }
}