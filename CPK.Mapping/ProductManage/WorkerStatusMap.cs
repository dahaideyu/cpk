using CPK.Domain.Entity.ProductManage;
using Microsoft.EntityFrameworkCore;

namespace CPK.Mapping.ProductManage
{
    public class WorkerStatusMap : EntityTypeConfiguration<WorkerStatusEntity>
    {
        public override void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<WorkerStatusEntity>().ToTable("Sys_WorkerStatus").HasKey(_ => _.F_Id);
        }
    }
}