using CPK.Domain.Entity.ProductManage;
using Microsoft.EntityFrameworkCore;

namespace CPK.Mapping.ProductManage
{
    public class Chart_Parameters_SPCMap : EntityTypeConfiguration<Chart_Parameters_SPCEntity>
    {
        public override void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Chart_Parameters_SPCEntity>().ToTable("SPC_Chart_Parameters_SPC").HasKey(_ => _.F_Id);
        }
    }
}