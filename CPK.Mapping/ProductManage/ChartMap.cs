using CPK.Domain.Entity.ProductManage;
using Microsoft.EntityFrameworkCore;

namespace CPK.Mapping.ProductManage
{
    public class ChartMap : EntityTypeConfiguration<ChartEntity>
    {
        public override void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ChartEntity>().ToTable("SPC_Chart").HasKey(_ => _.F_Id);
        }
    }
}