using CPK.Domain.Entity.ProductManage;
using Microsoft.EntityFrameworkCore;

namespace CPK.Mapping.ProductManage
{
    public class DataSource_MDCSMap : EntityTypeConfiguration<DataSource_MDCSEntity>
    {
        public override void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DataSource_MDCSEntity>().ToTable("SPC_DataSource_MDCS").HasKey(_ => _.F_Id);
        }
    }
}