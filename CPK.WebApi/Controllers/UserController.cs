﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CPK.Application.SystemManage;
using CPK.Domain.Entity.SystemManage;
using CPK.Domain.ViewModel;
using CPK.WebApi.Infrastructure;
using AutoMapper;
using AutoMapper.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using PMS.Code;

namespace CPK.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private UserApp _userApp = new UserApp();
        private RoleApp _roleApp = new RoleApp();
        private OrganizeApp _OrganizeApp = new OrganizeApp();
        private DutyApp _dutyApp = new DutyApp();
        private IMapper _mapper { get; set; }
        private IConfiguration _configuration { get; set; }
        private IOptions<AppSettingsModel> _setting { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="settings"></param>
        public UserController(IMapper mapper, IOptions<AppSettingsModel> settings)
        {
            _mapper = mapper;
            _setting = settings;
        }
        /// <summary>
        /// 获得所有用户列表
        /// </summary>
        /// <returns></returns>
        // GET: api/User
        [HttpGet]
        public ApiJsonResult Get()
        {
            try
            {
                var ulist = _userApp.GetList();
                return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "success", Result = ulist };

            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "500", ErrorMessage = "entity is null", Result = ex.Message };
            }
        }
        /// <summary>
        /// 依据角色获得所有用户列表
        /// </summary>
        /// <returns></returns>
        // GET: api/User
        [HttpGet("GetByRole/{roleid}")]
        public ApiJsonResult GetByRole(string roleid)
        {
            try
            {
                var ulist = _userApp.GetList(t => t.F_RoleId == roleid);
                return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "success", Result = ulist };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "500", ErrorMessage = "entity is null", Result = ex.Message };
            }
        }
        /// <summary>
        /// 依据组织获得所有用户列表
        /// </summary>
        /// <returns></returns>
        // GET: api/User
        [HttpGet("GetByOrganize/{organizeid}")]
        public ApiJsonResult GetByOrganize(string organizeid)
        {
            try
            {
                var ulist = _userApp.GetList(t => t.F_OrganizeId == organizeid);
                return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "success", Result = ulist };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "500", ErrorMessage = "entity is null", Result = ex.Message };
            }
        }
        /// <summary>
        /// 获得所有角色
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetRole")]
        public ApiJsonResult GetRole()
        {
            try
            {
                var rlist = _roleApp.GetList();
                return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "success", Result = rlist };

            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "500", ErrorMessage = "entity is null", Result = ex.Message };

            }

        }
        /// <summary>
        /// 获得所有组织信息
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetOrganize")]
        public ApiJsonResult GetOrganize()
        {
            try
            {
                var rlist = _OrganizeApp.GetList();
                return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "success", Result = rlist };

            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "500", ErrorMessage = "entity is null", Result = ex.Message };

            }

        }
        /// <summary>
        /// 获得所有岗位信息
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetDuty")]
        public ApiJsonResult GetDuty()
        {
            try
            {
                var list = _dutyApp.GetList();
                return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "success", Result = list };

            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "500", ErrorMessage = "entity is null", Result = ex.Message };

            }

        }
        /// <summary>
        /// 获取单个用户的信息
        /// </summary>
        /// <param name="id">用户Id</param>
        /// <returns></returns>
        [HttpGet("GetUserInfo/{id}")]
        //  [HttpGet("{id}", Name = "GetUserInfo")]
        public ApiJsonResult GetUserInfo(string id)
        {
            try
            {
                var imageHost = _setting.Value.ImageHost;
                var entity = _userApp.GetForm(u => u.F_Id == id);
                var uVM = _mapper.Map<APIUserVM>(entity);
                if (uVM != null)
                {
                    if (string.IsNullOrWhiteSpace(uVM.F_HeadIcon))
                    {
                        uVM.F_HeadIcon = imageHost + "upload//default.png";
                    }
                    else
                    {
                        uVM.F_HeadIcon = imageHost + uVM.F_HeadIcon;
                    }
                    var organizeEntity = _OrganizeApp.GetForm(uVM.F_OrganizeId);
                    if (organizeEntity != null)
                    {
                        uVM.F_OrganizeName = organizeEntity.F_FullName;
                    }
                    var roleEntity = _roleApp.GetForm(uVM.F_RoleId);
                    if (roleEntity != null)
                    {
                        uVM.F_RoleName = roleEntity.F_FullName;

                        // 0是客户1材料商2销售3设计师4施工人员5监理6项目经理7财务6管理者
                        switch (roleEntity.F_FullName)
                        {

                            case "guest": uVM.F_UserType = "0"; break;
                            case "Supplier": uVM.F_UserType = "1"; break;
                            case "businessPersonnel": uVM.F_UserType = "2"; break;
                            case "design": uVM.F_UserType = "3"; break;
                            case "implement": uVM.F_UserType = "4"; break;
                            case "IPQC": uVM.F_UserType = "5"; break;
                            case "manager": uVM.F_UserType = "6"; break;
                            case "finance": uVM.F_UserType = "7"; break;
                            case "system": uVM.F_UserType = "8"; break;
                            default: uVM.F_UserType = "10"; break;


                        }

                    }
                    var dutyEntity = _dutyApp.GetForm(uVM.F_DutyId);
                    if (dutyEntity != null)
                    {
                        uVM.F_DutyName = dutyEntity.F_FullName;
                    }
                }

                return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "success", Result = uVM };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "500", ErrorMessage = "entity is null", Result = ex.Message };

            }
        }


        // /// <summary>
        // /// 新增用户
        // /// </summary>
        // /// <param name="entity"></param>
        // // POST: api/User
        // [HttpPost]
        // public void Post([FromBody] UserEntity entity)
        // {
        // }

        // // PUT: api/User/5
        // [HttpPut("{id}")]
        // public void Put(string id, [FromBody] UserEntity value)
        // {
        // }

        // // DELETE: api/ApiWithActions/5
        // [HttpDelete("{id}")]
        // public void Delete(string id)
        // {
        // }
    }
}
