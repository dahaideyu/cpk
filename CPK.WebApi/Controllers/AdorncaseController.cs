﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CPK.Application.ProductManage;
using CPK.Application.SystemManage;
using CPK.WebApi.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using PMS.Code;

namespace CPK.WebApi.Controllers
{
    /// <summary>
    /// 装修案例
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class CPKcaseController : ControllerBase
    {
        private IOptions<AppSettingsModel> _setting { get; set; }
        private CPKCaseApp _CPKCaseApp = new CPKCaseApp();
        private ItemsApp _ItemApp = new ItemsApp();
        private ItemsDetailApp _itemdetailApp = new ItemsDetailApp();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="settings"></param>
        public CPKcaseController(IOptions<AppSettingsModel> settings)
        {
            _setting = settings;
        }
        /// <summary>
        /// 通过类型获取所有案例实体
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetByType/{type}")]
        public ApiJsonResult GetByType(string type)
        {
            try
            {
                var caselist = _CPKCaseApp.GetList(t => t.F_Type == type);
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "200", ErrorMessage = "success", Result = caselist };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = ex.Message };

            }
        }
        /// <summary>
        /// 获取所有案例实体
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ApiJsonResult Get()
        {
            try
            {

                var caselist = _CPKCaseApp.GetList();
                foreach (var item in caselist)
                {
                    item.F_Image = _setting.Value.ImageHost + item.F_Image;
                    item.F_MaxImage01 = _setting.Value.ImageHost + item.F_MaxImage01;
                    item.F_MaxImage02 = _setting.Value.ImageHost + item.F_MaxImage02;
                    item.F_MaxImage03 = _setting.Value.ImageHost + item.F_MaxImage03;
                    item.F_MaxImage04 = _setting.Value.ImageHost + item.F_MaxImage04;
                    item.F_MInImage = _setting.Value.ImageHost + item.F_MInImage;
                }
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "200", ErrorMessage = "success", Result = caselist };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = ex.Message };

            }
        }
        /// <summary>
        /// 获取单个案例实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ApiJsonResult Get(string id)
        {
            try
            {

                var caseEntity = _CPKCaseApp.GetForm(id);
                if (caseEntity != null)
                {
                    caseEntity.F_Image = _setting.Value.ImageHost + caseEntity.F_Image;
                    caseEntity.F_MaxImage01 = _setting.Value.ImageHost + caseEntity.F_MaxImage01;
                    caseEntity.F_MaxImage02 = _setting.Value.ImageHost + caseEntity.F_MaxImage02;
                    caseEntity.F_MaxImage03 = _setting.Value.ImageHost + caseEntity.F_MaxImage03;
                    caseEntity.F_MaxImage04 = _setting.Value.ImageHost + caseEntity.F_MaxImage04;
                    caseEntity.F_MInImage = _setting.Value.ImageHost + caseEntity.F_MInImage;

                }
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "200", ErrorMessage = "success", Result = caseEntity };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = ex.Message };

            }
        }
        /// <summary>
        /// 获得全部装修案例风格类型
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetCPKType")]
        public ApiJsonResult GetCPKType()
        {
            try
            {
                var itemdetaillist = _itemdetailApp.GetListEnCode("CPKType");
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "200", ErrorMessage = "success", Result = itemdetaillist };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = ex.Message };

            }
        }
    }
}