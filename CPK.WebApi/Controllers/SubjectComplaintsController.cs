﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CPK.Application.ProductManage;
using CPK.Application.SystemManage;
using CPK.Domain.Entity.ProductManage;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PMS.Code;

namespace CPK.WebApi.Controllers
{
    /// <summary>
    /// 项目投诉
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class SubjectComplaintsController : ControllerBase
    {
        private UserApp _userApp = new UserApp();
        private RoleApp _roleApp = new RoleApp();
        private SubjectApp _subjectApp = new SubjectApp();
        private SubjectForumApp _subjectForumApp = new SubjectForumApp();
        private SubjectComplaintsApp _subjectComplaintsForumApp = new SubjectComplaintsApp();
        /// <summary>
        /// 获得项目投诉列表
        /// </summary>
        /// <param name="keyword">项目id</param>
        /// <returns></returns>
        [HttpGet("GetListBySubjectId/{keyword}")]
        public ApiJsonResult GetListBySubjectId(string keyword)
        {
            try
            {

               var list= _subjectComplaintsForumApp.GetList(t=>t.F_SubjectId==keyword);
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "200", ErrorMessage = "success", Result = list };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = ex.Message };

            }
        }
        /// <summary>
        /// 获得项目投诉（单个实体）
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        [HttpGet]
        public ApiJsonResult Get(string keyword)
        {
            try
            {
              var entity=  _subjectComplaintsForumApp.GetForm(keyword);

                return new ApiJsonResult { IsSucceed = false, ErrorCode = "200", ErrorMessage = "success", Result = entity };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = ex.Message };

            }
        }
        /// <summary>
        /// 添加项目投诉
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        // POST: api/Subject
        [HttpPost]
        public ApiJsonResult Post([FromBody] SubjectComplaintsEntity entity)
        {
            try
            {
                if (entity == null)
                {
                    return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = "entity is null", Result = "" };
                }
                _subjectComplaintsForumApp.SubmitForm(entity, entity.F_Id);
                return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "success", Result = "" };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = ex.Message };

            }
        }
        /// <summary>
        /// 修改项目投诉
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        // PUT: api/Subject/5
        [HttpPut("{id}")]
        public ApiJsonResult Put(string id, [FromBody] SubjectComplaintsEntity entity)
        {
            try
            {
                if (entity == null)
                {
                    return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = "entity is null", Result = "" };
                }
                _subjectComplaintsForumApp.SubmitForm(entity, entity.F_Id);
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "200", ErrorMessage = "success", Result = "" };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = ex.Message };

            }
        }
        /// <summary>
        /// 删除投诉
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public ApiJsonResult Delete(string id)
        {
            try
            {
                _subjectComplaintsForumApp.DeleteForm(id);
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "200", ErrorMessage = "success", Result = "" };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = ex.Message };

            }
        }
    }
}