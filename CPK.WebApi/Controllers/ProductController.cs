﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CPK.Application.ProductManage;
using CPK.Application.SystemManage;
using CPK.Domain.Entity.ProductManage;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PMS.Code;

namespace CPK.WebApi.Controllers
{
    /// <summary>
    /// 材料产品的api
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private ProductApp _productApp = new ProductApp();
        private UserApp _userApp = new UserApp();
        private RoleApp _roleApp = new RoleApp();
        private SubjectApp _subjectApp = new SubjectApp();
        private ItemsDetailApp _itemdetailApp = new ItemsDetailApp();
        /// <summary>
        /// 获得项目 根据用户角色不同获得不同的项目列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ApiJsonResult Get(string id)
        {
            try
            {
                var uEntity = _userApp.GetForm(id);
                if (uEntity == null)
                {
                    return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = "entity is null", Result = "" };

                }
                if (uEntity.F_Account == "admin" && string.IsNullOrWhiteSpace(uEntity.F_RoleId))
                {
                    var plist = _productApp.GetList();
                    return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "success", Result = plist };

                }


                var slist = _productApp.GetList(s => s.F_OwnerId == id);
                return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "success", Result = slist };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = ex.Message };

            }
        }

        /// <summary>
        /// 添加材料
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        // POST: api/Subject
        [HttpPost]
        public ApiJsonResult Post([FromBody] ProductEntity entity)
        {
            try
            {
                if (entity == null)
                {
                    return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = "entity is null", Result = "" };
                }
                _productApp.SubmitForm(entity, entity.F_Id);
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "200", ErrorMessage = "success", Result = "" };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = ex.Message };

            }
        }
        /// <summary>
        /// 修改材料
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        // PUT: api/Subject/5
        [HttpPut("{id}")]
        public ApiJsonResult Put(string id, [FromBody] ProductEntity entity)
        {
            try
            {
                if (entity == null)
                {
                    return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = "entity is null", Result = "" };
                }
                _productApp.SubmitForm(entity, entity.F_Id);
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "200", ErrorMessage = "success", Result = "" };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = ex.Message };

            }
        }
        /// <summary>
        /// 
        ///删除材料
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public ApiJsonResult Delete(string id)
        {
            try
            {
                var entity = _productApp.GetForm(id);
                if (entity == null)
                {
                    return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = "entity is null", Result = "" };
                }
                _productApp.DeleteForm(id);
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "200", ErrorMessage = "success", Result = "" };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = ex.Message };

            }
        }
        /// <summary>
        /// 获得材料的类型
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetCPKProcess")]
        public ApiJsonResult GetProductType()
        {
            try
            {
                var itemdetaillist = _itemdetailApp.GetListEnCode("ProductType");
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "200", ErrorMessage = "success", Result = itemdetaillist };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = ex.Message };

            }
        }
    }
}