﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CPK.Application.ProductManage;
using CPK.Application.SystemManage;
using CPK.Domain.Entity.ProductManage;
using CPK.Domain.ViewModel;
using CPK.WebApi.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using PMS.Code;

namespace CPK.WebApi.Controllers
{
    /// <summary>
    /// 施工项目
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class SubjectController : ControllerBase
    {
        private UserApp _userApp = new UserApp();
        private RoleApp _roleApp = new RoleApp();
        private SubjectApp _subjectApp = new SubjectApp();
        private ItemsDetailApp _itemdetailApp = new ItemsDetailApp();
        private IOptions<AppSettingsModel> _setting { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="settings"></param>
        public SubjectController(IOptions<AppSettingsModel> settings)
        {
            _setting = settings;
        }
        /// <summary>
        /// 备用获取列表
        /// </summary>
        /// <returns></returns>
        // GET: api/Subject
        [HttpGet("All")]
        public ApiJsonResult Get()
        {
            try
            {

                var list = _subjectApp.GetList();
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "200", ErrorMessage = "success", Result = list };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = ex.Message };

            }
        }
        /// <summary>
        /// 获得项目 根据用户角色不同获得不同的项目列表
        /// </summary>
        /// <param name="id">用户id</param>
        /// <returns></returns>

        // GET: api/Subject/5
        [HttpGet("{id}", Name = "Get")]
        public ApiJsonResult Get(string id)
        {
            try
            {
                var imgHost = _setting.Value.ImageHost;
                var uEntity = _userApp.GetForm(id);
                if (uEntity == null)
                {
                    return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = "entity is null", Result = "" };

                }
                if (uEntity.F_Account == "admin" && string.IsNullOrWhiteSpace(uEntity.F_RoleId))
                {
                    var slist = _subjectApp.GetList();
                    foreach(var item in slist)
                    {
                        item.F_MinImage = imgHost + item.F_MinImage;
                        item.F_Image01 = imgHost + item.F_Image01;
                        item.F_Image02 = imgHost + item.F_Image02;
                        item.F_Image03 = imgHost + item.F_Image03;
                        item.F_Image04 = imgHost + item.F_Image04;
                    }
                    return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "success", Result = slist };

                }
                var roleEntity = _roleApp.GetForm(uEntity.F_RoleId);
                if (roleEntity == null)
                {
                    return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = "role is null", Result = "" };
                }
                if (roleEntity.F_EnCode.ToLower() == "guest")
                {
                    var slist = _subjectApp.GetList(s => s.F_ClientId == id);
                    foreach (var item in slist)
                    {
                        item.F_MinImage = imgHost + item.F_MinImage;
                        item.F_Image01 = imgHost + item.F_Image01;
                        item.F_Image02 = imgHost + item.F_Image02;
                        item.F_Image03 = imgHost + item.F_Image03;
                        item.F_Image04 = imgHost + item.F_Image04;
                    }
                    return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "success", Result = slist };
                }
                if (roleEntity.F_EnCode.ToLower() == "implement")
                {
                    var slist = _subjectApp.GetList(s => s.F_ImplementId == id);
                    foreach (var item in slist)
                    {
                        item.F_MinImage = imgHost + item.F_MinImage;
                        item.F_Image01 = imgHost + item.F_Image01;
                        item.F_Image02 = imgHost + item.F_Image02;
                        item.F_Image03 = imgHost + item.F_Image03;
                        item.F_Image04 = imgHost + item.F_Image04;
                    }
                    return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "success", Result = slist };
                }
                if (roleEntity.F_EnCode.ToUpper() == "IPQC")
                {
                    var slist = _subjectApp.GetList(s => s.F_IPQC == id);
                    foreach (var item in slist)
                    {
                        item.F_MinImage = imgHost + item.F_MinImage;
                        item.F_Image01 = imgHost + item.F_Image01;
                        item.F_Image02 = imgHost + item.F_Image02;
                        item.F_Image03 = imgHost + item.F_Image03;
                        item.F_Image04 = imgHost + item.F_Image04;
                    }
                    return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "success", Result = slist };
                }
                if (roleEntity.F_EnCode.ToUpper() == "design")
                {
                    var slist = _subjectApp.GetList(s => s.F_DesignId == id);
                    foreach (var item in slist)
                    {
                        item.F_MinImage = imgHost + item.F_MinImage;
                        item.F_Image01 = imgHost + item.F_Image01;
                        item.F_Image02 = imgHost + item.F_Image02;
                        item.F_Image03 = imgHost + item.F_Image03;
                        item.F_Image04 = imgHost + item.F_Image04;
                    }
                    return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "success", Result = slist };
                }
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "200", ErrorMessage = "success", Result = "" };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = ex.Message };

            }
        }
        /// <summary>
        /// 添加项目
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        // POST: api/Subject
        [HttpPost]
        public ApiJsonResult Post([FromBody] SubjectEntity entity)
        {
            try
            {
                if (entity == null)
                {
                    return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = "entity is null", Result = "" };
                }
                _subjectApp.SubmitForm(entity, entity.F_Id);
                return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "success", Result = "" };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = ex.Message };

            }
        }
        /// <summary>
        /// 修改项目
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        // PUT: api/Subject/5
        [HttpPut("{id}")]
        public ApiJsonResult Put(string id, [FromBody] SubjectEntity entity)
        {
            try
            {
                if (entity == null)
                {
                    return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = "entity is null", Result = "" };
                }
                _subjectApp.SubmitForm(entity, entity.F_Id);
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "200", ErrorMessage = "success", Result = "" };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = ex.Message };

            }
        }

        // DELETE: api/ApiWithActions/5
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
        /// <summary>
        /// 获得全部装修流程
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetCPKProcess")]
        public ApiJsonResult GetCPKProcess()
        {
            try
            {
                SubjectUserVM suVM = new SubjectUserVM();
                suVM.ItemDetailList = _itemdetailApp.GetListEnCode("CPKProcess");
                suVM.ClientList = _userApp.GetListByRoleCode("guest");
                suVM.DesignList = _userApp.GetListByRoleCode("design");
                suVM.ImplementList = _userApp.GetListByRoleCode("implement");
                suVM.ManagerList = _userApp.GetListByRoleCode("manager");
                suVM.IPQCList = _userApp.GetListByRoleCode("IPQC");

                return new ApiJsonResult { IsSucceed = false, ErrorCode = "200", ErrorMessage = "success", Result = suVM };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = ex.Message };

            }
        }

    }
}
