﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CPK.Application;
using CPK.Application.SystemManage;
using CPK.Application.SystemSecurity;
using CPK.Code;
using CPK.Domain.Entity.SystemManage;
using CPK.Domain.Entity.SystemSecurity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PMS.Code;

namespace CPK.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        // POST: api/User
        [HttpPost]
        public ApiJsonResult Post([FromBody] LoginEntity entity)
        {
            if (entity == null)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = "entity is null"};

            }
            LogEntity logEntity = new LogEntity();
            logEntity.F_ModuleName = "系统登录";
            logEntity.F_Type = DbLogType.Login.ToString();
            try
            {
               var pw= Md5.md5(entity.Password, 32);
                entity.Password = Md5.md5(entity.Password, 32);
                UserEntity userEntity = new UserApp().CheckLogin(entity.UserName, entity.Password);
                if (userEntity != null)
                {
      
                    logEntity.F_Account = userEntity.F_Account;
                    logEntity.F_NickName = userEntity.F_RealName;
                    logEntity.F_Result = true;
                    logEntity.F_Description = "登录成功";
                    logEntity.F_CreatorUserId = userEntity.F_Id;
                    logEntity.F_CreatorUserName = userEntity.F_Account;
                    new LogApp().WriteAPIDbLog(logEntity);
                }
                return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "", Result = userEntity };

            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "500", ErrorMessage = ex.Message, };

            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class LoginEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Password { get; set; }
    }
}