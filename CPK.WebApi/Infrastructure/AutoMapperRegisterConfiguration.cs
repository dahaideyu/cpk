﻿using CPK.Domain.Entity.SystemManage;
using CPK.Domain.ViewModel;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPK.Domain.Entity.ProductManage;

namespace CPK.WebApi.Infrastructure
{
    /// <summary>
    /// 
    /// </summary>
    public class AutoMapperRegisterConfiguration:Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public AutoMapperRegisterConfiguration()
        {
            ///设置映射关系
            CreateMap<ChartRPSMessageVM, ChartJobResultEntity>()
                .ForMember(dest=>dest.F_ChartId,opt=>opt.MapFrom(src=>src.ChartId))
                .ForMember(dest => dest.F_ChartName, opt => opt.MapFrom(src => src.ChartName))
                .ForMember(dest => dest.F_Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.F_StartTime, opt => opt.MapFrom(src => src.StartTime))
                .ForMember(dest => dest.F_EndTime, opt => opt.MapFrom(src => src.EndTime));
            CreateMap<ChartJobResultEntity, ChartRPSMessageVM>().ForMember(dest => dest.ChartId, opt => opt.MapFrom(src => src.F_ChartId))
                .ForMember(dest => dest.ChartName, opt => opt.MapFrom(src => src.F_ChartName))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.F_Description))
                .ForMember(dest => dest.StartTime, opt => opt.MapFrom(src => src.F_StartTime))
                .ForMember(dest => dest.EndTime, opt => opt.MapFrom(src => src.F_EndTime)); 

            CreateMap<APIUserVM, UserEntity>();
            CreateMap<UserEntity, APIUserVM>();
        }
    }

}
