﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CPK.WebApi.Infrastructure
{
    /// <summary>
    /// 
    /// </summary>
    public class AppSettingsModel
    {/// <summary>
     /// 登陆提供者模式：Session、Cookie
     /// </summary>
        public string LoginProvider { get; set; }
        /// <summary>
        /// 启用系统日志
        /// </summary>
        public bool IsLog { get; set; }
        /// <summary>
        /// 数据库超时间
        /// </summary>
        public string CommandTimeout { get; set; }
        /// <summary>
        /// 启用IP过滤
        /// </summary>
        public bool IsIPFilter { get; set; }
        /// <summary>
        /// 联系人
        /// </summary>
        public string Contact { get; set; }
        /// <summary>
        /// 软件名
        /// </summary>
        public string SoftName { get; set; }
        /// <summary>
        /// 版本
        /// </summary>
        public string Version { get; set; }
        /// <summary>
        /// 软件授权码 测试备用
        /// </summary>
        public string LicenceKey { get; set; }
        /// <summary>
        /// 邮件名
        /// </summary>
        public string MailName { get; set; }
        /// <summary>
        /// 邮件用户名
        /// </summary>
        public string MailUserName { get; set; }
        /// <summary>
        /// 邮件密码
        /// </summary>
        public string MailPassword { get; set; }
        /// <summary>
        /// 邮件主机地址
        /// </summary>
        public string MailHost { get; set; }
        /// <summary>
        /// 图片主机地址
        /// </summary>
        public string ImageHost { get; set; }
        /// <summary>
        /// 图片保存地址
        /// </summary>
        public string ImageSavePath { get; set; }
    }
}
