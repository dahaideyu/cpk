using CPK.Application.ProductManage;
using CPK.Code;
using CPK.Domain.Entity.ProductManage;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CPK.Web.Areas.ProductManage.Controllers
{
    public class ProductController : BaseController
    {
        private ProductApp _ProductApp = new ProductApp();
        [HttpGet]
        public ActionResult GetTreeSelectJson()
        {
            var data = _ProductApp.GetList();
            var treeList = new List<TreeSelectModel>();
            foreach (var item in data)
            {
                TreeSelectModel treeModel = new TreeSelectModel();
                treeModel.id = item.F_Id;
                treeModel.text = item.F_EnCode;
                treeModel.parentId ="0";
                treeModel.data = item;
                treeList.Add(treeModel);
            }
            return Content(treeList.TreeSelectJson());
        }
        [HttpGet]
        public ActionResult GetTreeGridJson()
        {
            var data = _ProductApp.GetList();
            var treeList = new List<TreeGridModel>();
            foreach (var item in data)
            {
                TreeGridModel treeModel = new TreeGridModel();
                treeModel.id = item.F_Id;
                treeModel.text = item.F_EnCode;
                treeModel.parentId = "0";
                treeModel.expanded = true;
                treeModel.entityJson = item.ToJson();
                treeList.Add(treeModel);
            }
            return Content(treeList.TreeGridJson());
        }



        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = _ProductApp.GetForm(keyValue);
            return Content(data.ToJson());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitForm(ProductEntity eProductEntity, string keyValue)
        {
            
            var lProductlist = _ProductApp.GetList(t => t.F_Id != keyValue);
            var wEntity = lProductlist.Find(c => c.F_EnCode == eProductEntity.F_EnCode);
            if (wEntity != null)
            {
                return Error("EnCode HaveExisted"  );
            }

            _ProductApp.SubmitForm(eProductEntity, keyValue);
            return Success("OperationSuccess");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteForm(string keyValue)
        {
            _ProductApp.DeleteForm(keyValue);
            return Success("DeleSuccess");
        }
    }
}
