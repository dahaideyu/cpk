using CPK.Application.SystemManage;
using CPK.Code;
using CPK.Domain.Entity.SystemManage;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using CPK.Application.ProductManage;

namespace CPK.Web.Areas.ProductManage.Controllers
{
    public class NewsInfoController : BaseController
    {
        private NewsInfoApp _NewsInfoApp = new NewsInfoApp();
        [HttpGet]
        public ActionResult GetTreeSelectJson()
        {
            var data = _NewsInfoApp.GetList();
            var treeList = new List<TreeSelectModel>();
            foreach (var item in data)
            {
                TreeSelectModel treeModel = new TreeSelectModel();
                treeModel.id = item.F_Id;
                treeModel.text = item.F_Title;
                treeModel.parentId ="0";
                treeModel.data = item;
                treeList.Add(treeModel);
            }
            return Content(treeList.TreeSelectJson());
        }
        [HttpGet]
        public ActionResult GetTreeGridJson()
        {
            var data = _NewsInfoApp.GetList();
            var treeList = new List<TreeGridModel>();
            foreach (var item in data)
            {
                TreeGridModel treeModel = new TreeGridModel();
                treeModel.id = item.F_Id;
                treeModel.text = item.F_Title;
                treeModel.parentId = "0";
                treeModel.expanded = true;
                treeModel.entityJson = item.ToJson();
                treeList.Add(treeModel);
            }
            return Content(treeList.TreeGridJson());
        }



        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = _NewsInfoApp.GetForm(keyValue);
            return Content(data.ToJson());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitForm(NewsInfoEntity eNewsInfoEntity, string keyValue)
        {
            _NewsInfoApp.SubmitForm(eNewsInfoEntity, keyValue);
            return Success("OperationSuccess");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteForm(string keyValue)
        {
            _NewsInfoApp.DeleteForm(keyValue);
            return Success("DeleSuccess");
        }
    }
}
