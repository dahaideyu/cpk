using CPK.Application.ProductManage;
using CPK.Code;
using CPK.Domain.Entity.ProductManage;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
namespace CPK.Web.Areas.ProductManage.Controllers
{
    public class SubjectForumController : BaseController
    {
        private SubjectForumApp _SubjectForumApp = new SubjectForumApp();
        [HttpGet]
        public ActionResult GetTreeSelectJson()
        {
            var data = _SubjectForumApp.GetList();
            var treeList = new List<TreeSelectModel>();
            foreach (var item in data)
            {
                TreeSelectModel treeModel = new TreeSelectModel();
                treeModel.id = item.F_Id;
                treeModel.text = item.F_EnCode;
                treeModel.parentId ="0";
                treeModel.data = item;
                treeList.Add(treeModel);
            }
            return Content(treeList.TreeSelectJson());
        }
        [HttpGet]
        public ActionResult GetTreeGridJson()
        {
            var data = _SubjectForumApp.GetList();
            var treeList = new List<TreeGridModel>();
            foreach (var item in data)
            {
                TreeGridModel treeModel = new TreeGridModel();
                treeModel.id = item.F_Id;
                treeModel.text = item.F_EnCode;
                treeModel.parentId = "0";
                treeModel.expanded = true;
                treeModel.entityJson = item.ToJson();
                treeList.Add(treeModel);
            }
            return Content(treeList.TreeGridJson());
        }



        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = _SubjectForumApp.GetForm(keyValue);
            return Content(data.ToJson());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitForm(SubjectForumEntity eSubjectForumEntity, string keyValue)
        {
            _SubjectForumApp.SubmitForm(eSubjectForumEntity, keyValue);
            return Success("OperationSuccess");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteForm(string keyValue)
        {
            _SubjectForumApp.DeleteForm(keyValue);
            return Success("DeleSuccess");
        }
    }
}
