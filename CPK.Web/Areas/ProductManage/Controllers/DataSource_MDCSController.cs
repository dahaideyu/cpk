using CPK.Code;
using CPK.Domain.Entity.ProductManage;
using CPK.Application.ProductManage;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System;
using CPK.Application.Infrastructure;
using System.Data;
using CPK.Domain.ViewModel;
using CPK.Application;

namespace CPK.Web.Areas.ProductManage.Controllers
{
    public class DataSource_MDCSController : BaseController
    {
        private DataSource_MDCSApp _DataSource_MDCSApp = new DataSource_MDCSApp();
        [HttpGet]
        public ActionResult GetSelectJson()
        {
            var list=_DataSource_MDCSApp.GetList();
            var data = from item in list
                    select new 
                    {
                        item.F_Id,
                        item.F_ConnectString,
                        Name = item.F_Line
                        +"-"+_DataSource_MDCSApp.GetDeviceName(item.F_ConnectString,item.F_Line,item.F_Device)
                        +"-"+_DataSource_MDCSApp.GetVarName(item.F_ConnectString,item.F_Device,item.F_Variable)
                        +(string.IsNullOrEmpty(item.F_BoardType)?"":" [BoardType:"+item.F_BoardType+"]")
                        +(string.IsNullOrEmpty(item.F_MCF)?"":" [MCF Number:"+item.F_MCF+"]"),
                    };
            return Content(data.ToJson());
        }
        public ActionResult GetGridJson(string keyword)
        {
            var list=_DataSource_MDCSApp.GetList();
            var rows = from item in list
                    select new 
                    {
                        item.F_Id,
                        item.F_ConnectString,
                        item.F_Line,
                        item.F_Device,
                        DeviceName = _DataSource_MDCSApp.GetDeviceName(item.F_ConnectString,item.F_Line,item.F_Device),
                        item.F_Variable,
                        VariableName = _DataSource_MDCSApp.GetVarName(item.F_ConnectString,item.F_Device,item.F_Variable),
                        item.F_EnabledMark,
                        item.F_CreatorTime,
                        item.F_DeleteMark,
                        item.F_CreatorUserName,
                        item.F_DeleteUserName,
                        item.F_LastModifyUserName,
                        item.F_MCF,
                        item.F_BoardType,
                    };
                if(!string.IsNullOrWhiteSpace(keyword))
                {
                    rows=rows.Where(t=>t.F_Line.Contains(keyword));
                }
                    
            var data =new { rows = rows};
            return Content(data.ToJson());
        }

        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = _DataSource_MDCSApp.GetForm(keyValue);
            return Content(data.ToJson());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitForm(DataSource_MDCSEntity eDataSource_MDCSEntity, string keyValue)
        {
             if(eDataSource_MDCSEntity.F_MCF==null) eDataSource_MDCSEntity.F_MCF="";
             if(eDataSource_MDCSEntity.F_BoardType==null) eDataSource_MDCSEntity.F_BoardType="";
            _DataSource_MDCSApp.SubmitForm(eDataSource_MDCSEntity, keyValue);
            return Success("OperationSuccess");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteForm(string keyValue)
        {
            _DataSource_MDCSApp.DeleteForm(keyValue);
            return Success("DeleSuccess");
        }

        [HttpGet]
        public ActionResult GetLines(string ConnString)
        {
            DataSourceFromMDCS MDCSDB=new DataSourceFromMDCS(ConnString);
            DataTable dt = MDCSDB.getLinesList("");
            return Content(dt.ToJson());            
        }
        [HttpGet]
        public ActionResult GetDevices(string ConnString,string LineName)
        {
            DataSourceFromMDCS MDCSDB=new DataSourceFromMDCS(ConnString);
            DataTable dt = MDCSDB.getDevicesList("",LineName);
            return Content(dt.ToJson());            
        }
        [HttpGet]
        public ActionResult GetVarriables(string ConnString,string DeviceID)
        {
            DataSourceFromMDCS MDCSDB=new DataSourceFromMDCS(ConnString);
            DataTable dt = MDCSDB.getVariablesList("",DeviceID);
            return Content(dt.ToJson());            
        }
        [HttpGet]
        public ActionResult GetDefualtConnectStirng()
        {
            var data=new { ConnString = Configs.GetValue("MDCSDataConnString") };
            return Content(data.ToJson());            
        }

        [HttpGet]
        public ActionResult GetPreViewData(string ConnString,string lineName,string deviceID,string varID,string startTime,string endTime)
        {
            DataSourceFromMDCS MDCSDB=new DataSourceFromMDCS(ConnString);
            DateTime dStartTime = Convert.ToDateTime(startTime);
            DateTime dEndTime = Convert.ToDateTime(endTime);
            List<DataSourceItemVM> result = MDCSDB.getMDCSDataSource(lineName,deviceID,varID,dStartTime,dEndTime);
            //List<DataSourceCalRsultVM> SequenceGroupResult = DataItemGroupCom.SequenceSortGroup(result,6);
            //List<DataSourceCalRsultVM> PickSortGroupResult = DataItemGroupCom.PickSortGroup(result,5,10);
            //List<DataSourceCalRsultVM> PickSortGroupTime = DataItemGroupCom.PickSortGroupByTime(result,5,30);
            var data = new { rows = result };
            return Content(data.ToJson());
        }

        [HttpGet]
        public ActionResult GetPreSequenceGroupResultData(string keyValue, string startTime,string endTime)
        {
            DataSource_MDCSEntity dbsource=_DataSource_MDCSApp.GetForm(keyValue);
            string ConnString=dbsource.F_ConnectString;
            string lineName=dbsource.F_Line;
            string deviceID=dbsource.F_Device;
            string varID=dbsource.F_Variable;
            DataSourceFromMDCS MDCSDB=new DataSourceFromMDCS(ConnString);
            DateTime dStartTime = Convert.ToDateTime(startTime);
            DateTime dEndTime = Convert.ToDateTime(endTime);
            List<DataSourceItemVM> result = MDCSDB.getMDCSDataSource(lineName,deviceID,varID,dStartTime,dEndTime);
            List<DataSourceCalRsultVM> SequenceGroupResult = DataItemGroupCom.SequenceSortGroup(result,6);
            CPKResultVM cpkresult=DataItemGroupCom.CaculateCPK(result,60,66);
            var data= new {SPC= SequenceGroupResult,CPK=cpkresult,LSL=60,USL=66};
            return Content(data.ToJson());
        }


    }
}
