using CPK.Code;
using CPK.Domain.Entity.ProductManage;
using CPK.Application.ProductManage;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System;

namespace CPK.Web.Areas.ProductManage.Controllers
{
    public class Chart_Parameters_SPCController : BaseController
    {
        private Chart_Parameters_SPCApp _Chart_Parameters_SPCApp = new Chart_Parameters_SPCApp();
        [HttpGet]
        public ActionResult GetTreeSelectJson()
        {
            var data = _Chart_Parameters_SPCApp.GetList();
            var treeList = new List<TreeSelectModel>();
            foreach (var item in data)
            {
                TreeSelectModel treeModel = new TreeSelectModel();
                treeModel.id = item.F_Id;
                treeModel.parentId ="0";
                treeModel.data = item;
                treeList.Add(treeModel);
            }
            return Content(treeList.TreeSelectJson());
        }
        [HttpGet]
        public ActionResult GetTreeGridJson()
        {
            var data = _Chart_Parameters_SPCApp.GetList();
            var treeList = new List<TreeGridModel>();
            foreach (var item in data)
            {
                TreeGridModel treeModel = new TreeGridModel();
                treeModel.id = item.F_Id;
                treeModel.parentId = "0";
                treeModel.expanded = true;
                treeModel.entityJson = item.ToJson();
                treeList.Add(treeModel);
            }
            return Content(treeList.TreeGridJson());
        }



        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = _Chart_Parameters_SPCApp.GetForm(keyValue);
            return Content(data.ToJson());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitForm(Chart_Parameters_SPCEntity eChart_Parameters_SPCEntity, string keyValue)
        {
            
            _Chart_Parameters_SPCApp.SubmitForm(eChart_Parameters_SPCEntity, keyValue);
            return Success("OperationSuccess");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteForm(string keyValue)
        {
            _Chart_Parameters_SPCApp.DeleteForm(keyValue);
            return Success("DeleSuccess");
        }
    }
}
