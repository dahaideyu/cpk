using CPK.Code;
using CPK.Domain.Entity.ProductManage;
using CPK.Application.ProductManage;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System;
using CPK.Application.Infrastructure;
using CPK.Domain.ViewModel;
using CPK.Application;

namespace CPK.Web.Areas.ProductManage.Controllers
{
    public class ChartController : BaseController
    {
        private ChartApp _ChartApp = new ChartApp();
        private DataSource_MDCSApp _MDCSApp =new DataSource_MDCSApp();
        private Chart_Parameters_SPCApp _SPC =new Chart_Parameters_SPCApp();
        private Chart_Parameters_CPKApp _CPK =new Chart_Parameters_CPKApp();
        private DataSource_MDCSApp _DataSource_MDCSApp = new DataSource_MDCSApp();

        [HttpGet]
        public ActionResult GetTreeSelectJson()
        {
            var data = _ChartApp.GetList();
            var treeList = new List<TreeSelectModel>();
            foreach (var item in data)
            {
                TreeSelectModel treeModel = new TreeSelectModel();
                treeModel.id = item.F_Id;
                treeModel.text = item.F_EnCode;
                treeModel.parentId ="0";
                treeModel.data = item;
                treeList.Add(treeModel);
            }
            return Content(treeList.TreeSelectJson());
        }
        [HttpGet]
        public ActionResult GetGridJson(string keyword)
        {
            var expression = ExtLinq.True<ChartEntity>();
            if(!string.IsNullOrWhiteSpace(keyword))
            {
                expression=expression.And(t=>t.F_EnCode.ToUpper().Contains(keyword.ToUpper()));
            }
            var list = _ChartApp.GetList(expression);
             var rows = from item in list
                    select new 
                    {
                        item.F_Id,
                        item.F_EnCode,
                        item.F_DataSourceId,
                        DataSource= _MDCSApp.GetForm(item.F_DataSourceId).F_Line 
                        + "-" + Convert.ToString(_MDCSApp.GetDeviceName(_MDCSApp.GetForm(item.F_DataSourceId).F_ConnectString,_MDCSApp.GetForm(item.F_DataSourceId).F_Line,_MDCSApp.GetForm(item.F_DataSourceId).F_Device))
                        + "-" + Convert.ToString(_MDCSApp.GetVarName(_MDCSApp.GetForm(item.F_DataSourceId).F_ConnectString,_MDCSApp.GetForm(item.F_DataSourceId).F_Device,_MDCSApp.GetForm(item.F_DataSourceId).F_Variable)),
                        item.F_FullName,
                        item.F_Description,
                        item.F_FrequencyType,
                        item.F_Frequency,
                        item.F_LastLoadTime,
                        item.F_SortCode,
                        item.F_EnabledMark,
                        item.F_CreatorTime,
                        item.F_DeleteMark,
                        item.F_CreatorUserName,
                        item.F_DeleteUserName,
                        item.F_LastModifyUserName
                    };
                    
            var data =new { rows = rows};
            return Content(data.ToJson());
        }



        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = _ChartApp.GetForm(keyValue);
            return Content(data.ToJson());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitForm(ChartEntity eChartEntity, string keyValue)
        {
            if(eChartEntity.F_DirtyRead==null) eChartEntity.F_DirtyRead="";
            _ChartApp.SubmitForm(eChartEntity, keyValue);
            var data= new {F_Id = eChartEntity.F_Id };
            return Success("OperationSuccess",data);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteForm(string keyValue)
        {
            _ChartApp.DeleteForm(keyValue);
            return Success("DeleSuccess");
        }

        [HttpGet]
        public ActionResult GetDataSoure(string keyValue)
        {
            var chartitem = _ChartApp.GetForm(keyValue);
            var spcitem = _SPC.GetForm(keyValue);
            var cpkitem = _CPK.GetForm(keyValue);
            var mdcsitem = _MDCSApp.GetForm(chartitem.F_DataSourceId);
            var data=new {chart=chartitem,mdcsdata=mdcsitem,spcparam=spcitem,cpkparam=cpkitem};
            return Content(data.ToJson());
        }

        [HttpGet]
        // [HandlerAuthorize]
        public  ActionResult Preview()
        {
            return View();
        }

        [HttpGet] 
        public ActionResult GetMonitorDateNow(string keyValue)
        {
            var chartitem = _ChartApp.GetForm(keyValue);
            DateTime startTime = Convert.ToDateTime(chartitem.F_LastLoadTime);//_ChartApp.GetMonitorStartDate(chartitem);
            var data = new {startTime=startTime.ToString(), endTime = DateTime.Now.ToString()};
            return Content(data.ToJson());
        }

        [HttpGet]
         public ActionResult GetPreViewData(string ChartId,string startTime,string endTime)
        {
            ChartEntity chartparam=_ChartApp.GetForm(ChartId);
            DataSource_MDCSEntity MDCSEntity=_MDCSApp.GetForm(chartparam.F_DataSourceId);
            DataSourceFromMDCS MDCSDB=new DataSourceFromMDCS(MDCSEntity.F_ConnectString);
            DateTime dStartTime = Convert.ToDateTime(startTime);
            DateTime dEndTime = Convert.ToDateTime(endTime);
            string[] DirtyRead=null;
            if(!string.IsNullOrWhiteSpace(chartparam.F_DirtyRead))
            {
                DirtyRead=chartparam.F_DirtyRead.Trim().TrimEnd(',').Split(',');
            }
            List<DataSourceItemVM> result = MDCSDB.getMDCSDataSource(MDCSEntity.F_Line,MDCSEntity.F_Device,MDCSEntity.F_Variable,dStartTime,dEndTime,DirtyRead);
            //List<DataSourceCalRsultVM> SequenceGroupResult = DataItemGroupCom.SequenceSortGroup(result,6);
            //List<DataSourceCalRsultVM> PickSortGroupResult = DataItemGroupCom.PickSortGroup(result,5,10);
            //List<DataSourceCalRsultVM> PickSortGroupTime = DataItemGroupCom.PickSortGroupByTime(result,5,30);
            var data = new { rows = result };
            return Content(data.ToJson());
        }

        [HttpGet]
        public ActionResult GetChartData(string keyValue,string startTime,string endTime)
        {
            GroupCalSPCResultVM GroupResult = new GroupCalSPCResultVM();
            CPKResultVM cpkresult=new CPKResultVM();
            _ChartApp.GetChartData(keyValue,startTime,endTime,out GroupResult,out cpkresult);
            var cpkendTime = DateTime.Now;
            var cpkstartTime = cpkendTime.AddDays(-7);
            DateTime.TryParse(startTime,out cpkstartTime);
            DateTime.TryParse(endTime, out cpkendTime);
           

            var cpkLineResult = new CPKResultApp().GetList(t=>t.F_ChartId==keyValue&&t.F_LastLoadTime>= cpkstartTime&&t.F_LastLoadTime < cpkendTime);
            var data= new {SPC= GroupResult,CPK=cpkresult,LSL=(cpkresult==null?0:cpkresult.LSL),USL=(cpkresult==null?0:cpkresult.USL), CPKLineResult = cpkLineResult };
            return Content(data.ToJson());
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetChangeCPKChartData(string keyValue, string endTime)
        {
            var cpkentity = _CPK.GetForm(keyValue);
            if (cpkentity == null)
            {
                return Content("");
            }
            var cpkendTime = DateTime.Now;
            var cpkstartTime = cpkendTime.AddDays(-7);
            DateTime.TryParse(endTime, out cpkendTime);
            cpkstartTime = cpkendTime.AddHours(-cpkentity.F_Frequency.Value);
            var startTime = cpkstartTime.ToString("yyyy-MM-dd HH:MM");
            GroupCalSPCResultVM GroupResult = new GroupCalSPCResultVM();
            CPKResultVM cpkresult = new CPKResultVM();
            _ChartApp.GetChartData(keyValue, startTime, endTime, out GroupResult, out cpkresult);
            
            DateTime.TryParse(startTime, out cpkstartTime);
            


            var cpkLineResult = new CPKResultApp().GetList(t => t.F_ChartId == keyValue && t.F_LastLoadTime >= cpkstartTime && t.F_LastLoadTime < cpkendTime);
            var data = new { SPC = GroupResult, CPK = cpkresult, LSL = (cpkresult == null ? 0 : cpkresult.LSL), USL = (cpkresult == null ? 0 : cpkresult.USL), CPKLineResult = cpkLineResult };
            return Content(data.ToJson());
        }


    }
}
