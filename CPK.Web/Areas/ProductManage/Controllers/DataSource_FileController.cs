using CPK.Code;
using CPK.Domain.Entity.ProductManage;
using CPK.Application.ProductManage;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System;

namespace CPK.Web.Areas.ProductManage.Controllers
{
    public class DataSource_FileController : BaseController
    {
        private DataSource_FileApp _DataSource_FileApp = new DataSource_FileApp();
        [HttpGet]
        public ActionResult GetTreeSelectJson()
        {
            var data = _DataSource_FileApp.GetList();
            var treeList = new List<TreeSelectModel>();
            foreach (var item in data)
            {
                TreeSelectModel treeModel = new TreeSelectModel();
                treeModel.id = item.F_Id;
                treeModel.parentId ="0";
                treeModel.data = item;
                treeList.Add(treeModel);
            }
            return Content(treeList.TreeSelectJson());
        }
        [HttpGet]
        public ActionResult GetTreeGridJson()
        {
            var data = _DataSource_FileApp.GetList();
            var treeList = new List<TreeGridModel>();
            foreach (var item in data)
            {
                TreeGridModel treeModel = new TreeGridModel();
                treeModel.id = item.F_Id;
                treeModel.parentId = "0";
                treeModel.expanded = true;
                treeModel.entityJson = item.ToJson();
                treeList.Add(treeModel);
            }
            return Content(treeList.TreeGridJson());
        }



        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = _DataSource_FileApp.GetForm(keyValue);
            return Content(data.ToJson());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitForm(DataSource_FileEntity eDataSource_FileEntity, string keyValue)
        {
            
            _DataSource_FileApp.SubmitForm(eDataSource_FileEntity, keyValue);
            return Content("OperationSuccess");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteForm(string keyValue)
        {
            _DataSource_FileApp.DeleteForm(keyValue);
            return Content("DeleSuccess");
        }
    }
}
