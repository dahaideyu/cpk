using CPK.Application.ProductManage;
using CPK.Code;
using CPK.Domain.Entity.ProductManage;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CPK.Web.Areas.ProductManage.Controllers
{
    public class OrderController : BaseController
    {
        private OrderApp _OrderApp = new OrderApp();
        [HttpGet]
        public ActionResult GetTreeSelectJson()
        {
            var data = _OrderApp.GetList();
            var treeList = new List<TreeSelectModel>();
            foreach (var item in data)
            {
                TreeSelectModel treeModel = new TreeSelectModel();
                treeModel.id = item.F_Id;
                treeModel.text = item.F_OrderNo;
                treeModel.parentId ="0";
                treeModel.data = item;
                treeList.Add(treeModel);
            }
            return Content(treeList.TreeSelectJson());
        }
        [HttpGet]
        public ActionResult GetTreeGridJson()
        {
            var data = _OrderApp.GetList();
            var treeList = new List<TreeGridModel>();
            foreach (var item in data)
            {
                TreeGridModel treeModel = new TreeGridModel();
                treeModel.id = item.F_Id;
                treeModel.text = item.F_OrderNo;
                treeModel.parentId = "0";
                treeModel.expanded = true;
                treeModel.entityJson = item.ToJson();
                treeList.Add(treeModel);
            }
            return Content(treeList.TreeGridJson());
        }



        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = _OrderApp.GetForm(keyValue);
            return Content(data.ToJson());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitForm(OrderEntity eOrderEntity, string keyValue)
        {
            
            var lOrderlist = _OrderApp.GetList(t => t.F_Id != keyValue);
            var wEntity = lOrderlist.Find(c => c.F_OrderNo == eOrderEntity.F_OrderNo);
            if (wEntity != null)
            {
                return Error("EnCode HaveExisted" );
            }

            _OrderApp.SubmitForm(eOrderEntity, keyValue);
            return Success("OperationSuccess");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteForm(string keyValue)
        {
            _OrderApp.DeleteForm(keyValue);
            return Success("DeleSuccess");
        }
    }
}
