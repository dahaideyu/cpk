﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CPK.Application.SystemManage;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PMS.Code;

namespace CPK.Web.Areas.MoblieAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubjectController : ControllerBase
    {
        private UserApp _userApp = new UserApp();
        private RoleApp _roleApp = new RoleApp();
        // GET: api/Subject/5
        [HttpGet]
        public ApiJsonResult GetSubjectByUseId(string userId)
        {
            try
            {
                var userEntity = _userApp.GetForm(userId);
                if (userEntity == null)
                {
                    return new ApiJsonResult { IsSucceed = false, ErrorCode = "403", ErrorMessage = "无此用户", };
                }
                return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "", Result = userEntity };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "500", ErrorMessage = ex.Message, };

            }

        }
        // GET: api/Subject
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }



        // POST: api/Subject
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Subject/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
