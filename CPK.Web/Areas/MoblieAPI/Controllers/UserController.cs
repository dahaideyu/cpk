﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CPK.Application;
using CPK.Application.SystemManage;
using CPK.Application.SystemSecurity;
using CPK.Code;
using CPK.Domain.Entity.SystemManage;
using CPK.Domain.Entity.SystemSecurity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PMS.Code;

namespace CPK.Web.Areas.MoblieAPI.Controllers
{
    public class UserController : ControllerBase
    {
        private UserApp _userApp = new UserApp();
        private RoleApp _roleApp = new RoleApp();
        // GET: api/User
        [HttpPost]
        public async Task<ApiJsonResult> Login(string username, string password)
        {

            LogEntity logEntity = new LogEntity();
            logEntity.F_ModuleName = "系统登录";
            logEntity.F_Type = DbLogType.Login.ToString();
            try
            {
                UserEntity userEntity = new UserApp().CheckLogin(username, password);
                if (userEntity != null)
                {
                    OperatorModel operatorModel = new OperatorModel();
                    operatorModel.UserId = userEntity.F_Id;
                    operatorModel.UserCode = userEntity.F_Account;
                    operatorModel.UserName = userEntity.F_RealName;
                    operatorModel.CompanyId = userEntity.F_OrganizeId;
                    operatorModel.DepartmentId = userEntity.F_DepartmentId;
                    operatorModel.RoleId = userEntity.F_RoleId;
                    operatorModel.LoginIPAddress = Net.Ip;
                    operatorModel.LoginIPAddressName = Net.GetLocation(operatorModel.LoginIPAddress);
                    operatorModel.LoginTime = DateTime.Now;
                    operatorModel.LoginToken = DESEncrypt.Encrypt(Guid.NewGuid().ToString());
                    if (userEntity.F_Account == "admin")
                    {
                        operatorModel.IsSystem = true;
                    }
                    else
                    {
                        operatorModel.IsSystem = false;
                    }
                    await OperatorProvider.Provider.AddCurrent(operatorModel);
                    logEntity.F_Account = userEntity.F_Account;
                    logEntity.F_NickName = userEntity.F_RealName;
                    logEntity.F_Result = true;
                    logEntity.F_Description = "登录成功";
                    logEntity.F_CreatorUserId = operatorModel.UserId;
                    logEntity.F_CreatorUserName = operatorModel.UserName;
                    new LogApp().WriteDbLog(logEntity);
                }
                return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "", Result = userEntity };

            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "500", ErrorMessage = ex.Message, };

            }

        }


        // GET: api/User/5
        [HttpGet]
        public ApiJsonResult GetUserInfo(string id)
        {
            try
            {
              var userEntity=  _userApp.GetForm(id);
                return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "", Result= userEntity };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "500", ErrorMessage = ex.Message, };

            }
        }
        [HttpGet]
        public ApiJsonResult GetRoleInfo(string id)
        {
            try
            {
                var entity = _roleApp.GetForm(id);
                return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "", Result = entity };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "500", ErrorMessage = ex.Message, };

            }
        }
        /// <summary>
        /// 获得全部角色列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ApiJsonResult GetRoleList()
        {
            try
            {
                var entityList = _roleApp.GetList();
                return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "", Result = entityList };
            }
            catch (Exception ex)
            {
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "500", ErrorMessage = ex.Message, };

            }
        }

        // // POST: api/User
        // [HttpPost]
        // public async Task<ApiJsonResult> Post([FromBody] UserEntity entity)
        // {
        //     try
        //     {
        //         if (entity == null)
        //         {
        //             return new ApiJsonResult { IsSucceed = false, ErrorCode = "401", ErrorMessage = "value is null", };
        //         }
        //         var lgEntity = new UserLogOnEntity();
        //         _userApp.SubmitForm(entity, lgEntity, entity.F_Id);
        //         return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "", };
        //     }
        //     catch (Exception ex)
        //     {
        //         return new ApiJsonResult { IsSucceed = false, ErrorCode = "500", ErrorMessage = ex.Message, };

        //     }
        // }

        // // PUT: api/User/5
        // [HttpPut("{id}")]
        // public void Put(int id, [FromBody] string value)
        // {
        // }

        // // DELETE: api/ApiWithActions/5
        // [HttpDelete("{id}")]
        // public void Delete(int id)
        // {
        // }
    }
}
