﻿using CPK.Code;
using Microsoft.Extensions.Hosting;
using NLog;
using NLog.Fluent;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CPK.Web
{
    public class QuartzService : IHostedService
    {
        // private readonly ILogger _logger;
        private IScheduler _scheduler;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                _logger.Info("开始Quartz调度...");
                NameValueCollection props = new NameValueCollection
                {
                    { "quartz.serializer.type", "binary" }
                };
                props["quartz.threadPool.threadCount"] = "99";
                StdSchedulerFactory factory = new StdSchedulerFactory(props);
                _scheduler = await factory.GetScheduler();
                await _scheduler.Start();
                EchartCheckExc(_scheduler);
                CPKEchartResultExc(_scheduler);
                CPKLinechartResultExc(_scheduler);
            }
            catch (Exception ex)
            {
                var inkk = ex;
            }

        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.Info("停止Quartz调度...");
            await _scheduler.Shutdown(cancellationToken);
        }

        /// <summary>
        /// 发送RPS20%
        /// </summary>
        /// <param name="scheduler"></param>
        public async static void EchartCheckExc(IScheduler scheduler)
        {
            try
            {
              //  int secondrun = 5*60;
                int secondrun = Configs.GetIntValue("ResultJobSecond");

                IJobDetail job = JobBuilder.Create<EchartCheck>()
                .WithIdentity("EchartCheckJob", "EchartCheckGroup")
                .Build();//创建一个作业
                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity("EchartCheckJobTrigger", "EchartCheckGroupTrigger")
                    .WithSimpleSchedule(t =>
                    t.WithIntervalInSeconds(secondrun) //触发执行，s一次
                    .RepeatForever())          //重复执行
                    .Build();


                // Tell quartz to schedule the job using our trigger
                await scheduler.ScheduleJob(job, trigger);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                /// _log.Error(ex.Message);
            }
        }

        /// <summary>
        /// 存储ChartResult 
        /// </summary>
        /// <param name="scheduler"></param>
        public async static void CPKEchartResultExc(IScheduler scheduler)
        {
            try
            {
                int secondrun = 60*20;


                IJobDetail job = JobBuilder.Create<CPKChartResult>()
                .WithIdentity("CPKEchartResultJob", "CPKEchartResultGroup")
                .Build();//创建一个作业
                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity("CPKEchartResultJobTrigger", "CPKEchartResultGroupTrigger")
                    .WithSimpleSchedule(t =>
                    t.WithIntervalInSeconds(secondrun) //触发执行，s一次
                    .RepeatForever())          //重复执行
                    .Build();


                // Tell quartz to schedule the job using our trigger
                await scheduler.ScheduleJob(job, trigger);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                /// _log.Error(ex.Message);
            }
        }

        /// <summary>
        /// 存储ChartResult 
        /// </summary>
        /// <param name="scheduler"></param>
        public async static void CPKLinechartResultExc(IScheduler scheduler)
        {
            try
            {
                int secondrun = 60 * 60*4;


                IJobDetail job = JobBuilder.Create<CPKLineResult>()
                .WithIdentity("CPKLinechartResultJob", "CPKLinechartResultGroup")
                .Build();//创建一个作业
                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity("CPKLinechartResultJobTrigger", "CPKLinechartResultGroupTrigger")
                    .WithSimpleSchedule(t =>
                    t.WithIntervalInSeconds(secondrun) //触发执行，s一次
                    .RepeatForever())          //重复执行
                    .Build();


                // Tell quartz to schedule the job using our trigger
                await scheduler.ScheduleJob(job, trigger);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                /// _log.Error(ex.Message);
            }
        }

    }



}
