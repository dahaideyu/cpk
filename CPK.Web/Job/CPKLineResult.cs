﻿using CPK.Application;
using CPK.Application.Infrastructure;
using CPK.Application.ProductManage;
using CPK.Code;
using CPK.Domain.Entity.ProductManage;
using CPK.Domain.ViewModel;
using NLog;
using Quartz;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
namespace CPK.Web
{
    public class CPKLineResult : IJob
    {
        private ChartApp _ChartApp = new ChartApp();
        private readonly static Logger _logger = LogManager.GetCurrentClassLogger();
        private DataSource_MDCSApp _DataSource_MDCSApp = new DataSource_MDCSApp();
        public Task Execute(IJobExecutionContext context)
        {
            try
            {

                Stopwatch sw = new Stopwatch();
                sw.Start();

               LineCheckCPK();
                //TempleLineCheckCPK();
                _logger.Info(string.Format("[{0:yyyy-MM-dd hh:mm:ss:ffffff}]任务执行！", DateTime.Now));
                Console.WriteLine("EchartResult测试job启动");
                var longtime = sw.ElapsedMilliseconds;
                _logger.Info(string.Format("任务执行总时间：{0}", longtime));
                sw.Stop();
                return Task.CompletedTask;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Task.CompletedTask;
            }
        }
        /// <summary>
        /// 取样CPK并存储
        /// </summary>
        private void LineCheckCPK()
        {
            var cpkparamList = new Chart_Parameters_CPKApp().GetList();


            ChartApp chartapp = new ChartApp();
            var chartlist = chartapp.GetList(t => t.F_EnabledMark == true);
            var CPKresultList = new CPKResultApp().GetList(t=>t.F_CreatorTime>DateTime.Now.AddDays(-2));

            foreach (var chartitem in chartlist)
            {
                DateTime shouldEndTime = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd"));
                DateTime shouldStartTime = Convert.ToDateTime(DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd")); 
                var cpkConfigEntity = CPKresultList?.Where(t => t.F_ChartId == chartitem.F_Id)?.OrderByDescending(o=>o.F_CreatorTime).FirstOrDefault();
                if (cpkConfigEntity != null)
                {

                    if (cpkConfigEntity.F_LastLoadTime != null)
                    {
                        shouldStartTime = cpkConfigEntity.F_LastLoadTime.Value;
                        if (cpkConfigEntity.F_Frequency != null)
                        {
                            shouldEndTime = cpkConfigEntity.F_LastLoadTime.Value.AddHours(cpkConfigEntity.F_Frequency.Value);
                        }
                        
                    }

                    if (shouldEndTime < DateTime.Now)
                    {
                        continue;
                    }
                }

                Chart_Parameters_CPKEntity cpkparam = new Chart_Parameters_CPKEntity();
                if (cpkparamList != null)
                {
                    cpkparam = cpkparamList.Find(t => t.F_Id == chartitem.F_Id);
                }
                DataSource_MDCSEntity dbsource = _DataSource_MDCSApp.GetForm(chartitem.F_DataSourceId);
                string ConnString = dbsource.F_ConnectString;
                string lineName = dbsource.F_Line;
                string deviceID = dbsource.F_Device;
                string varID = dbsource.F_Variable;
                string MCF = dbsource.F_MCF;
                string BoardType = dbsource.F_BoardType;
                DataSourceFromMDCS MDCSDB = new DataSourceFromMDCS(ConnString);
                DateTime dStartTime = Convert.ToDateTime(shouldStartTime);
                DateTime dEndTime = Convert.ToDateTime(shouldEndTime);
                string[] DirtyRead = null;
                if (!string.IsNullOrWhiteSpace(chartitem.F_DirtyRead))
                {
                    DirtyRead = chartitem.F_DirtyRead.Trim().TrimEnd(',').Split(',');
                }
                var result = MDCSDB.getMDCSDataSource(lineName, deviceID, varID, dStartTime, dEndTime, DirtyRead, MCF, BoardType);
                if (result == null)
                {
                    continue;
                }
                var cpkresult = DataItemGroupCom.CaculateCPK(result, Convert.ToDecimal(cpkparam.F_LSL), Convert.ToDecimal(cpkparam.F_USL));
                if (cpkresult == null)
                {
                    continue;
                }
                CPKResultEntity cpkResultEntity = new CPKResultEntity();
                cpkResultEntity.F_LastLoadTime = shouldEndTime;
                cpkResultEntity.F_USL = cpkresult.USL;
                cpkResultEntity.F_LSL = cpkresult.LSL;
                cpkResultEntity.F_Sigma = cpkresult.Sigma;
                cpkResultEntity.F_DistributeStatistics = cpkresult.DistributeStatistics.ToJson();
                cpkResultEntity.F_ChartId = chartitem.F_Id;
                cpkResultEntity.F_CPK = cpkresult.CPK;
                new CPKResultApp().SubmitForm(cpkResultEntity, null);

            }

        }
        /// <summary>
        /// 临时取样CPK并存储
        /// </summary>
        private void TempleLineCheckCPK()
        {
            var cpkparamList = new Chart_Parameters_CPKApp().GetList();


            ChartApp chartapp = new ChartApp();
            var chartlist = chartapp.GetList(t => t.F_EnabledMark == true);
            var CPKresultList = new CPKResultApp().GetList(t => t.F_CreatorTime > DateTime.Now.AddDays(-2));
            for(var i = 0; i < 30; i++)
            {
                foreach (var chartitem in chartlist)
                {
                    DateTime shouldEndTime = Convert.ToDateTime(DateTime.Now.AddDays(i-30).ToString("yyyy-MM-dd"));
                    DateTime shouldStartTime = Convert.ToDateTime(DateTime.Now.AddDays(i - 31).ToString("yyyy-MM-dd"));
                

                    Chart_Parameters_CPKEntity cpkparam = new Chart_Parameters_CPKEntity();
                    if (cpkparamList != null)
                    {
                        cpkparam = cpkparamList.Find(t => t.F_Id == chartitem.F_Id);
                    }
                    DataSource_MDCSEntity dbsource = _DataSource_MDCSApp.GetForm(chartitem.F_DataSourceId);
                    string ConnString = dbsource.F_ConnectString;
                    string lineName = dbsource.F_Line;
                    string deviceID = dbsource.F_Device;
                    string varID = dbsource.F_Variable;
                    string MCF = dbsource.F_MCF;
                    string BoardType = dbsource.F_BoardType;
                    DataSourceFromMDCS MDCSDB = new DataSourceFromMDCS(ConnString);
                    DateTime dStartTime = Convert.ToDateTime(shouldStartTime);
                    DateTime dEndTime = Convert.ToDateTime(shouldEndTime);
                    string[] DirtyRead = null;
                    if (!string.IsNullOrWhiteSpace(chartitem.F_DirtyRead))
                    {
                        DirtyRead = chartitem.F_DirtyRead.Trim().TrimEnd(',').Split(',');
                    }
                    var result = MDCSDB.getMDCSDataSource(lineName, deviceID, varID, dStartTime, dEndTime, DirtyRead, MCF, BoardType);
                    if (result == null)
                    {
                        continue;
                    }
                    var cpkresult = DataItemGroupCom.CaculateCPK(result, Convert.ToDecimal(cpkparam.F_LSL), Convert.ToDecimal(cpkparam.F_USL));
                    if (cpkresult == null)
                    {
                        continue;
                    }
                    CPKResultEntity cpkResultEntity = new CPKResultEntity();
                    cpkResultEntity.F_LastLoadTime = shouldEndTime;
                    cpkResultEntity.F_USL = cpkresult.USL;
                    cpkResultEntity.F_LSL = cpkresult.LSL;
                    cpkResultEntity.F_Sigma = cpkresult.Sigma;
                    cpkResultEntity.F_DistributeStatistics = cpkresult.DistributeStatistics.ToJson();
                    cpkResultEntity.F_ChartId = chartitem.F_Id;
                    cpkResultEntity.F_CPK = cpkresult.CPK;
                    new CPKResultApp().SubmitForm(cpkResultEntity, null);

                }
            }
            

        }
    }
}
