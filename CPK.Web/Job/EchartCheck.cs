﻿using CPK.Application.ProductManage;
using CPK.Code;
using CPK.Domain.Entity.ProductManage;
using CPK.Domain.ViewModel;
using NLog;
using Quartz;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.ServiceModel;
using AndonTEST;

namespace CPK.Web
{
    public class EchartCheck : IJob
    {
        private ChartApp _ChartApp = new ChartApp();
        private readonly static Logger _logger = LogManager.GetCurrentClassLogger();
        Task IJob.Execute(IJobExecutionContext context)
        {
            try
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                List<ChartRPSMessageVM> RPSMsgList = new List<ChartRPSMessageVM>();
                RPSMsgList = MonitorResult();
                //foreach (var item in RPSMsgList)
                //{
                //    OpenRPSCase(item);
                //}
                _logger.Info(string.Format("[{0:yyyy-MM-dd hh:mm:ss:ffffff}]任务执行！", DateTime.Now));
                Console.WriteLine("EchartCheck测试job启动");
                var longtime = sw.ElapsedMilliseconds;
                _logger.Info(string.Format("任务执行总时间：{0}", longtime));
                sw.Stop();
                return Task.CompletedTask;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Task.CompletedTask;
            }

        }

        public List<ChartRPSMessageVM> MonitorResult(bool job = true)
        {
            List<ChartRPSMessageVM> result = new List<ChartRPSMessageVM>();
            ChartApp chartapp = new ChartApp();
            Chart_Parameters_SPCApp spcapp = new Chart_Parameters_SPCApp();
            Chart_Parameters_CPKApp cpkapp = new Chart_Parameters_CPKApp();
            var chartlist = chartapp.GetList(t => t.F_EnabledMark == true);
            foreach (var chartitem in chartlist)
            {
                ChartRPSMessageVM RPSMessage = new ChartRPSMessageVM();
                try
                {
                    RPSMessage.ChartId = chartitem.F_Id;
                    RPSMessage.ChartName = chartitem.F_EnCode;
                    RPSMessage.Description = "";
                    var spcparam = spcapp.GetForm(chartitem.F_Id);
                    var cpkparam = cpkapp.GetForm(chartitem.F_Id);
                    DateTime dtLast = Convert.ToDateTime(chartitem.F_LastLoadTime);
                    DateTime dtFreq = dtLast;
                    int freq = Convert.ToInt32(chartitem.F_Frequency);
                    if (job)
                    {
                        switch (chartitem.F_FrequencyType)
                        {
                            case "S":
                                dtFreq = dtLast.AddSeconds(freq);
                                break;
                            case "MI":
                                dtFreq = dtLast.AddMinutes(freq);
                                break;
                            case "H":
                                dtFreq = dtLast.AddHours(freq);
                                break;
                            case "D":
                                dtFreq = dtLast.AddDays(freq);
                                break;
                            case "W":
                                dtFreq = dtLast.AddDays(freq * 7);
                                break;
                            case "M":
                                dtFreq = dtLast.AddMonths(freq);
                                break;
                            case "Y":
                                dtFreq = dtLast.AddYears(freq);
                                break;
                            default:
                                dtFreq = dtLast.AddMinutes(freq);
                                break;
                        }
                    }
                    else
                    {
                        dtFreq = System.DateTime.Now;
                    }
                    if (System.DateTime.Now >= dtFreq)
                    {
                        GroupCalSPCResultVM GroupResult = new GroupCalSPCResultVM();
                        CPKResultVM cpkresult = new CPKResultVM();
                        _ChartApp.GetChartData(chartitem.F_Id, dtLast.ToString(), dtFreq.ToString(), out GroupResult, out cpkresult);
                        if (GroupResult != null)
                        {
                            if (GroupResult.DataSourceGroupList.Where(t => t.Rule1 == true).Count() > 0)
                            {
                                RPSMessage.Description = RPSMessage.Description + "Xchart:Rule1,1个点落在A区以外;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.Rule2 == true).Count() > 0)
                            {
                                RPSMessage.Description = RPSMessage.Description + "Xchart:Rule2,连续9个点落在中心线的同一侧;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.Rule3 == true).Count() > 0)
                            {
                                RPSMessage.Description = RPSMessage.Description + "Xchart:Rule3,连续6个点递增或递减;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.Rule4 == true).Count() > 0)
                            {
                                RPSMessage.Description = RPSMessage.Description + "Xchart:Rule4,连续14个点中相邻点交替上下;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.Rule5 == true).Count() > 0)
                            {
                                RPSMessage.Description = RPSMessage.Description + "Xchart:Rule5,连续3个点中有2个点落在中心线同一侧的B区以外;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.Rule6 == true).Count() > 0)
                            {
                                RPSMessage.Description = RPSMessage.Description + "Xchart:Rule6,连续5个点中有4个点落在中心线同一侧的C区以外;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.Rule7 == true).Count() > 0)
                            {
                                RPSMessage.Description = RPSMessage.Description + "Xchart:Rule7,连续15个点落在中心线两侧的C区以内;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.Rule8 == true).Count() > 0)
                            {
                                RPSMessage.Description = RPSMessage.Description + "Xchart:Rule8,连续8个点落在中心线两侧且无一在C区内;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.RuleR1 == true).Count() > 0)
                            {
                                RPSMessage.Description = RPSMessage.Description + "Rchart:Rule1,1个点落在A区以外;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.RuleR2 == true).Count() > 0)
                            {
                                RPSMessage.Description = RPSMessage.Description + "Rchart:Rule2,连续9个点落在中心线的同一侧;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.RuleR3 == true).Count() > 0)
                            {
                                RPSMessage.Description = RPSMessage.Description + "Rchart:Rule3,连续6个点递增或递减;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.RuleR4 == true).Count() > 0)
                            {
                                RPSMessage.Description = RPSMessage.Description + "Rchart:Rule4,连续14个点中相邻点交替上下;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.RuleR5 == true).Count() > 0)
                            {
                                RPSMessage.Description = RPSMessage.Description + "Rchart:Rule5,连续3个点中有2个点落在中心线同一侧的B区以外;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.RuleR6 == true).Count() > 0)
                            {
                                RPSMessage.Description = RPSMessage.Description + "Rchart:Rule6,连续5个点中有4个点落在中心线同一侧的C区以外;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.RuleR7 == true).Count() > 0)
                            {
                                RPSMessage.Description = RPSMessage.Description + "Rchart:Rule7,连续15个点落在中心线两侧的C区以内;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.RuleR8 == true).Count() > 0)
                            {
                                RPSMessage.Description = RPSMessage.Description + "Rchart:Rule8,连续8个点落在中心线两侧且无一在C区内;" + System.Environment.NewLine;
                            }
                        }
                        RPSMessage.StartTime = dtLast;
                        RPSMessage.EndTime = dtFreq;
                        if (job)
                        {
                            if (!string.IsNullOrWhiteSpace(RPSMessage.Description))
                            {
                                result.Add(RPSMessage);
                            }
                            //发送RPS 成功才去更新时间
                            if (OpenRPSCase(RPSMessage))
                            {
                                chartitem.F_LastRPSTime = DateTime.Now;
                                chartitem.F_LastMessage = RPSMessage.Description;
                                chartitem.F_LastLoadTime = dtFreq;
                            }


                        }
                        else
                        {
                            result.Add(RPSMessage);
                        }
                    }
                }
                catch (Exception ex)
                {
                    RPSMessage.Description = "Error:" + ex.Message;
                    if (!result.Contains(RPSMessage))
                        result.Add(RPSMessage);
                    continue;
                }
            }
            chartapp.Update(chartlist);
            return result;
        }
        public bool OpenRPSCase(ChartRPSMessageVM RPSMessage)
        {
            ChartEntity chart = _ChartApp.GetForm(RPSMessage.ChartId);
            HttpClient httpClient = new HttpClient();
            string rpsUrl = Configs.GetValue("RPSConnString") + @"Case/";
            string rpsAccount = Configs.GetValue("RPSAccount");
            string rpsBusiness = Configs.GetValue("RPSBusiness");
            string rpsSite = Configs.GetValue("RPSSite");
            string rpsErrorCode = Configs.GetValue("RPSErrorCode");
            if (!string.IsNullOrWhiteSpace(chart.F_RPSLineId))
            {
                var binding = new BasicHttpBinding();
                var endpoint = new EndpointAddress(Configs.GetValue("RPSWebserviceConnString"));
                AndonTEST.AndonServiceSoapClient ar = new AndonTEST.AndonServiceSoapClient(binding, endpoint);
                string caseid = "";
                string siteobject = "";
                string WO = "";
                string strErrorMsg = "";
                var result = ar.OpenCaseAsync(new OpenCaseRequest(rpsBusiness, rpsSite, chart.F_RPSLineId, rpsErrorCode, RPSMessage.Description, rpsAccount, caseid, siteobject, 0, strErrorMsg, WO)).Result;
                if (result != null && !string.IsNullOrWhiteSpace(result.i_CaseID) && result.i_CaseID != "0")
                {
                    return true;
                }
                else {
                    return false;
                }

                //var content = new FormUrlEncodedContent(new Dictionary<string, string>()
                //        {
                //            {"BUid",rpsBusiness},
                //            {"siteid",rpsSite},
                //            {"lineid",chart.F_RPSLineId},
                //            {"errorcode",rpsErrorCode},
                //            {"description",RPSMessage.Description},
                //            {"userid",rpsAccount},
                //        });
                //string rpsresult = "";
                //HttpResponseMessage response = httpClient.PostAsync(rpsUrl, content).Result;
                //if (response.IsSuccessStatusCode)
                //{
                //    rpsresult = response.Content.ReadAsStringAsync().Result;
                //    _logger.Info("RPS start"+ RPSMessage.ChartName);

                //    _logger.Info(rpsresult);
                //    return true;
                //}
                //else
                //{
                //    _logger.Info("RPS send fail" + RPSMessage.ChartName);
                //    return false;
                //}
            }
            else
                return false;
        }
    }
}
