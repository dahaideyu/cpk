﻿using CPK.Application.ProductManage;
using CPK.Code;
using CPK.Domain.Entity.ProductManage;
using CPK.Domain.ViewModel;
using NLog;
using Quartz;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace CPK.Web
{
    public class CPKChartResult : IJob
    {
        private ChartApp _ChartApp = new ChartApp();
        private readonly static Logger _logger = LogManager.GetCurrentClassLogger();
        public Task Execute(IJobExecutionContext context)
        {
            try
            {

                Stopwatch sw = new Stopwatch();
                sw.Start();
            
                ResultCheckData();
                _logger.Info(string.Format("[{0:yyyy-MM-dd hh:mm:ss:ffffff}]任务执行！", DateTime.Now));
                Console.WriteLine("EchartResult测试job启动");
                var longtime = sw.ElapsedMilliseconds;
                _logger.Info(string.Format("任务执行总时间：{0}", longtime));
                sw.Stop();
                return Task.CompletedTask;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return Task.CompletedTask;
            }
        }
        private void ResultCheckData()
        {

            List<ChartJobResultEntity> result = new List<ChartJobResultEntity>();
            ChartJobResultApp chartJobResultApp = new ChartJobResultApp();
            ChartApp chartapp = new ChartApp();
            Chart_Parameters_SPCApp spcapp = new Chart_Parameters_SPCApp();
            Chart_Parameters_CPKApp cpkapp = new Chart_Parameters_CPKApp();
            var chartlist = chartapp.GetList(t => t.F_EnabledMark == true);
            foreach (var chartitem in chartlist)
            {
                ChartJobResultEntity RPSMessage = new ChartJobResultEntity();
                try
                {
                    RPSMessage.F_ChartId = chartitem.F_Id;
                    RPSMessage.F_ChartName = chartitem.F_EnCode;
                    RPSMessage.F_Description = "";
                    var spcparam = spcapp.GetForm(chartitem.F_Id);
                    var cpkparam = cpkapp.GetForm(chartitem.F_Id);
                    DateTime dtLast = Convert.ToDateTime(chartitem.F_LastLoadTime);
                    DateTime dtFreq = dtLast;
                    int freq = Convert.ToInt32(chartitem.F_Frequency);
                    dtFreq = System.DateTime.Now;
                    if (System.DateTime.Now >= dtFreq)
                    {
                        GroupCalSPCResultVM GroupResult = new GroupCalSPCResultVM();
                        CPKResultVM cpkresult = new CPKResultVM();
                        _ChartApp.GetChartData(chartitem.F_Id, dtLast.ToString(), dtFreq.ToString(), out GroupResult, out cpkresult);
                        if (GroupResult != null)
                        {
                            if (GroupResult.DataSourceGroupList.Where(t => t.Rule1 == true).Count() > 0)
                            {
                                RPSMessage.F_Description = RPSMessage.F_Description + "Xchart:Rule1,1个点落在A区以外;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.Rule2 == true).Count() > 0)
                            {
                                RPSMessage.F_Description = RPSMessage.F_Description + "Xchart:Rule2,连续9个点落在中心线的同一侧;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.Rule3 == true).Count() > 0)
                            {
                                RPSMessage.F_Description = RPSMessage.F_Description + "Xchart:Rule3,连续6个点递增或递减;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.Rule4 == true).Count() > 0)
                            {
                                RPSMessage.F_Description = RPSMessage.F_Description + "Xchart:Rule4,连续14个点中相邻点交替上下;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.Rule5 == true).Count() > 0)
                            {
                                RPSMessage.F_Description = RPSMessage.F_Description + "Xchart:Rule5,连续3个点中有2个点落在中心线同一侧的B区以外;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.Rule6 == true).Count() > 0)
                            {
                                RPSMessage.F_Description = RPSMessage.F_Description + "Xchart:Rule6,连续5个点中有4个点落在中心线同一侧的C区以外;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.Rule7 == true).Count() > 0)
                            {
                                RPSMessage.F_Description = RPSMessage.F_Description + "Xchart:Rule7,连续15个点落在中心线两侧的C区以内;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.Rule8 == true).Count() > 0)
                            {
                                RPSMessage.F_Description = RPSMessage.F_Description + "Xchart:Rule8,连续8个点落在中心线两侧且无一在C区内;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.RuleR1 == true).Count() > 0)
                            {
                                RPSMessage.F_Description = RPSMessage.F_Description + "Rchart:Rule1,1个点落在A区以外;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.RuleR2 == true).Count() > 0)
                            {
                                RPSMessage.F_Description = RPSMessage.F_Description + "Rchart:Rule2,连续9个点落在中心线的同一侧;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.RuleR3 == true).Count() > 0)
                            {
                                RPSMessage.F_Description = RPSMessage.F_Description + "Rchart:Rule3,连续6个点递增或递减;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.RuleR4 == true).Count() > 0)
                            {
                                RPSMessage.F_Description = RPSMessage.F_Description + "Rchart:Rule4,连续14个点中相邻点交替上下;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.RuleR5 == true).Count() > 0)
                            {
                                RPSMessage.F_Description = RPSMessage.F_Description + "Rchart:Rule5,连续3个点中有2个点落在中心线同一侧的B区以外;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.RuleR6 == true).Count() > 0)
                            {
                                RPSMessage.F_Description = RPSMessage.F_Description + "Rchart:Rule6,连续5个点中有4个点落在中心线同一侧的C区以外;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.RuleR7 == true).Count() > 0)
                            {
                                RPSMessage.F_Description = RPSMessage.F_Description + "Rchart:Rule7,连续15个点落在中心线两侧的C区以内;" + System.Environment.NewLine;
                            }
                            if (GroupResult.DataSourceGroupList.Where(t => t.RuleR8 == true).Count() > 0)
                            {
                                RPSMessage.F_Description = RPSMessage.F_Description + "Rchart:Rule8,连续8个点落在中心线两侧且无一在C区内;" + System.Environment.NewLine;
                            }
                        }
                        RPSMessage.F_StartTime = dtLast;
                        RPSMessage.F_EndTime = dtFreq;
                        result.Add(RPSMessage);

                    }
                }
                catch (Exception ex)
                {
                    RPSMessage.F_Description = "Error:" + ex.Message;
                    if (!result.Contains(RPSMessage))
                        result.Add(RPSMessage);
                    continue;
                }
                
            }
            chartJobResultApp.Insert(result);
        }
    }
}
