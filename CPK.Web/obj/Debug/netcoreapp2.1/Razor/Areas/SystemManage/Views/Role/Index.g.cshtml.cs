#pragma checksum "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\Role\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "dc49b7b30d54af8aeebf79e7179484e0bfd67bda"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_SystemManage_Views_Role_Index), @"mvc.1.0.view", @"/Areas/SystemManage/Views/Role/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Areas/SystemManage/Views/Role/Index.cshtml", typeof(AspNetCore.Areas_SystemManage_Views_Role_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"dc49b7b30d54af8aeebf79e7179484e0bfd67bda", @"/Areas/SystemManage/Views/Role/Index.cshtml")]
    public class Areas_SystemManage_Views_Role_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\Role\Index.cshtml"
  
    ViewBag.Title = "Index";
    Layout = "~/Views/Shared/_Index.cshtml";

#line default
#line hidden
            BeginContext(83, 221, true);
            WriteLiteral("<script>\r\n   $(function () {\r\n        gridList();\r\n    })\r\n    function gridList() {\r\n        var $gridList = $(\"#gridList\");\r\n        $gridList.dataGrid({\r\n            height: $(window).height() - 96,\r\n            url: \"");
            EndContext();
            BeginContext(305, 33, false);
#line 13 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\Role\Index.cshtml"
             Write(Url.Action("GetGridJson", "Role"));

#line default
#line hidden
            EndContext();
            BeginContext(338, 3669, true);
            WriteLiteral(@""",//'/BaseData/Role/GetGridJson',         //请求后台的URL（*）
            method: 'get',                      //请求方式（*）
            //toolbar: '#bstoolbar',                //工具按钮用哪个容器
            striped: true,                      //是否显示行间隔色
            cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
            pagination: false,                   //是否显示分页（*）
            sortable: true,                     //是否启用排序
            sortOrder: ""asc"",                   //排序方式
            //传递参数（*）
            queryParams: function (params) {
                return {
                    keyword: $(""#txt_keyword"").val()
                };
            },
            //showExport: true,                     //是否显示导出
            //exportDataType: ""basic"",              //basic', 'all', 'selected'.
            sidePagination: ""server"",           //分页方式：client客户端分页，server服务端分页（*）
            pageNumber: 1,                       //初始化加载第一页，默认第一页
            pageSize: 10,                ");
            WriteLiteral(@"       //每页的记录行数（*）
            pageList: [10, 25, 50, 100, 'ALL'],        //可供选择的每页的行数（*）
            //search: true,                       //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
            //strictSearch: true,
            //showColumns: false,                  //是否显示所有的列
            //showRefresh: true,                  //是否显示刷新按钮
            minimumCountColumns: 2,             //最少允许的列数
            clickToSelect: true,                //是否启用点击选中行
            //height: 500,                        //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
            uniqueId: ""F_Id"",                     //每一行的唯一标识，一般为主键列
            //showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
            //cardView: false,                    //是否显示详细视图
            //detailView: false,                   //是否显示父子表
            sortName: ""F_CreatorTime desc"",
            columns: [
                { radio: true },
                { title: '主键', field: 'F_Id', width: 80, align: 'left', visible: false },
            ");
            WriteLiteral(@"    { title: '角色名称', field: 'F_FullName', width: 150, align: 'left' },
                { title: '角色编号', field: 'F_EnCode', width: 150, align: 'left' },
                {
                    title: '角色类型', field: 'F_Type', width: 80, align: 'left',
                    formatter: function (value, row, index) {
                        return top.clients.dataItems[""RoleType""][value] == undefined ? """" : top.clients.dataItems[""RoleType""][value];
                    }
                },
                {
                    title: '归属机构', field: 'F_OrganizeId', width: 150, align: 'left',
                    formatter: function (value, row, index) {
                        return top.clients.organize[value] == null ? """" : top.clients.organize[value].fullname;
                    }
                },
                { title: '创建时间', field: 'F_CreatorTime', width: 80, align: 'left' },
                {
                    title: ""有效"", field: ""F_EnabledMark"", align: ""center"", width: ""60"",
              ");
            WriteLiteral(@"      formatter: function (value, row, index) {
                        if (value == 1)
                        { return ""<i class=\""fa fa-toggle-on\""></i>""; }
                        else
                        { return ""<i class=\""fa fa-toggle-off\""></i>""; }
                    }
                }
            ],
        });
        $(""#btn_search"").click(function () {
            $.currentWindow().$(""#gridList"").bootstrapTable('refresh');
        });
    }
    function btn_add() {
        $.modalOpen({
            id: ""Form"",
            title: ""新增角色"",
            url: """);
            EndContext();
            BeginContext(4008, 39, false);
#line 82 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\Role\Index.cshtml"
             Write(Url.Content("~/SystemManage/Role/Form"));

#line default
#line hidden
            EndContext();
            BeginContext(4047, 307, true);
            WriteLiteral(@""",
            width: ""550px"",
            height: ""570px"",
            btn: null
        });
    }
    function btn_edit() {
        var keyValue = $(""#gridList"").bootstrapTable('getSelections')[0].F_Id;
        $.modalOpen({
            id: ""Form"",
            title: ""修改角色"",
            url: """);
            EndContext();
            BeginContext(4355, 39, false);
#line 93 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\Role\Index.cshtml"
             Write(Url.Content("~/SystemManage/Role/Form"));

#line default
#line hidden
            EndContext();
            BeginContext(4394, 198, true);
            WriteLiteral("?keyValue=\" + keyValue,\r\n            width: \"550px\",\r\n            height: \"570px\",\r\n            btn: null\r\n        });\r\n    }\r\n    function btn_delete() {\r\n        $.deleteForm({\r\n            url: \"");
            EndContext();
            BeginContext(4593, 45, false);
#line 101 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\Role\Index.cshtml"
             Write(Url.Content("~/SystemManage/Role/DeleteForm"));

#line default
#line hidden
            EndContext();
            BeginContext(4638, 444, true);
            WriteLiteral(@""",
            param: { keyValue: $(""#gridList"").bootstrapTable('getSelections')[0].F_Id },
            success: function () {
                $.currentWindow().$(""#gridList"").trigger(""reloadGrid"");
            }
        })
    }
    function btn_details() {
        var keyValue = $(""#gridList"").bootstrapTable('getSelections')[0].F_Id;
        $.modalOpen({
            id: ""Details"",
            title: ""查看角色"",
            url: """);
            EndContext();
            BeginContext(5083, 42, false);
#line 113 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\Role\Index.cshtml"
             Write(Url.Content("~/SystemManage/Role/Details"));

#line default
#line hidden
            EndContext();
            BeginContext(5125, 1859, true);
            WriteLiteral(@"?keyValue="" + keyValue,
            width: ""550px"",
            height: ""620px"",
            btn: null,
        });
    }
</script>

<div class=""topPanel"">
    <div class=""toolbar"">
        <div class=""btn-group"">
            <a class=""btn btn-primary"" onclick=""$.reload()""><span class=""glyphicon glyphicon-refresh""></span></a>
        </div>
        <div class=""btn-group"">
            <a id=""NF-add"" authorize=""yes"" class=""btn btn-primary dropdown-text"" onclick=""btn_add()""><i class=""fa fa-plus""></i>新建角色</a>
        </div>
        <div class=""operate"">
            <ul class=""nav nav-pills"">
                <li class=""first"">已选中<span>1</span>项</li>
                <li><a id=""NF-edit"" authorize=""yes"" onclick=""btn_edit()""><i class=""fa fa-pencil-square-o""></i>修改角色</a></li>
                <li><a id=""NF-delete"" authorize=""yes"" onclick=""btn_delete()""><i class=""fa fa-trash-o""></i>删除角色</a></li>
                <li><a id=""NF-Details"" authorize=""yes"" onclick=""btn_details()""><i class=""fa fa-search-plus");
            WriteLiteral(@"""></i>查看角色</a></li>
            </ul>
            <a href=""javascript:;"" class=""close""></a>
        </div>
        <script>$('.toolbar').authorizeButton()</script>
    </div>
    <div class=""search"">
        <table>
            <tr>
                <td>
                    <div class=""input-group"">
                        <input id=""txt_keyword"" type=""text"" class=""form-control"" placeholder=""角色名称/角色编号"" style=""width: 200px;"">
                        <span class=""input-group-btn"">
                            <button id=""btn_search"" type=""button"" class=""btn  btn-primary""><i class=""fa fa-search""></i></button>
                        </span>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
<div class=""gridPanel"">
    <table id=""gridList""></table>
</div>
");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
