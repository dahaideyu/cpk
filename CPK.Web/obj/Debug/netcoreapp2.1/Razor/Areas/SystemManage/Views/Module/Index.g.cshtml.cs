#pragma checksum "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\Module\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2a2eb3c87616b3bd8c6d6556db14283b82359023"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_SystemManage_Views_Module_Index), @"mvc.1.0.view", @"/Areas/SystemManage/Views/Module/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Areas/SystemManage/Views/Module/Index.cshtml", typeof(AspNetCore.Areas_SystemManage_Views_Module_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2a2eb3c87616b3bd8c6d6556db14283b82359023", @"/Areas/SystemManage/Views/Module/Index.cshtml")]
    public class Areas_SystemManage_Views_Module_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\Module\Index.cshtml"
  
    ViewBag.Title = "Index";
    Layout = "~/Views/Shared/_Index.cshtml";

#line default
#line hidden
            BeginContext(83, 394, true);
            WriteLiteral(@"<script>
    $(function () {
        $.support.cors = true;
        var oTable = new TableInit();
        oTable.Init();

    })
    var TableInit = function () {

        var oTableInit = new Object();

        /*初始化Table*/

        oTableInit.Init = function () {

            $('#gridList').dataGrid({
                height: $(window).height() - 90,
                url:  """);
            EndContext();
            BeginContext(478, 39, false);
#line 22 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\Module\Index.cshtml"
                  Write(Url.Action("GetTreeGridJson", "Module"));

#line default
#line hidden
            EndContext();
            BeginContext(517, 5245, true);
            WriteLiteral(@""",//'/BaseData/Module/GetTreeGridJson',         //请求后台的URL（*）
                method: 'get',                      //请求方式（*）
                //toolbar: '#bstoolbar',                //工具按钮用哪个容器
                //striped: true,                      //是否显示行间隔色
                cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
                pagination: false,                   //是否显示分页（*）
                //sortable: true,                     //是否启用排序
                //sortOrder: ""asc"",                   //排序方式
                //传递参数（*）
                queryParams: function (params) { return { keyword: $(""#txt_keyword"").val() }; },
                //showExport: true,                     //是否显示导出
                //exportDataType: ""basic"",              //basic', 'all', 'selected'.
                sidePagination: ""server"",           //分页方式：client客户端分页，server服务端分页（*）
                pageNumber: 1,                       //初始化加载第一页，默认第一页
                pageSize: 10,                ");
            WriteLiteral(@"       //每页的记录行数（*）
                pageList: [10, 25],        //可供选择的每页的行数（*）
                //search: true,                       //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
                //strictSearch: true,
                //showColumns: false,                  //是否显示所有的列
                //showRefresh: true,                  //是否显示刷新按钮
                //minimumCountColumns: 2,             //最少允许的列数
                clickToSelect: true,                //是否启用点击选中行
                //height: 500,                        //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
                uniqueId: ""F_Id"",                     //每一行的唯一标识，一般为主键列
                //showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
                //cardView: false,                    //是否显示详细视图
                //detailView: true,                   //是否显示父子表
                treeGrid: true,
                treeGridMainID: ""F_Id"",
                treeGridParentID: ""F_ParentId"",
                sortName: """",
                col");
            WriteLiteral(@"umns: [
                   { radio: true },
                   { title: ""主键"", field: ""F_Id"", align: ""center"", visible: false },
                   { title: ""名称"", field: ""F_FullName"", align: ""left"", width: ""200"" },
                   { title: ""连接"", field: ""F_UrlAddress"", align: ""left"", width: ""260"" },
                   {
                       title: ""目标"", field: ""F_Target"", align: ""center"", width: ""100"",
                       formatter: function (value, row, index) {
                           if (value == ""expand"")
                           { return ""无页面""; }
                           else if (value == ""iframe"")
                           { return ""框架页""; }
                           else if (value == ""open"")
                           { return ""弹出页""; }
                           else if (value == ""blank"")
                           { return ""新窗口""; }
                       }
                   },
                   {
                       title: ""菜单"", field: ""F_IsMenu"", align: ""center"",");
            WriteLiteral(@" width: ""60"",
                       formatter: function (value, row, index) {
                           if (value == true)
                           { return ""<i class=\""fa fa-toggle-on\""></i>""; }
                           else
                           { return ""<i class=\""fa fa-toggle-off\""></i>""; }
                       }
                   },
                   {
                       title: ""展开"", field: ""F_IsExpand"", align: ""center"", width: ""60"",
                       formatter: function (value, row, index) {
                           if (value == true)
                           { return ""<i class=\""fa fa-toggle-on\""></i>""; }
                           else
                           { return ""<i class=\""fa fa-toggle-off\""></i>""; }
                       }
                   },
                   {
                       title: ""公共"", field: ""F_IsPublic"", align: ""center"", width: ""60"",
                       formatter: function (value, row, index) {
                           ");
            WriteLiteral(@"if (value == true)
                           { return ""<i class=\""fa fa-toggle-on\""></i>""; }
                           else
                           { return ""<i class=\""fa fa-toggle-off\""></i>""; }
                       }
                   },
                   {
                       title: ""有效"", field: ""F_EnabledMark"", align: ""center"", width: ""60"",
                       formatter: function (value, row, index) {
                           if (value == 1)
                           { return ""<i class=\""fa fa-toggle-on\""></i>""; }
                           else
                           { return ""<i class=\""fa fa-toggle-off\""></i>""; }
                       }
                   },
                   { title: ""介绍"", field: ""F_Description"", align: ""left"", width: ""300"" },
                ]
            });
            $(""#btn_search"").click(function () {
                $.currentWindow().$(""#gridList"").bootstrapTable('refresh');
            });
        }
        return oTableInit;
  ");
            WriteLiteral("  }\r\n    function btn_add() {\r\n        $.modalOpen({\r\n            id: \"Form\",\r\n            title: \"新增菜单\",\r\n            url: \"");
            EndContext();
            BeginContext(5763, 41, false);
#line 120 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\Module\Index.cshtml"
             Write(Url.Content("~/SystemManage/Module/Form"));

#line default
#line hidden
            EndContext();
            BeginContext(5804, 396, true);
            WriteLiteral(@""",
            width: ""700px"",
            height: ""440px"",
            callBack: function (iframeId) {
                top.frames[iframeId].submitForm();
            }
        });
    }
    function btn_edit() {
        var keyValue = $(""#gridList"").bootstrapTable('getSelections')[0].F_Id;
        $.modalOpen({
            id: ""Form"",
            title: ""修改菜单"",
            url: """);
            EndContext();
            BeginContext(6201, 41, false);
#line 133 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\Module\Index.cshtml"
             Write(Url.Content("~/SystemManage/Module/Form"));

#line default
#line hidden
            EndContext();
            BeginContext(6242, 287, true);
            WriteLiteral(@"?keyValue="" + keyValue,
            width: ""700px"",
            height: ""440px"",
            callBack: function (iframeId) {
                top.frames[iframeId].submitForm();
            }
        });
    }
    function btn_delete() {
        $.deleteForm({
            url: """);
            EndContext();
            BeginContext(6530, 47, false);
#line 143 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\Module\Index.cshtml"
             Write(Url.Content("~/SystemManage/Module/DeleteForm"));

#line default
#line hidden
            EndContext();
            BeginContext(6577, 484, true);
            WriteLiteral(@""",
            param: { keyValue: $(""#gridList"").bootstrapTable('getSelections')[0].F_Id },
            success: function () {
                $.currentWindow().$(""#gridList"").bootstrapTable('refresh');
                $.loading(false);
            }
        })
    }
    function btn_details() {
        var keyValue = $(""#gridList"").bootstrapTable('getSelections')[0].F_Id;;
        $.modalOpen({
            id: ""Details"",
            title: ""查看菜单"",
            url: """);
            EndContext();
            BeginContext(7062, 44, false);
#line 156 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\Module\Index.cshtml"
             Write(Url.Content("~/SystemManage/Module/Details"));

#line default
#line hidden
            EndContext();
            BeginContext(7106, 346, true);
            WriteLiteral(@"?keyValue="" + keyValue,
            width: ""700px"",
            height: ""490px"",
            btn: null,
        });
    }
    function btn_modulebutton() {
        var keyValue = $(""#gridList"").bootstrapTable('getSelections')[0].F_Id;;
        $.modalOpen({
            id: ""modulebutton"",
            title: ""系统按钮"",
            url: """);
            EndContext();
            BeginContext(7453, 48, false);
#line 167 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\Module\Index.cshtml"
             Write(Url.Content("~/SystemManage/ModuleButton/Index"));

#line default
#line hidden
            EndContext();
            BeginContext(7501, 2031, true);
            WriteLiteral(@"?moduleId="" + keyValue,
            width: ""950px"",
            height: ""600px"",
            btn: null,
        });
    }
</script>

<div class=""topPanel"">
    <div class=""toolbar"">
        <div class=""btn-group"">
            <a class=""btn btn-primary"" onclick=""$.reload()""><span class=""glyphicon glyphicon-refresh""></span></a>
        </div>
        <div class=""btn-group"">
            <a id=""NF-add"" authorize=""yes"" class=""btn btn-primary dropdown-text"" onclick=""btn_add()""><i class=""fa fa-plus""></i>新建菜单</a>
        </div>
        <div class=""operate"">
            <ul class=""nav nav-pills"">
                <li class=""first"">已选中<span>1</span>项</li>
                <li><a id=""NF-edit"" authorize=""yes"" onclick=""btn_edit()""><i class=""fa fa-pencil-square-o""></i>修改菜单</a></li>
                <li><a id=""NF-delete"" authorize=""yes"" onclick=""btn_delete()""><i class=""fa fa-trash-o""></i>删除菜单</a></li>
                <li><a id=""NF-Details"" authorize=""yes"" onclick=""btn_details()""><i class=""fa fa-search-plus");
            WriteLiteral(@"""></i>查看菜单</a></li>
                <li class=""split""></li>
                <li><a id=""NF-modulebutton"" authorize=""yes"" onclick=""btn_modulebutton()""><i class=""fa fa-gavel""></i>按钮管理</a></li>
            </ul>
            <a href=""javascript:;"" class=""close""></a>
        </div>
        <script>$('.toolbar').authorizeButton()</script>
    </div>
    <div class=""search"">
        <table>
            <tr>
                <td>
                    <div class=""input-group"">
                        <input id=""txt_keyword"" type=""text"" class=""form-control"" placeholder=""请输入要查询关键字"" style=""width: 200px;"">
                        <span class=""input-group-btn"">
                            <button id=""btn_search"" type=""button"" class=""btn  btn-primary""><i class=""fa fa-search""></i></button>
                        </span>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
<div class=""gridPanel"">
    <table id=""gridList""></table>
</div>
");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
