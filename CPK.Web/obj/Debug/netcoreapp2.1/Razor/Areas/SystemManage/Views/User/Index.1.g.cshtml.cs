#pragma checksum "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\User\Index.1.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "6e3a813ce32ae469e99643f0c019123d31dbeb76"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_SystemManage_Views_User_Index_1), @"mvc.1.0.view", @"/Areas/SystemManage/Views/User/Index.1.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Areas/SystemManage/Views/User/Index.1.cshtml", typeof(AspNetCore.Areas_SystemManage_Views_User_Index_1))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6e3a813ce32ae469e99643f0c019123d31dbeb76", @"/Areas/SystemManage/Views/User/Index.1.cshtml")]
    public class Areas_SystemManage_Views_User_Index_1 : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\User\Index.1.cshtml"
  
    ViewBag.Title = "Index";
    Layout = "~/Views/Shared/_Index.cshtml";

#line default
#line hidden
            BeginContext(83, 223, true);
            WriteLiteral("<script>\r\n    $(function () {\r\n        gridList();\r\n    })\r\n    function gridList() {\r\n        var $gridList = $(\"#gridList\");\r\n        $gridList.dataGrid({\r\n            height: $(window).height() - 128,\r\n            url: \"");
            EndContext();
            BeginContext(307, 33, false);
#line 13 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\User\Index.1.cshtml"
             Write(Url.Action("GetGridJson", "User"));

#line default
#line hidden
            EndContext();
            BeginContext(340, 4446, true);
            WriteLiteral(@""", //'/BaseData/User/GetGridJson',         //请求后台的URL（*）
            method: 'get',                      //请求方式（*）
            //toolbar: '#bstoolbar',                //工具按钮用哪个容器
            striped: true,                      //是否显示行间隔色
            cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
            pagination: true,                   //是否显示分页（*）
            sortable: true,                     //是否启用排序
            sortOrder: ""asc"",                   //排序方式
            //传递参数（*）
            queryParams: function (params) {
                return {
                    rows: params.limit,   //页面大小
                    page: params.offset / params.limit + 1, //页码
                    sord: params.order,
                    sidx: params.sort,
                    keyword: $(""#txt_keyword"").val()
                };
            },
            //showExport: true,                     //是否显示导出
            //exportDataType: ""basic"",              //basic', 'all', 'select");
            WriteLiteral(@"ed'.
            sidePagination: ""server"",           //分页方式：client客户端分页，server服务端分页（*）
            pageNumber: 1,                       //初始化加载第一页，默认第一页
            pageSize: 10,                       //每页的记录行数（*）
            pageList: [10, 25, 50, 100, 'ALL'],        //可供选择的每页的行数（*）
            //search: true,                       //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
            //strictSearch: true,
            //showColumns: false,                  //是否显示所有的列
            //showRefresh: true,                  //是否显示刷新按钮
            minimumCountColumns: 2,             //最少允许的列数
            clickToSelect: true,                //是否启用点击选中行
            height: $(window).height() - 96,                        //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
            uniqueId: ""F_Id"",                     //每一行的唯一标识，一般为主键列
            //showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
            //cardView: false,                    //是否显示详细视图
            //detailView: false,                   //");
            WriteLiteral(@"是否显示父子表
            sortName: ""F_DepartmentId asc,F_CreatorTime desc"",
            columns: [
            { radio: true },
            { title: '主键', field: 'F_Id', width: 80, align: 'left', visible: false },
            { title: 'EID', field: 'F_Account', width: 80, align: 'left' },
            //{ title: '工号', field: 'F_EMP_NO', width: 80, align: 'left' },
            { title: '姓名', field: 'F_RealName', width: 80, align: 'left' },
            { title: '英文名', field: 'F_NickName', width: 80, align: 'left' },
            //{ title: '性别', field: 'F_Gender', width: 80, align: 'left',
            //    formatter: function (value, row, index) {
            //        if (value == true) {
            //            return '男';
            //        } else {
            //            return '女';
            //        }
            //    }
            //},
            { title: '联系方式', field: 'F_MobilePhone', width: 80, align: 'left' },
            {
                title: '部门', field: 'F_DepartmentI");
            WriteLiteral(@"d', width: 80, align: 'left',
                formatter: function (value, row, index) {
                    return top.clients.organize[value] == null ? """" : top.clients.organize[value].fullname;
                }
            },
            { title: '职位', field: 'F_DutyId', width: 80, align: 'left',
                formatter: function (value, row, index) {
                    return top.clients.duty[value] == null ? """" : top.clients.duty[value].fullname;
                }
            },
            { title: '创建时间', field: 'F_CreatorTime', width: 80, align: 'left' },
            {
                title: '是否在职', field: 'F_EnabledMark', width: 60, align: 'center',
                formatter: function (value, row, index) {
                    if (value == 1)
                    { return '<span class=\""label label-success""\"">启用</span>'; }
                    else
                    { return '<span class=\""label label-default""\"">禁用</span>'; }
                }
            },
            { title:");
            WriteLiteral(@" '备注', field: 'F_Description', width: 80, align: 'left' },
            ],
        });
        $(""#btn_search"").click(function () {
            $.currentWindow().$(""#gridList"").bootstrapTable('refresh');
        });
    }

    function btn_add() {
        $.modalOpen({
            id: ""Form"",
            title: ""新增用户"",
            url: """);
            EndContext();
            BeginContext(4787, 39, false);
#line 99 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\User\Index.1.cshtml"
             Write(Url.Content("~/SystemManage/User/Form"));

#line default
#line hidden
            EndContext();
            BeginContext(4826, 425, true);
            WriteLiteral(@""",
            width: ""700px"",
            height: ""510px"",
            callBack: function (iframeId) {
                top.frames[iframeId].submitForm();
            }
        });
    }
    function btn_edit() {
        var keyValue = $.parseJSON(JSON.stringify($(""#gridList"").bootstrapTable('getSelections')))[0].F_Id;
        $.modalOpen({
            id: ""Form"",
            title: ""修改用户"",
            url: """);
            EndContext();
            BeginContext(5252, 39, false);
#line 112 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\User\Index.1.cshtml"
             Write(Url.Content("~/SystemManage/User/Form"));

#line default
#line hidden
            EndContext();
            BeginContext(5291, 287, true);
            WriteLiteral(@"?keyValue="" + keyValue,
            width: ""700px"",
            height: ""510px"",
            callBack: function (iframeId) {
                top.frames[iframeId].submitForm();
            }
        });
    }
    function btn_delete() {
        $.deleteForm({
            url: """);
            EndContext();
            BeginContext(5579, 45, false);
#line 122 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\User\Index.1.cshtml"
             Write(Url.Content("~/SystemManage/User/DeleteForm"));

#line default
#line hidden
            EndContext();
            BeginContext(5624, 506, true);
            WriteLiteral(@""",
            param: { keyValue: $.parseJSON(JSON.stringify($(""#gridList"").bootstrapTable('getSelections')))[0].F_Id },
            success: function () {
                $.currentWindow().$(""#gridList"").bootstrapTable('refresh');
            }
        })
    }
    function btn_details() {
        var keyValue = $.parseJSON(JSON.stringify($(""#gridList"").bootstrapTable('getSelections')))[0].F_Id;
        $.modalOpen({
            id: ""Details"",
            title: ""查看用户"",
            url: """);
            EndContext();
            BeginContext(6131, 42, false);
#line 134 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\User\Index.1.cshtml"
             Write(Url.Content("~/SystemManage/User/Details"));

#line default
#line hidden
            EndContext();
            BeginContext(6173, 606, true);
            WriteLiteral(@"?keyValue="" + keyValue,
            width: ""700px"",
            height: ""550px"",
            btn: null,
        });
    }
    function btn_revisepassword() {
        var keyValue = $.parseJSON(JSON.stringify($(""#gridList"").bootstrapTable('getSelections')))[0].F_Id;
        var Account = $.parseJSON(JSON.stringify($(""#gridList"").bootstrapTable('getSelections')))[0].F_Account;
        var RealName = $.parseJSON(JSON.stringify($(""#gridList"").bootstrapTable('getSelections')))[0].F_RealName;
        $.modalOpen({
            id: ""RevisePassword"",
            title: '重置密码',
            url: '");
            EndContext();
            BeginContext(6780, 49, false);
#line 147 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\User\Index.1.cshtml"
             Write(Url.Content("~/SystemManage/User/RevisePassword"));

#line default
#line hidden
            EndContext();
            BeginContext(6829, 561, true);
            WriteLiteral(@"?keyValue=' + keyValue + ""&account="" + escape(Account) + '&realName=' + escape(RealName),
            width: ""450px"",
            height: ""260px"",
            callBack: function (iframeId) {
                top.frames[iframeId].submitForm();
            }
        });
    }
    function btn_disabled() {
        var keyValue = $.parseJSON(JSON.stringify($(""#gridList"").bootstrapTable('getSelections')))[0].F_Id;
        $.modalConfirm(""注：您确定要【禁用】该项账户吗？"", function (r) {
            if (r) {
                $.submitForm({
                    url: """);
            EndContext();
            BeginContext(7391, 50, false);
#line 160 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\User\Index.1.cshtml"
                     Write(Url.Content("~/SystemManage/User/DisabledAccount"));

#line default
#line hidden
            EndContext();
            BeginContext(7441, 541, true);
            WriteLiteral(@""",
                    param: { keyValue: keyValue },
                    success: function () {
                        $.currentWindow().$(""#gridList"").bootstrapTable('refresh');
                    }
                })
            }
        });
    }
    function btn_enabled() {
        var keyValue = $.parseJSON(JSON.stringify($(""#gridList"").bootstrapTable('getSelections')))[0].F_Id;
        $.modalConfirm(""注：您确定要【启用】该项账户吗？"", function (r) {
            if (r) {
                $.submitForm({
                    url: """);
            EndContext();
            BeginContext(7983, 49, false);
#line 174 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemManage\Views\User\Index.1.cshtml"
                     Write(Url.Content("~/SystemManage/User/EnabledAccount"));

#line default
#line hidden
            EndContext();
            BeginContext(8032, 2492, true);
            WriteLiteral(@""",
                    param: { keyValue: keyValue },
                    success: function () {
                        $.currentWindow().$(""#gridList"").bootstrapTable('refresh');
                    }
                })
            }
        });
    }
</script>

<div class=""topPanel"">
    <div class=""toolbar"">
        <div class=""btn-group"">
            <a class=""btn btn-primary"" onclick=""$.reload()""><span class=""glyphicon glyphicon-refresh""></span></a>
        </div>
        <div class=""btn-group"">
            <a id=""NF-add"" authorize=""yes"" class=""btn btn-primary dropdown-text"" onclick=""btn_add()""><i class=""fa fa-plus""></i>新建用户</a>
        </div>
        <div class=""operate"">
            <ul class=""nav nav-pills"">
                <li class=""first"">已选中<span>1</span>项</li>
                <li><a id=""NF-edit"" authorize=""yes"" onclick=""btn_edit()""><i class=""fa fa-pencil-square-o""></i>修改用户</a></li>
                <li><a id=""NF-delete"" authorize=""yes"" onclick=""btn_delete()""><i class=""fa fa");
            WriteLiteral(@"-trash-o""></i>删除用户</a></li>
                <li><a id=""NF-Details"" authorize=""yes"" onclick=""btn_details()""><i class=""fa fa-search-plus""></i>查看用户</a></li>
                <li class=""split""></li>
                <li><a id=""NF-revisepassword"" authorize=""yes"" onclick=""btn_revisepassword()""><i class=""fa fa-key""></i>密码重置</a></li>
                <li class=""split""></li>
                <li><a id=""NF-disabled"" authorize=""yes"" onclick=""btn_disabled()""><i class=""fa fa-stop-circle""></i>禁用</a></li>
                <li><a id=""NF-enabled"" authorize=""yes"" onclick=""btn_enabled()""><i class=""fa fa-play-circle""></i>启用</a></li>
            </ul>
            <a href=""javascript:;"" class=""close""></a>
        </div>
        <script>$('.toolbar').authorizeButton()</script>
    </div>
    <div class=""search"">
        <table>
            <tr>
                <td>
                    <div class=""input-group"">
                        <input id=""txt_keyword"" type=""text"" class=""form-control"" placeholder=""账户/姓名/手机"" style=""");
            WriteLiteral(@"width: 200px;"">
                        <span class=""input-group-btn"">
                            <button id=""btn_search"" type=""button"" class=""btn  btn-primary""><i class=""fa fa-search""></i></button>
                        </span>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
<div class=""gridPanel"">
    <table id=""gridList""></table>
    <div id=""gridPager""></div>
</div>
");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
