#pragma checksum "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\ProductManage\Views\DataSource_File\Form.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "fbb55996a15cec0487f4e8b9355d0a8ec5fddb78"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_ProductManage_Views_DataSource_File_Form), @"mvc.1.0.view", @"/Areas/ProductManage/Views/DataSource_File/Form.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Areas/ProductManage/Views/DataSource_File/Form.cshtml", typeof(AspNetCore.Areas_ProductManage_Views_DataSource_File_Form))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"fbb55996a15cec0487f4e8b9355d0a8ec5fddb78", @"/Areas/ProductManage/Views/DataSource_File/Form.cshtml")]
    public class Areas_ProductManage_Views_DataSource_File_Form : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\ProductManage\Views\DataSource_File\Form.cshtml"
  
    ViewBag.Title = "Form";
    Layout = "~/Views/Shared/_Form.cshtml";

#line default
#line hidden
            BeginContext(81, 169, true);
            WriteLiteral("<script>\r\n    var keyValue = $.request(\"keyValue\");\r\n    $(function () {\r\n        initControl();\r\n        if (!!keyValue) {\r\n            $.ajax({\r\n                url: \"");
            EndContext();
            BeginContext(251, 44, false);
#line 11 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\ProductManage\Views\DataSource_File\Form.cshtml"
                 Write(Url.Action("GetFormJson", "DataSource_File"));

#line default
#line hidden
            EndContext();
            BeginContext(295, 496, true);
            WriteLiteral(@""",//,
                data: { keyValue: keyValue },
                dataType: ""json"",
                async: false,
                cache: false,
                success: function (data) {
                    $(""#form1"").formSerialize(data);
                }
            });
        }
    });

    function initControl() {

    }
    function submitForm() {
        if (!$('#form1').formValid()) {
            return false;
        }
        $.submitForm({
            url: """);
            EndContext();
            BeginContext(792, 57, false);
#line 31 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\ProductManage\Views\DataSource_File\Form.cshtml"
             Write(Url.Content("~/ProductManage/DataSource_File/SubmitForm"));

#line default
#line hidden
            EndContext();
            BeginContext(849, 2823, true);
            WriteLiteral(@"?keyValue="" + keyValue,
            param: $(""#form1"").formSerialize(),
            success: function () {
                //$.currentWindow().$(""#gridList"").resetSelection();
                //$.currentWindow().$(""#gridList"").trigger(""reloadGrid"");
                $.currentWindow().$(""#gridList"").bootstrapTable('refresh');
                $.loading(false);
            }
        })
    }
</script>

<form id=""form1"">
    <div style=""padding-top: 20px; margin-right: 20px;"">
        <table class=""form"">
            <tr>
<th class=""formTitle"" valign=""top"" style=""padding-top: 5px; "">类型(csv,excel)</th > 
<td class=""formValue""><input id = ""F_Type"" name = ""F_Type"" maxlength = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /></td>
</tr>
<tr>
<th class=""formTitle"" valign=""top"" style=""padding-top: 5px; "">文件路径</th > 
<td class=""formValue""><input id = ""F_Path"" name = ""F_Path"" maxlength = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /></td>
</tr>
<tr>
<th cl");
            WriteLiteral(@"ass=""formTitle"" valign=""top"" style=""padding-top: 5px; "">Sheet名称</th > 
<td class=""formValue""><input id = ""F_SheetName"" name = ""F_SheetName"" maxlength = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /></td>
</tr>
<tr>
<th class=""formTitle"" valign=""top"" style=""padding-top: 5px; "">关键字段名称</th > 
<td class=""formValue""><input id = ""F_KeyField"" name = ""F_KeyField"" maxlength = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /></td>
</tr>
<tr>
<th class=""formTitle"" valign=""top"" style=""padding-top: 5px; "">排序字段</th > 
<td class=""formValue""><input id = ""F_SortField"" name = ""F_SortField"" maxlength = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /></td>
</tr>
<tr>
<th class=""formTitle"" valign=""top"" style=""padding-top: 5px; "">样本值字段</th > 
<td class=""formValue""><input id = ""F_ValueField"" name = ""F_ValueField"" maxlength = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /></td>
</tr>
<tr>
<th class=""formTitle"" valign=""top"" style=""pa");
            WriteLiteral(@"dding-top: 5px; "">时间字段</th > 
<td class=""formValue""><input id = ""F_TimeField"" name = ""F_TimeField"" maxlength = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /></td>
</tr>
<tr>
<th class=""formTitle"" valign=""top"" style=""padding-top: 5px; "">标签字段，多个用逗号隔开</th > 
<td class=""formValue""><input id = ""F_TagFields"" name = ""F_TagFields"" maxlength = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /></td>
</tr>
<tr>
<th class=""formTitle"" valign=""top"" style=""padding-top: 5px; "">有效标志</th > 
<td class=""formValue""><input id = ""F_EnabledMark"" name = ""F_EnabledMark"" maxlength = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /><label for=""F_EnabledMark"">有效</label></td>
</tr>

        </table>
    </div>
</form>
");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
