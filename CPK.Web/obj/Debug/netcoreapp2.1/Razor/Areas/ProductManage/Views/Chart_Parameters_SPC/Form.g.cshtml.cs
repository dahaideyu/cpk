#pragma checksum "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\ProductManage\Views\Chart_Parameters_SPC\Form.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7566ff68c622b895c93aedbddd6b944e5abe728f"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_ProductManage_Views_Chart_Parameters_SPC_Form), @"mvc.1.0.view", @"/Areas/ProductManage/Views/Chart_Parameters_SPC/Form.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Areas/ProductManage/Views/Chart_Parameters_SPC/Form.cshtml", typeof(AspNetCore.Areas_ProductManage_Views_Chart_Parameters_SPC_Form))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7566ff68c622b895c93aedbddd6b944e5abe728f", @"/Areas/ProductManage/Views/Chart_Parameters_SPC/Form.cshtml")]
    public class Areas_ProductManage_Views_Chart_Parameters_SPC_Form : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\ProductManage\Views\Chart_Parameters_SPC\Form.cshtml"
  
    ViewBag.Title = "Form";
    Layout = "~/Views/Shared/_Form.cshtml";

#line default
#line hidden
            BeginContext(81, 169, true);
            WriteLiteral("<script>\r\n    var keyValue = $.request(\"keyValue\");\r\n    $(function () {\r\n        initControl();\r\n        if (!!keyValue) {\r\n            $.ajax({\r\n                url: \"");
            EndContext();
            BeginContext(251, 49, false);
#line 11 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\ProductManage\Views\Chart_Parameters_SPC\Form.cshtml"
                 Write(Url.Action("GetFormJson", "Chart_Parameters_SPC"));

#line default
#line hidden
            EndContext();
            BeginContext(300, 496, true);
            WriteLiteral(@""",//,
                data: { keyValue: keyValue },
                dataType: ""json"",
                async: false,
                cache: false,
                success: function (data) {
                    $(""#form1"").formSerialize(data);
                }
            });
        }
    });

    function initControl() {

    }
    function submitForm() {
        if (!$('#form1').formValid()) {
            return false;
        }
        $.submitForm({
            url: """);
            EndContext();
            BeginContext(797, 62, false);
#line 31 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\ProductManage\Views\Chart_Parameters_SPC\Form.cshtml"
             Write(Url.Content("~/ProductManage/Chart_Parameters_SPC/SubmitForm"));

#line default
#line hidden
            EndContext();
            BeginContext(859, 4746, true);
            WriteLiteral(@"?keyValue="" + keyValue,
            param: $(""#form1"").formSerialize(),
            success: function () {
                //$.currentWindow().$(""#gridList"").resetSelection();
                //$.currentWindow().$(""#gridList"").trigger(""reloadGrid"");
                $.currentWindow().$(""#gridList"").bootstrapTable('refresh');
                $.loading(false);
            }
        })
    }
</script>

<form id=""form1"">
    <div style=""padding-top: 20px; margin-right: 20px;"">
        <table class=""form"">
            <tr>
<th class=""formTitle"" valign=""top"" style=""padding-top: 5px; "">编码</th > 
<td class=""formValue""><input id = ""F_ChartId"" name = ""F_ChartId"" maxlength = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /></td>
</tr>
<tr>
<th class=""formTitle"" valign=""top"" style=""padding-top: 5px; "">LCL</th > 
<td class=""formValue""><input id = ""F_LCL"" name = ""F_LCL"" maxlength = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /></td>
</tr>
<tr>
<th class=""for");
            WriteLiteral(@"mTitle"" valign=""top"" style=""padding-top: 5px; "">CL</th > 
<td class=""formValue""><input id = ""F_CL"" name = ""F_CL"" maxlength = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /></td>
</tr>
<tr>
<th class=""formTitle"" valign=""top"" style=""padding-top: 5px; "">UCL</th > 
<td class=""formValue""><input id = ""F_UCL"" name = ""F_UCL"" maxlength = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /></td>
</tr>
<tr>
<th class=""formTitle"" valign=""top"" style=""padding-top: 5px; "">LSL</th > 
<td class=""formValue""><input id = ""F_LSL"" name = ""F_LSL"" maxlength = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /></td>
</tr>
<tr>
<th class=""formTitle"" valign=""top"" style=""padding-top: 5px; "">USL</th > 
<td class=""formValue""><input id = ""F_USL"" name = ""F_USL"" maxlength = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /></td>
</tr>
<tr>
<th class=""formTitle"" valign=""top"" style=""padding-top: 5px; "">1个点落在A区以外</th > 
<td class=""formValue""><input id =");
            WriteLiteral(@" ""F_Rule1"" name = ""F_Rule1"" maxlength = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /></td>
</tr>
<tr>
<th class=""formTitle"" valign=""top"" style=""padding-top: 5px; "">连续9个点落在中心线的同一侧</th > 
<td class=""formValue""><input id = ""F_Rule2"" name = ""F_Rule2"" maxlength = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /></td>
</tr>
<tr>
<th class=""formTitle"" valign=""top"" style=""padding-top: 5px; "">连续6个点递增或递减</th > 
<td class=""formValue""><input id = ""F_Rule3"" name = ""F_Rule3"" maxlength = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /></td>
</tr>
<tr>
<th class=""formTitle"" valign=""top"" style=""padding-top: 5px; "">连续14个点中相邻点交替上下</th > 
<td class=""formValue""><input id = ""F_Rule4"" name = ""F_Rule4"" maxlength = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /></td>
</tr>
<tr>
<th class=""formTitle"" valign=""top"" style=""padding-top: 5px; "">连续3个点中有2个点落在中心线同一侧的B区以外</th > 
<td class=""formValue""><input id = ""F_Rule5"" name = ""F_Rule5"" max");
            WriteLiteral(@"length = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /></td>
</tr>
<tr>
<th class=""formTitle"" valign=""top"" style=""padding-top: 5px; "">连续5个点中有4个点落在中心线同一侧的C区以外</th > 
<td class=""formValue""><input id = ""F_Rule6"" name = ""F_Rule6"" maxlength = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /></td>
</tr>
<tr>
<th class=""formTitle"" valign=""top"" style=""padding-top: 5px; "">连续15个点落在中心线两侧的C区以内</th > 
<td class=""formValue""><input id = ""F_Rule7"" name = ""F_Rule7"" maxlength = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /></td>
</tr>
<tr>
<th class=""formTitle"" valign=""top"" style=""padding-top: 5px; "">连续8个点落在中心线两侧且无一在C区内</th > 
<td class=""formValue""><input id = ""F_Rule8"" name = ""F_Rule8"" maxlength = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /></td>
</tr>
<tr>
<th class=""formTitle"" valign=""top"" style=""padding-top: 5px; "">豁免周期(天)</th > 
<td class=""formValue""><input id = ""F_ExemptionDay"" name = ""F_ExemptionDay"" maxlength = ""2");
            WriteLiteral(@"0"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /></td>
</tr>
<tr>
<th class=""formTitle"" valign=""top"" style=""padding-top: 5px; "">排序码</th > 
<td class=""formValue""><input id = ""F_SortCode"" name = ""F_SortCode"" maxlength = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /></td>
</tr>
<tr>
<th class=""formTitle"" valign=""top"" style=""padding-top: 5px; "">有效标志</th > 
<td class=""formValue""><input id = ""F_EnabledMark"" name = ""F_EnabledMark"" maxlength = ""20"" type = ""text"" class=""form-control required"" placeholder=""请输入"" /><label for=""F_EnabledMark"">有效</label></td>
</tr>

        </table>
    </div>
</form>
");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
