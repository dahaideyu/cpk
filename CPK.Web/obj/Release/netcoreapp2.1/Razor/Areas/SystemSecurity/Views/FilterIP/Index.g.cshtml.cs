#pragma checksum "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemSecurity\Views\FilterIP\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "e7a8f22d1cce0d3658bce4079f569bd0a6e193c0"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_SystemSecurity_Views_FilterIP_Index), @"mvc.1.0.view", @"/Areas/SystemSecurity/Views/FilterIP/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Areas/SystemSecurity/Views/FilterIP/Index.cshtml", typeof(AspNetCore.Areas_SystemSecurity_Views_FilterIP_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"e7a8f22d1cce0d3658bce4079f569bd0a6e193c0", @"/Areas/SystemSecurity/Views/FilterIP/Index.cshtml")]
    public class Areas_SystemSecurity_Views_FilterIP_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "C:\Lucus\Projects\net\work\F001591\CPK\CPK.Web\Areas\SystemSecurity\Views\FilterIP\Index.cshtml"
  
    ViewBag.Title = "Index";
    Layout = "~/Views/Shared/_Index.cshtml";

#line default
#line hidden
            BeginContext(83, 4538, true);
            WriteLiteral(@"<script>
    $(function () {
        gridList();
    })
    function gridList() {
        var $gridList = $(""#gridList"");
        $gridList.dataGrid({
            url: ""/SystemSecurity/FilterIP/GetGridJson"",
            height: $(window).height() - 96,
            colModel: [
                { label: ""主键"", name: ""F_Id"", hidden: true, key: true },
                {
                    label: '策略类型', name: 'F_Type', width: 80, align: 'left',
                    formatter: function (cellvalue, options, rowObject) {
                        if (cellvalue == true) {
                            return '<span class=\""label label-success\"">允许访问</span>';
                        } else if (cellvalue == false) {
                            return '<span class=\""label label-default\"">拒绝访问</span>';
                        }
                    }
                },
                { label: '起始IP', name: 'F_StartIP', width: 200, align: 'left' },
                { label: '结束IP', name: 'F_EndIP', width: 2");
            WriteLiteral(@"00, align: 'left' },
                { label: '创建人员', name: 'F_CreatorUserId', width: 100, align: 'left' },
                {
                    label: '创建时间', name: 'F_CreatorTime', width: 100, align: 'left',
                    formatter: ""date"", formatoptions: { srcformat: 'Y-m-d H:i', newformat: 'Y-m-d H:i' }
                },
                { label: '备注', name: 'F_Description', width: 300, align: 'left' }
            ]
        });
        $(""#btn_search"").click(function () {
            $gridList.jqGrid('setGridParam', {
                postData: { keyword: $(""#txt_keyword"").val() },
            }).trigger('reloadGrid');
        });
    }
    function btn_add() {
        $.modalOpen({
            id: ""Form"",
            title: ""新建策略"",
            url: ""/SystemSecurity/FilterIP/Form"",
            width: ""450px"",
            height: ""400px"",
            callBack: function (iframeId) {
                top.frames[iframeId].submitForm();
            }
        });
    }
    functi");
            WriteLiteral(@"on btn_delete() {
        $.deleteForm({
            url: ""/SystemSecurity/FilterIP/DeleteForm"",
            param: { keyValue: $.parseJSON(JSON.stringify($(""#gridList"").bootstrapTable('getSelections')))[0].F_Id; },
            success: function () {
                $.currentWindow().$(""#gridList"").trigger(""reloadGrid"");
            }
        })
    }
    function btn_details() {
        var keyValue = $.parseJSON(JSON.stringify($(""#gridList"").bootstrapTable('getSelections')))[0].F_Id;
        $.modalOpen({
            id: ""Details"",
            title: ""查看策略"",
            url: ""/SystemSecurity/FilterIP/Details?keyValue="" + keyValue,
            width: ""450px"",
            height: ""520px"",
            btn: null,
        });
    }
</script>

<div class=""topPanel"">
    <div class=""toolbar"">
        <div class=""btn-group"">
            <a class=""btn btn-primary"" onclick=""$.reload()""><span class=""glyphicon glyphicon-refresh""></span></a>
        </div>
        <div class=""btn-group"">
    ");
            WriteLiteral(@"        <a id=""NF-add"" authorize=""yes"" class=""btn btn-primary dropdown-text"" onclick=""btn_add()""><i class=""fa fa-plus""></i>新建策略</a>
        </div>
        <div class=""operate"">
            <ul class=""nav nav-pills"">
                <li class=""first"">已选中<span>1</span>项</li>
                <li><a id=""NF-edit"" authorize=""yes"" onclick=""btn_edit()""><i class=""fa fa-pencil-square-o""></i>修改策略</a></li>
                <li><a id=""NF-delete"" authorize=""yes"" onclick=""btn_delete()""><i class=""fa fa-trash-o""></i>删除策略</a></li>
                <li><a id=""NF-Details"" authorize=""yes"" onclick=""btn_details()""><i class=""fa fa-search-plus""></i>查看策略</a></li>
            </ul>
            <a href=""javascript:;"" class=""close""></a>
        </div>
        <script>$('.toolbar').authorizeButton()</script>
    </div>
    <div class=""search"">
        <table>
            <tr>
                <td>
                    <div class=""input-group"">
                        <input id=""txt_keyword"" type=""text"" class=""form-control"" p");
            WriteLiteral(@"laceholder=""请输入要查询关键字"" style=""width: 200px;"">
                        <span class=""input-group-btn"">
                            <button id=""btn_search"" type=""button"" class=""btn  btn-primary""><i class=""fa fa-search""></i></button>
                        </span>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
<div class=""gridPanel"">
    <table id=""gridList""></table>
</div>
");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
