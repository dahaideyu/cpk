﻿$(function () {
    document.body.className = localStorage.getItem('config-skin');
    $("[data-toggle='tooltip']").tooltip();
})
$.reload = function () {
    location.reload();
    return false;
}
$.loading = function (bool, text) {
    var $loadingpage = top.$("#loadingPage");
    var $loadingtext = $loadingpage.find('.loading-content');
    if (bool) {
        $loadingpage.show();
    } else {
        if ($loadingtext.attr('istableloading') == undefined) {
            $loadingpage.hide();
        }
    }
    if (!!text) {
        $loadingtext.html(text);
    } else {
        $loadingtext.html("数据加载中，请稍后…");
    }
    $loadingtext.css("left", (top.$('body').width() - $loadingtext.width()) / 2 - 50);
    $loadingtext.css("top", (top.$('body').height() - $loadingtext.height()) / 2);
}
$.request = function (name) {
    var search = location.search.slice(1);
    var arr = search.split("&");
    for (var i = 0; i < arr.length; i++) {
        var ar = arr[i].split("=");
        if (ar[0] == name) {
            if (unescape(ar[1]) == 'undefined') {
                return "";
            } else {
                return unescape(ar[1]);
            }
        }
    }
    return "";
}
$.currentWindow = function () {
    var iframeId = top.$(".CPK_iframe:visible").attr("id");
    return top.frames[iframeId];
}
$.browser = function () {
    var userAgent = navigator.userAgent;
    var isOpera = userAgent.indexOf("Opera") > -1;
    if (isOpera) {
        return "Opera"
    };
    if (userAgent.indexOf("Firefox") > -1) {
        return "FF";
    }
    if (userAgent.indexOf("Chrome") > -1) {
        if (window.navigator.webkitPersistentStorage.toString().indexOf('DeprecatedStorageQuota') > -1) {
            return "Chrome";
        } else {
            return "360";
        }
    }
    if (userAgent.indexOf("Safari") > -1) {
        return "Safari";
    }
    if (userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera) {
        return "IE";
    };
}
$.download = function (url, data, method) {
    if (url && data) {
        data = typeof data == 'string' ? data : jQuery.param(data);
        var inputs = '';
        $.each(data.split('&'), function () {
            var pair = this.split('=');
            inputs += '<input type="hidden" name="' + pair[0] + '" value="' + pair[1] + '" />';
        });
        $('<form action="' + url + '" method="' + (method || 'post') + '">' + inputs + '</form>').appendTo('body').submit().remove();
    };
};
$.modalOpen = function (options) {
    var defaults = {
        id: null,
        title: '系统窗口',
        width: "100px",
        height: "100px",
        url: '',
        shade: 0.3,
        btn: ['确认', '关闭'],
        btnclass: ['btn btn-primary', 'btn btn-danger'],
        callBack: null
    };
    var options = $.extend(defaults, options);
    var _width = top.$(window).width() > parseInt(options.width.replace('px', '')) ? options.width : top.$(window).width() + 'px';
    var _height = top.$(window).height() > parseInt(options.height.replace('px', '')) ? options.height : top.$(window).height() + 'px';
    top.layer.open({
        id: options.id,
        type: 2,
        shade: options.shade,
        title: options.title,
        fix: false,
        area: [_width, _height],
        content: options.url,
        btn: options.btn,
        btnclass: options.btnclass,
        yes: function () {
            options.callBack(options.id)
        }, cancel: function () {
            return true;
        }
    });
}
$.modalConfirm = function (content, callBack) {
    top.layer.confirm(content, {
        icon: "fa-exclamation-circle",
        title: "系统提示",
        btn: ['确认', '取消'],
        btnclass: ['btn btn-primary', 'btn btn-danger'],
    }, function () {
        callBack(true);
    }, function () {
        callBack(false)
    });
}
$.modalAlert = function (content, type) {
    var icon = "";
    if (type == 'success') {
        icon = "fa-check-circle";
    }
    if (type == 'error') {
        icon = "fa-times-circle";
    }
    if (type == 'warning') {
        icon = "fa-exclamation-circle";
    }
    top.layer.alert(content, {
        icon: icon,
        title: "系统提示",
        btn: ['确认'],
        btnclass: ['btn btn-primary'],
    });
}
$.modalMsg = function (content, type) {
    if (type != undefined) {
        var icon = "";
        if (type == 'success') {
            icon = "fa-check-circle";
        }
        if (type == 'error') {
            icon = "fa-times-circle";
        }
        if (type == 'warning') {
            icon = "fa-exclamation-circle";
        }
        top.layer.msg(content, { icon: icon, time: 4000, shift: 5 });
        top.$(".layui-layer-msg").find('i.' + icon).parents('.layui-layer-msg').addClass('layui-layer-msg-' + type);
    } else {
        top.layer.msg(content);
    }
}
$.modalClose = function () {
    var index = top.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
    var $IsdialogClose = top.$("#layui-layer" + index).find('.layui-layer-btn').find("#IsdialogClose");
    var IsClose = $IsdialogClose.is(":checked");
    if ($IsdialogClose.length == 0) {
        IsClose = true;
    }
    if (IsClose) {
        top.layer.close(index);
    } else {
        location.reload();
    }
}
$.submitForm = function (options) {
    var defaults = {
        url: "",
        param: [],
        loading: "正在提交数据...",
        success: null,
        cache: false,
        close: true
    };
    var options = $.extend(defaults, options);
    $.loading(true, options.loading);
    window.setTimeout(function () {
        if ($('[name=__RequestVerificationToken]').length > 0) {
            options.param["__RequestVerificationToken"] = $('[name=__RequestVerificationToken]').val();
        }
        $.ajax({
            url: options.url,
            data: options.param,
            type: "post",
            dataType: "json",
            success: function (data) {
                if (data.state == "success") {
                    options.success(data);
                    $.modalMsg(data.message, data.state);
                    if (options.close == true) {
                        $.modalClose();
                    }
                } else {
                    $.modalAlert(data.message, data.state);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $.loading(false);
                $.modalMsg(errorThrown, "error");
            },
            beforeSend: function () {
                $.loading(true, options.loading);
            },
            complete: function () {
                $.loading(false);
            }
        });
    }, 10);
}
$.deleteForm = function (options) {
    var defaults = {
        prompt: "注：您确定要删除该项数据吗？",
        url: "",
        param: [],
        loading: "正在删除数据...",
        success: null,
        close: true
    };
    var options = $.extend(defaults, options);
    if ($('[name=__RequestVerificationToken]').length > 0) {
        options.param["__RequestVerificationToken"] = $('[name=__RequestVerificationToken]').val();
    }
    $.modalConfirm(options.prompt, function (r) {
        if (r) {
            $.loading(true, options.loading);
            window.setTimeout(function () {
                $.ajax({
                    url: options.url,
                    data: options.param,
                    type: "post",
                    dataType: "json",
                    success: function (data) {
                        if (data.state == "success") {
                            options.success(data);
                            $.modalMsg(data.message, data.state);
                        } else {
                            $.modalAlert(data.message, data.state);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $.loading(false);
                        $.modalMsg(errorThrown, "error");
                    },
                    beforeSend: function () {
                        $.loading(true, options.loading);
                    },
                    complete: function () {
                        $.loading(false);
                    }
                });
            }, 10);
        }
    });

}
$.jsonWhere = function (data, action) {
    if (action == null) return;
    var reval = new Array();
    $(data).each(function (i, v) {
        if (action(v)) {
            reval.push(v);
        }
    })
    return reval;
}
//$.fn.jqGridRowValue = function () {
//    var $grid = $(this);
//    var selectedRowIds = $grid.jqGrid("getGridParam", "selarrrow");
//    if (selectedRowIds != "") {
//        var json = [];
//        var len = selectedRowIds.length;
//        for (var i = 0; i < len ; i++) {
//            var rowData = $grid.jqGrid('getRowData', selectedRowIds[i]);
//            json.push(rowData);
//        }
//        return json;
//    } else {
//        return $grid.jqGrid('getRowData', $grid.jqGrid('getGridParam', 'selrow'));
//    }
//}

$.fn.formValid = function () {
    return $(this).valid({
        errorPlacement: function (error, element) {
            element.parents('.formValue').addClass('has-error');
            element.parents('.has-error').find('i.error').remove();
            element.parents('.has-error').append('<i class="form-control-feedback fa fa-exclamation-circle error" data-placement="left" data-toggle="tooltip" title="' + error + '"></i>');
            $("[data-toggle='tooltip']").tooltip();
            if (element.parents('.input-group').hasClass('input-group')) {
                element.parents('.has-error').find('i.error').css('right', '33px')
            }
        },
        success: function (element) {
            element.parents('.has-error').find('i.error').remove();
            element.parent().removeClass('has-error');
        }
    });
}
$.fn.formSerialize = function (formdate) {
    var element = $(this);
    if (!!formdate) {
        for (var key in formdate) {
            var elementid = element.find('#' + key);
            if(!!elementid[0])
            {
                var value = $.trim(formdate[key]).replace(/&nbsp;/g, '');
                var type = elementid[0].tagName.toLowerCase();//$id.attr('type');
                var $id = elementid;
                switch (type) {
                    case "input":
                        switch($id.attr('type'))
                        {
                            case "checkbox":
                                if (value == "true") {
                                    $id.attr("checked", 'checked');
                                } else {
                                    $id.removeAttr("checked");
                                }
                                break;
                            default:
                                $id.val(value);
                                break;
                        }
                        break;
                    case "select":
                        $id.val(value).trigger("change");
                        break;
                    default:
                        $id.val(value);
                        break;
                }
            }
        };
        return false;
    }
    var postdata = {};
    element.find('input,select,textarea').each(function (r) {
        var $this = $(this);
        var id = $this.attr('id');
        var type = $this.attr('type');
        switch (type) {
            case "checkbox":
                postdata[id] = $this.is(":checked");
                break;
            default:
                var value = $this.val() == "" ? "&nbsp;" : $this.val();
                if (!$.request("keyValue")) {
                    if (value instanceof Array) {
                        for (x in value) {
                            value[x] = value[x].replace(/&nbsp;/g, '');
                        }
                    } else {
                        value = value.replace(/&nbsp;/g, '');
                    }
                }
                postdata[id] = value;
                break;
        }
    });
    if ($('[name=__RequestVerificationToken]').length > 0) {
        postdata["__RequestVerificationToken"] = $('[name=__RequestVerificationToken]').val();
    }
    return postdata;
};
$.fn.bindSelect = function (options) {
    var defaults = {
        id: "id",
        text: "text",
        search: false,
        url: "",
        param: [],
        change: null,
        selectedfun: null
    };
    var options = $.extend(defaults, options);
    var $element = $(this);
    if (options.url != "") {
        $.ajax({
            url: options.url,
            data: options.param,
            dataType: "json",
            async: false,
            success: function (data) {
                // $element.empty();
                // $element.append($("<option>=请选择=</option>"));
                $.each(data, function (i) {
                    $element.append($("<option></option>").val(data[i][options.id]).html(data[i][options.text]));
                });
                $element.on("change", function (e) {
                    if (options.change != null) {
                        options.change(data[$(this).find("option:selected").index()]);
                    }
                });

                if (options.selectedfun != null && typeof options.selectedfun === "function") {
                    options.selectedfun();
                }
            }
        });
    } else {
    }
}
$.fn.authorizeButton = function () {
    var moduleId = top.$(".CPK_iframe:visible").attr("id").substr(6);
    var dataJson = top.clients.authorizeButton[moduleId];
    var $element = $(this);
    $element.find('a[authorize=yes]').attr('authorize', 'no');
    if (dataJson != undefined) {
        $.each(dataJson, function (i) {
            $element.find("#" + dataJson[i].F_EnCode).attr('authorize', 'yes');
        });
    }
    $element.find("[authorize=no]").parents('li').prev('.split').remove();
    $element.find("[authorize=no]").parents('li').remove();
    $element.find('[authorize=no]').remove();
}
//$.fn.dataGrid = function (options) {
//    var defaults = {
//        datatype: "json",
//        autowidth: true,
//        rownumbers: true,
//        shrinkToFit: false,
//        gridview: true
//    };
//    var options = $.extend(defaults, options);
//    var $element = $(this);
//    options["onSelectRow"] = function (rowid) {
//        var length = $(this).jqGrid("getGridParam", "selrow").length;
//        var $operate = $(".operate");
//        if (length > 0) {
//            $operate.animate({ "left": 0 }, 200);
//        } else {
//            $operate.animate({ "left": '-100.1%' }, 200);
//        }
//        $operate.find('.close').click(function () {
//            $operate.animate({ "left": '-100.1%' }, 200);
//        })
//    };
//    $element.jqGrid(options);
//};

var $table;
var selectionIds = [];	//保存选中ids
$.fn.dataGrid = function (options) {
    //var defaults = {
    //    datatype: "json",
    //    autowidth: true,
    //    rownumbers: true,
    //    shrinkToFit: false,
    //    gridview: true
    //};
    //var options = $.extend(defaults, options);
    debugger;
    var $element = $(this);
    options["onClickRow"] = function (row, $element, field) {
        //var length = $(this).jqGrid("getGridParam", "selrow").length;
        var length = $(this).bootstrapTable('getSelections').length;
        var $operate = $(".operate");
        if (length > 0) {
            $operate.stop(true, true).animate({ "left": 0 }, 200);
        } else {
            $operate.stop(true, true).animate({ "left": '-100.1%' }, 200);
        }
        if ($.isFunction(options.SelectRowData)) {
            options.SelectRowData(row, $element, field);
        }
        $operate.find('.close').click(function () {
            $operate.stop(true, true).animate({ "left": '-100.1%' }, 200);
        })
    };
    options["responseHandler"] = function (res) {
        $.each(res.rows, function (i, row) {
            if (selectionIds.length > 0)
                row.checkstate = $.inArray(row[options["uniqueId"]], selectionIds) != -1;	//判断当前行的数据id是否存在与选中的数组，存在则将多选框状态变为true
        });
        return res;
    };
    if (options["selectIds"] != undefined) {
        var $selectIds = $(options["selectIds"]);
        var sSelect = $selectIds.val();
        if (sSelect.length > 0) {
            sSelect = sSelect.replace(/,$/, "");
            selectionIds = sSelect.split(',');
        }
        if (options["selectCount"] != undefined) {
            var $selectCount = $(options["selectCount"]);
            $selectCount.val(selectionIds.length);
        }
    }
    $table = $element.bootstrapTable(options);
    //选中事件操作数组
    var union = function (array, ids) {
        $.each(ids, function (i, id) {
            if ($.inArray(id, array) == -1) {
                array[array.length] = id;
            }
        });
        return array;
    };
    //取消选中事件操作数组
    var difference = function (array, ids) {
        $.each(ids, function (i, id) {
            var index = $.inArray(id, array);
            if (index != -1) {
                array.splice(index, 1);
            }
        });
        return array;
    };
    var _ = { "union": union, "difference": difference };
    //绑定选中事件、取消事件、全部选中、全部取消
    $table.on('check.bs.table check-all.bs.table uncheck.bs.table uncheck-all.bs.table', function (e, rows) {
        var bCheckBox = options["columns"][0].checkbox;
        if (bCheckBox == undefined)
            bCheckBox = false;
        var bRadio = options["columns"][0].radio;
        if (bRadio == undefined)
            bRadio = false;
        if (bCheckBox) {
            var ids = $.map(!$.isArray(rows) ? [rows] : rows, function (row) {
                return row[options["uniqueId"]];
            });
            func = $.inArray(e.type, ['check', 'check-all']) > -1 ? 'union' : 'difference';
            selectionIds = _[func](selectionIds, ids);

            if (options["selectIds"] != undefined) {
                var sSelect = "";
                for (var i = 0; i < selectionIds.length; i++) {
                    if (selectionIds[i] != '')
                        sSelect = sSelect + selectionIds[i] + ",";
                }
                var $selectIds = $(options["selectIds"]);
                $selectIds.val(sSelect);
                if (options["selectCount"] != undefined) {
                    var $selectCount = $(options["selectCount"]);
                    $selectCount.text(selectionIds.length);
                }
            }
        } else if (bRadio) {
            if (options["selectIds"] != undefined) {
                var $selectIds = $(options["selectIds"]);
                var sSelect = $.parseJSON(JSON.stringify($table.bootstrapTable('getSelections')))[0][options["uniqueId"]];
                selectionIds = [sSelect];
                $selectIds.val(sSelect);
                if (options["selectCount"] != undefined) {
                    var $selectCount = $(options["selectCount"]);
                    $selectCount.text(1);
                }
            }
        }
    });

};

