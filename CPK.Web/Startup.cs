﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Web;
using NLog.Extensions.Logging;
using Quartz.Spi;
using Quartz.Impl;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Localization;
using System.Globalization;
using CPK.Web.App_Code;
using AutoMapper;

namespace CPK.Web
{
    public class Startup
    {
        private const string enUSCulture = "en-US";  
        private const string zhCNCulture = "zh-CN"; 

        public Startup(IHostingEnvironment env)
        {
            // System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)//增加环境配置文件，新建项目默认有
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            IMvcBuilder mvcBuilder = services.AddMvc().AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
                        .AddDataAnnotationsLocalization(); 
            services.AddSingleton<Microsoft.AspNetCore.Http.IHttpContextAccessor, Microsoft.AspNetCore.Http.HttpContextAccessor>();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(20);
                options.Cookie.HttpOnly = true;
            });
            services.AddLocalization(options => options.ResourcesPath = "Resources");
            services.AddAutoMapper();
            // Configure supported cultures and localization options
            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new[]
                {
                            new CultureInfo(enUSCulture),
                            new CultureInfo(zhCNCulture)
                        };

                // State what the default culture for your application is. This is used if no specific culture
                // can be determined for a given request.
                options.DefaultRequestCulture = new RequestCulture(culture: enUSCulture, uiCulture: enUSCulture);

                // You must explicitly state which cultures your application supports.
                // These are the cultures the app supports for formatting numbers, dates, etc.
                options.SupportedCultures = supportedCultures;

                // These are the cultures the app supports for UI strings (that we have localized resources for).
                options.SupportedUICultures = supportedCultures;

                // You can change which providers are configured to determine the culture for requests, or even add a custom
                // provider with your own logic. The providers will be asked in order to provide a culture for each request,
                // and the first to provide a non-null result that is in the configured supported cultures list will be used.
                // By default, the following built-in providers are configured:
                // - QueryStringRequestCultureProvider, sets culture via "culture" and "ui-culture" query string values, useful for testing
                // - CookieRequestCultureProvider, sets culture via "ASPNET_CULTURE" cookie
                // - AcceptLanguageHeaderRequestCultureProvider, sets culture via the "Accept-Language" request header
                //
                //options.RequestCultureProviders.Insert(0, new CustomRequestCultureProvider(async context =>
                //{
                //  // My custom request culture logic
                //  return new ProviderCultureResult("en");
                //}));
            });
            //添加mvc自定义寻址Razor页面地址
            mvcBuilder.AddRazorOptions(options =>
            {
                options.ViewLocationExpanders.Add(new NamespaceViewLocationExpander());
            });

            services.AddAuthentication("IdentityCookieAuthenScheme").AddCookie("IdentityCookieAuthenScheme", options =>
            {
                options.LoginPath = "/Login/Index";
                options.Cookie.Name = "AuthCookie";
            });

            services.AddSingleton<IJobFactory, JobFactory>();
            //services.AddSingleton(provider =>
            //{
            //    var option = new QuartzOption(Configuration);
            //    var sf = new StdSchedulerFactory(option.ToProperties());
            //    var scheduler = sf.GetScheduler().Result;
            //    scheduler.JobFactory = provider.GetService<IJobFactory>();
            //    return scheduler;
            //});
            services.AddHostedService<QuartzService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddNLog();//添加NLog
            env.ConfigureNLog("nlog.config");
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                //app.UseExceptionHandler("/Home/Error");
                app.UseMiddleware<Code.Middleware.ExceptionHandlerMiddleware>();
            }
            app.UseErrorHandling();
            ///注册全局先上问关联的HttpContext
            System.Web.HttpContext.Configure(app.ApplicationServices.GetRequiredService<Microsoft.AspNetCore.Http.IHttpContextAccessor>(), env);

            //注册全局Configuration对象
            Code.ConfigurationManager.Configure(Configuration);

            app.UseSession();

            app.UseStaticFiles();

            app.UseAuthentication();
            var supportedCultures = new[]
                 {
                        new CultureInfo(enUSCulture),
                        new CultureInfo(zhCNCulture), 
                    };

            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture(enUSCulture),
                // Formatting numbers, dates, etc.
                SupportedCultures = supportedCultures,
                // UI strings that we have localized.
                SupportedUICultures = supportedCultures
            });
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
                routes.MapRoute(
                      name: "ProductManage",
                      template: "ProductManage/{controller}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "ExampleManage",
                    template: "ExampleManage/{controller}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "ReportManage",
                    template: "ReportManage/{controller}/{action=Index}/{id?}");

                routes.MapRoute(
                  name: "SystemManage",
                  template: "SystemManage/{controller}/{action=Index}/{id?}");

                routes.MapRoute(
                 name: "SystemSecurity",
                 template: "SystemSecurity/{controller}/{action=Index}/{id?}");
                routes.MapRoute(
                   name: "MoblieAPI",
                   template: "MoblieAPI/{controller}/{action=Index}/{id?}");
            });
        }
    }
}
