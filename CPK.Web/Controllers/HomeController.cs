﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CPK.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.AspNetCore.Hosting.Server;
using System.IO;
using System.DrawingCore;
using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Localization;
using CPK.Domain.ViewModel;
using CPK.Application.ProductManage;
using AutoMapper;
using CPK.Domain.Entity.ProductManage;
using CPK.Application.Infrastructure;
using CPK.Application;
using CPK.Code;

namespace CPK.Web.Controllers
{
    public class HomeController : BaseController
    {
        private ChartJobResultApp _chartJobResultApp = new ChartJobResultApp();
        private DataSource_MDCSApp _DataSource_MDCSApp = new DataSource_MDCSApp();
        private readonly IMapper _mapper;
        private IHostingEnvironment environment { get; set; }
        public HomeController(IHostingEnvironment environment, IMapper mapper)
        {
            this.environment = environment;
            _mapper = mapper;
        }

        /// <summary>
        /// 临时取样CPK并存储
        /// </summary>
        [HttpGet]
        public void TempleLineCheckCPK(int day = 30)
        {
            var cpkparamList = new Chart_Parameters_CPKApp().GetList();



            ChartApp chartapp = new ChartApp();
            var chartlist = chartapp.GetList(t => t.F_EnabledMark == true);
            var CPKresultList = new CPKResultApp().GetList(t => t.F_CreatorTime > DateTime.Now.AddDays(-2));
            for (var i = 0; i < day; i++)
            {
                foreach (var chartitem in chartlist)
                {
                    DateTime shouldEndTime = Convert.ToDateTime(DateTime.Now.AddDays(i - day).ToString("yyyy-MM-dd"));
                    DateTime shouldStartTime = Convert.ToDateTime(DateTime.Now.AddDays(i - day - 1).ToString("yyyy-MM-dd"));


                    Chart_Parameters_CPKEntity cpkparam = new Chart_Parameters_CPKEntity();
                    if (cpkparamList != null)
                    {
                        cpkparam = cpkparamList.Find(t => t.F_Id == chartitem.F_Id);
                    }
                    DataSource_MDCSEntity dbsource = _DataSource_MDCSApp.GetForm(chartitem.F_DataSourceId);
                    string ConnString = dbsource.F_ConnectString;
                    string lineName = dbsource.F_Line;
                    string deviceID = dbsource.F_Device;
                    string varID = dbsource.F_Variable;
                    string MCF = dbsource.F_MCF;
                    string BoardType = dbsource.F_BoardType;
                    DataSourceFromMDCS MDCSDB = new DataSourceFromMDCS(ConnString);
                    DateTime dStartTime = Convert.ToDateTime(shouldStartTime);
                    DateTime dEndTime = Convert.ToDateTime(shouldEndTime);
                    string[] DirtyRead = null;
                    if (!string.IsNullOrWhiteSpace(chartitem.F_DirtyRead))
                    {
                        DirtyRead = chartitem.F_DirtyRead.Trim().TrimEnd(',').Split(',');
                    }
                    var result = MDCSDB.getMDCSDataSource(lineName, deviceID, varID, dStartTime, dEndTime, DirtyRead, MCF, BoardType);
                    if (result == null)
                    {
                        continue;
                    }
                    var cpkresult = DataItemGroupCom.CaculateCPK(result, Convert.ToDecimal(cpkparam.F_LSL), Convert.ToDecimal(cpkparam.F_USL));
                    if (cpkresult == null)
                    {
                        continue;
                    }
                    CPKResultEntity cpkResultEntity = new CPKResultEntity();
                    cpkResultEntity.F_LastLoadTime = shouldEndTime;
                    cpkResultEntity.F_USL = cpkresult.USL;
                    cpkResultEntity.F_LSL = cpkresult.LSL;
                    cpkResultEntity.F_Sigma = cpkresult.Sigma;
                    cpkResultEntity.F_DistributeStatistics = cpkresult.DistributeStatistics.ToJson();
                    cpkResultEntity.F_ChartId = chartitem.F_Id;
                    cpkResultEntity.F_CPK = cpkresult.CPK;
                    new CPKResultApp().SubmitForm(cpkResultEntity, null);

                }
            }


        }
        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return LocalRedirect(returnUrl);
        }
        // [Authorize]
        public override ActionResult Index()
        {
            if (!string.IsNullOrWhiteSpace(HttpContext.Request.Query["ReturnUrl"].ToString()))
            {
                return Redirect(HttpContext.Request.Query["ReturnUrl"].ToString());
            }
            return View();
        }

        public IActionResult Default()
        {
            return View();
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]

        public ActionResult PostFile([FromForm] IFormCollection formCollection)
        {
            String result = "Fail";
            if (formCollection.ContainsKey("user"))
            {
                var user = formCollection["user"];
            }
            //Determine base path for the application.  
            var basePath = PlatformServices.Default.Application.ApplicationBasePath;
            basePath = basePath.Substring(0, basePath.IndexOf("\\bin")) + "\\wwwroot";
            var path = "\\UploadFiles\\mobile\\" + DateTime.Now.ToString("yyyyMMdd");
            var uploadpath = basePath + path;

            FormFileCollection fileCollection = (FormFileCollection)formCollection.Files;
            foreach (IFormFile file in fileCollection)
            {
                StreamReader reader = new StreamReader(file.OpenReadStream());
                String content = reader.ReadToEnd();
                String name = file.FileName;
                var filename = path + "\\" + DateTime.Now.ToFileTime().ToString() + ".jpg";
                if (!Directory.Exists(uploadpath))
                {
                    // iRepositoryPath = parentPath + "\\" + areaName + "\\" + "\\IRepositories";
                    Directory.CreateDirectory(uploadpath);
                }
                var mfilename = basePath + filename;
                if (System.IO.File.Exists(mfilename))
                {
                    System.IO.File.Delete(mfilename);
                }
                using (FileStream fs = System.IO.File.Create(mfilename))
                {
                    // 复制文件  
                    file.CopyTo(fs);
                    // 清空缓冲区数据  
                    fs.Flush();
                }
                result = filename;
            }
            return Json(result);
        }



        [HttpPost("upload-file")]
        public async Task<IActionResult> uploadFile(UploadFileData data)
        {
            var allFiles = Request.Form.Files; // 多文件的话可以直接从 form 拿到完, 或则设置成 List<IFormFile> 就可以了
            var root = environment.WebRootPath;
            var extension = Path.GetExtension(data.file.FileName);
            var guid = Guid.NewGuid().ToString();
            var fullPath = $@"{root}\images\{guid + extension}";
            using (FileStream stream = new FileStream(fullPath, FileMode.Create))
            {
                await data.file.CopyToAsync(stream);
            }
            return Ok();
        }
        public ActionResult Upload(IFormCollection files)
        {
            var result = new UploadResult();

            foreach (var item in files.Files)
            {
                #region 把文件流转化为字节流
                byte[] buffer = new byte[item.Length];

                Stream fs = item.OpenReadStream();

                fs.Read(buffer, 0, buffer.Length);
                #endregion

                //UploadConfig config = new UploadConfig
                //{
                //    Buffer = buffer,
                //    FileName = item.FileName,
                //    Chunked = files.Keys.Contains("chunk"),
                //    PreviosName = files["previosName"]
                //};

                //result = _provider.Upload(config);
            }
            return Json(result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="base64str"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult MUploadImgBase64Str(string base64str)
        {
            try
            {
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var imgData = base64str.Split(',')[1];
                //过滤特殊字符即可   
                string dummyData = imgData.Trim().Replace("%", "").Replace(",", "").Replace(" ", "+");
                if (dummyData.Length % 4 > 0)
                {
                    dummyData = dummyData.PadRight(dummyData.Length + 4 - dummyData.Length % 4, '=');
                }
                byte[] byteArray = Convert.FromBase64String(dummyData);
                using (System.IO.MemoryStream ms = new System.IO.MemoryStream(byteArray))
                {
                    var img = Image.FromStream(ms);
                    basePath = basePath.Substring(0, basePath.IndexOf("\\bin")) + "\\wwwroot";
                    var path = "\\UploadFiles\\mobile";
                    var uploadpath = basePath + path;
                    var filename = DateTime.Now.ToFileTime().ToString() + ".jpg";
                    if (!Directory.Exists(uploadpath))
                    {
                        Directory.CreateDirectory(uploadpath);
                    }
                    var saveName = uploadpath + filename;
                    img.Save(saveName);
                    return Json(path + filename);
                }
            }
            catch (Exception e)
            {
                return Json(e.Message);

            }
        }
        [HttpPost]
        public ActionResult MUploadImg(IFormFile mfile)
        {
            var files = Request.Form.Files;
            return UploadImg(mfile, "Mobile");
        }
        [HttpPost]
        public ActionResult UploadImg(IFormFile file, string dir = "UserImg")
        {

            //if (file != null)
            //{
            //    var files = Request.Form.Files;
            //    //using (var reader = new StreamReader(file.OpenReadStream()))
            //    //{
            //    //    var fileContent = reader.ReadToEnd();
            //    //    var parsedContentDisposition = ContentDispositionHeaderValue.Parse(file.ContentDisposition);
            //    //    var fileName = parsedContentDisposition.FileName;
            //    //}
            //    //var path = "~/Content/UploadFiles/" + dir + "/";
            //    //var uploadpath =  PlatformServices.Default.Application.ApplicationBasePath;
            //    //if (!Directory.Exists(uploadpath))
            //    //{
            //    //    Directory.CreateDirectory(uploadpath);
            //    //}
            //    //string fileName = Path.GetFileName(file.FileName);// 原始文件名称
            //    //string fileExtension = Path.GetExtension(fileName); // 文件扩展名
            //    ////string saveName = Guid.NewGuid() + fileExtension; // 保存文件名称 这是个好方法。
            //    //string saveName =DateTime.Now.ToString("yyyyMMddhhmmss") + fileExtension; // 保存文件名称 这是个好方法。
            //    //file.SaveAs(uploadpath + saveName);

            //    return Json(new { Success = true, SaveName = path + saveName });
            //}
            return Json(new { Success = false, Message = "请选择要上传的文件！" });

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="base64str"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult MUploadImg(string mfile)
        {
            try
            {
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var imgData = mfile.Split(',')[1];
                //过滤特殊字符即可   
                string dummyData = imgData.Trim().Replace("%", "").Replace(",", "").Replace(" ", "+");
                if (dummyData.Length % 4 > 0)
                {
                    dummyData = dummyData.PadRight(dummyData.Length + 4 - dummyData.Length % 4, '=');
                }
                byte[] byteArray = Convert.FromBase64String(dummyData);
                using (System.IO.MemoryStream ms = new System.IO.MemoryStream(byteArray))
                {
                    var img = Image.FromStream(ms);

                    var path = "/Content/UploadFiles/mobile/";
                    var uploadpath = basePath + path;
                    if (!Directory.Exists(uploadpath))
                    {
                        Directory.CreateDirectory(uploadpath);
                    }
                    var saveName = uploadpath + DateTime.Now.ToFileTime().ToString() + ".jpg";
                    img.Save(saveName);
                    return Json(saveName);
                }
            }
            catch (Exception e)
            {
                return Json(e.Message);

            }
        }
        [HttpGet]
        public IActionResult GetMonitorList(string stype)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            List<ChartRPSMessageVM> RPSMsgList=new List<ChartRPSMessageVM>();
            EchartCheck echartCheck=new EchartCheck();
            var chartJobresultlist = _chartJobResultApp.GetList(); 
            var result = _mapper.Map<List<ChartRPSMessageVM>>(chartJobresultlist); 
            sw.Stop();
            var total = sw.ElapsedMilliseconds;
            return Json(result);
        } 
    }
    public class UploadFileData
    {
        public IFormFile file { get; set; }
    }
}
